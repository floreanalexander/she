import axios from 'axios';
import { useDispatch } from 'react-redux';
import { showNotifAction } from '../redux/actions/notifActions';
import { logoutAction } from '../redux/actions/reduxFormActions';
import config from '../config/env';

const useGet = () => {
  const dispatch = useDispatch();
  const responseGetData = async (url, show_message = true) => {
    try {
      const response = await axios.get(`${config.API_URL}${url}`, {
        headers: {
          Authorization: localStorage.getItem('token_prospect'),
        },
      });
      const data = await response.data;
      console.log('response get result', data);
      if (show_message) {
        dispatch(showNotifAction(data?.message));
      }
      return data;
    } catch (error) {
      console.log('hooks get error', error);
      console.log('hooks get error response', error.response.status);

      dispatch(
        showNotifAction(
          error.response
            ? error.response.data?.message === undefined
              ? error.message
              : error.response.data?.message
            : error.message,
        ),
      );
			if (error.response.status === 401) {
        dispatch(logoutAction());
      }
      return null;
    }
  };

  return { responseGetData };
};

export default useGet;
