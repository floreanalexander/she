import axios from 'axios';
import { useDispatch } from 'react-redux';
import { showNotifAction } from '../redux/actions/notifActions';
import { logoutAction } from '../redux/actions/reduxFormActions';
import config from '../config/env';

const usePut = () => {
  const dispatch = useDispatch();
  const responsePutData = async (url, payload, show_message = true) => {
    try {
      const response = await axios.put(`${config.API_URL}${url}`, payload, {
        headers: {
          Authorization: localStorage.getItem('token_prospect'),
        },
      });
      const data = await response.data;
      console.log('response put result', data);
      if (show_message) {
        dispatch(showNotifAction(data?.message));
      }
      return data;
    } catch (error) {
      console.log('hooks put error', error);
      console.log('hooks put error response', error.response.status);

      dispatch(
        showNotifAction(
          error.response
            ? error.response.data?.message === undefined
              ? error.message
              : error.response.data?.message
            : error.message,
        ),
      );
      if (error.response.status === 401) {
        dispatch(logoutAction());
      }
      return null;
    }
  };

  return { responsePutData };
};

export default usePut;
