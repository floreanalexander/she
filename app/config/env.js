const dev = {
  // API_URL: 'https://stg-api-prospect.acset.co',
  API_URL: 'http://159.223.87.134:2500',
};

const prod = {
  // API_URL: 'https://stg-api-prospect.acset.co',
  API_URL: 'http://159.223.87.134:2500',
};

const config = process.env.NODE_ENV == 'development' ? dev : prod;
export default config;
