import React from 'react';
import { Helmet } from 'react-helmet';
import brand from 'dan-api/dummy/brand';
import { SourceReader, PapperBlock } from 'dan-components';
import {
  TodoListMini,
  ContactsMini,
  CalculatorMini,
  TradeMini
} from './demos';

function MiniApps() {
  const title = brand.name + ' - Widgets';
  const description = brand.desc;
  const docSrc = 'containers/Widgets/demos/';
  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
      <PapperBlock title="Contact" whiteBg icon="ion-ios-contacts-outline" desc="A shortcut way to call and read messages from your contact">
        <div>
          <ContactsMini />
          <SourceReader componentName={docSrc + 'ContactsMini.js'} />
        </div>
      </PapperBlock>
    </div>
  );
}

export default MiniApps;
