import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListPartner(props) {
  const columns = [
    {
      name: 'Name',
      options: {
        filter: true,
      },
    },
    {
      name: 'Project Manager',
      options: {
        filter: true,
      },
    },
    {
      name: 'Start Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Action',
      options: {
        filter: true,
        customBodyRender: value => {
          return (
            <div>
              <Button
                className={classes.button}
                variant="contained"
                color="secondary"
              >
                Choose
              </Button>
        </div>
          );
        },
      },
    },
  ];

  const data = [
    ['WEST VISTA RECIDENCE', '', '30 Apr 2018'],
    ['LRT CDA', '', '11 Nov 2016'],
    ['HEAD OFFICE', '', '21 Jul 2018'],
    ['WORKSHOP JONGGOL','WANWAN AGUNG NURWANTO', '21 Jul 2018'],
    ['WORKSHOP CILEUNGSI','Dedi Risiandi', '21 Jul 2018'],

    ['GRAND MANSION APARTMENT','', '21 Jul 2018'],
    ['MILLENIUM CENTENNIAL CENTER','', '21 Jul 2018'],
    ['THAMRIN NINE','EGY DJUMHANA KARTAKUSUMA', '21 Jul 2018'],
    ['PROYEK JALUR GANDA KA BOGOR-SUKABUMI','Romie Endianto', '01 Jun 2021'],
    ['CLEON PARK APARTMENT','IKHSAN PANGALITAN S', '01 Okt 2021'],

    ['PLTM Besai Kemu','ERIK HELMI SUSANTO WAELAURUW', '01 Jan 2021'],
    ['TOL SERBARAJA','Januar Ali', '11 Jan 2022'],
    ['KCC GLASS BATANG','Romie Endianto', '01 Mar 2022'],
    ['Revitalisasi Sistem Kelistrikan Istana Negara','ARIS ARYANTO', '19 Apr 2022'],
    ['CISERTI','HADI SASMITO', '01 Agt 2022']
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Master Data Project" data={data} columns={columns} options={options} />
    </div>
  );
}

ListPartner.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListPartner);
