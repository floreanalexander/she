import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import { PapperBlock } from 'dan-components';
import _ from 'lodash';
import useGet from '../../../../hooks/useGet';

import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 350,
  },
  button: {
    margin: theme.spacing(1),
  },
  leftIcon: {
    marginRight: theme.spacing(1),
  },
});

function TabShareholders(props) {
  const { classes } = props;
  const { responseGetData } = useGet();

  const initForm = {
    flag: 0, // 0 akan dihapus, 1 tetap ada
    id: '', // ambil dari Id partner
    idHolder: '', // ambil dari PartnerNumber partner
    nameHolder: '', // ambil dari PartnerName partner
    note: '',
  };

  const [loadingList, setLoadingList] = useState(true);
  const [dataState, setDataState] = useState(initForm);
  const [listPartner, setListPartner] = useState([]);

  const [open, setOpen] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);

  const fetchListPartner = async () => {
    const result = await responseGetData('/api/partner', false);
    if (!result) {
      setListPartner([]);
      return;
    }
    setListPartner(result);
  };

  useEffect(() => {
    async function fetchDropdown() {
      await fetchListPartner();
      setLoadingList(false);
    }
    fetchDropdown();
  }, []);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClickOpenDelete = item => {
    setDataState(item);
    setOpenDelete(true);
  };

  const handleClose = (event, reason) => {
    if (reason !== 'backdropClick') {
      setOpen(false);
      setDataState(initForm);
    }
  };
  const handleCloseDelete = (event, reason) => {
    if (reason !== 'backdropClick') {
      setOpenDelete(false);
      setDataState(initForm);
    }
  };

  const handleChangePartner = (Id, PartnerNumber, PartnerName) => {
    setDataState({
      ...dataState,
      id: Id,
      idHolder: PartnerNumber,
      nameHolder: PartnerName,
    });
  };

  const handleChange = event => {
    setDataState({
      ...dataState,
      [event.target.name]: event.target.value,
    });
  };

  const handleAdd = () => {
    const newData = [...props.dataState.shareHolder];
    if (dataState.id === '') {
      alert('data shareholder belum diisi');
      return;
    }
    if (dataState.id === props.dataState.Id) {
      alert('data shareholder tidak boleh sama dengan part number');
      return;
    }
    for (var i = 0; i < newData.length; i++) {
      if (newData[i].id === dataState.id) {
        alert('data shareholder sudah ada');
        return;
      }
    }
    newData.push(dataState);
    props.handleChange(newData);
    handleClose();
  };

  const handleDelete = () => {
    const newData = [...props.dataState.shareHolder];
    props.handleChange(
      _.filter(newData, function pick(n) {
        return n.idHolder !== dataState.idHolder;
      }),
    );
    handleCloseDelete();
  };

  return (
    <>
      <div style={{ textAlign: 'right' }}>
        <Button className={classes.button} variant="contained" color="secondary" onClick={handleClickOpen}>
          <AddIcon className={classes.leftIcon} />
          TAMBAH
        </Button>
      </div>
      {props.dataState.shareHolder.map(item => {
        return (
          <PapperBlock title={item.idHolder} desc={item.nameHolder}>
            <p style={{ display: 'flex' }}>
              <span style={{ width: '20%' }}>Note </span>
              <span style={{ width: '80%' }}>: {item.note}</span>
            </p>
            <div style={{ textAlign: 'right' }}>
              <Button
                className={classes.button}
                variant="contained"
                color="error"
                onClick={() => handleClickOpenDelete(item)}
              >
                <DeleteIcon />
              </Button>
            </div>
          </PapperBlock>
        );
      })}
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Tambah Shareholder</DialogTitle>
        <DialogContent>
          {/* <DialogContentText>
            To subscribe to this website, please enter your email address here. We will send updates occasionally.
          </DialogContentText> */}
          {/* <TextField autoFocus margin="dense" id="name" label="Email Address" type="email" fullWidth /> */}
          {loadingList ? (
            <div>Loading...</div>
          ) : (
            <div className={classes.root}>
              <FormControl className={classes.formControl} error={dataState.id === ''}>
                <InputLabel htmlFor="partner">Partner</InputLabel>
                <Select
                  value={dataState.id}
                  // onChange={handleChangePartner}
                  inputProps={{
                    name: 'id',
                    id: 'partner',
                  }}
                >
                  <MenuItem value="">
                    <em>--Pilih Partner--</em>
                  </MenuItem>
                  {listPartner.map(item => {
                    return (
                      <MenuItem
                        key={item.Id}
                        value={item.Id}
                        onClick={() => {
                          handleChangePartner(item.Id, item.PartnerNumber, item.PartnerName);
                        }}
                      >
                        {item.PartnerName}
                      </MenuItem>
                    );
                  })}
                </Select>
                {dataState.id === '' && <FormHelperText>Harap Pilih Partner</FormHelperText>}
              </FormControl>
              <FormControl className={classes.formControl}>
                <InputLabel htmlFor="note">Note</InputLabel>
                <Input
                  inputProps={{
                    name: 'note',
                    id: 'note',
                  }}
                  value={dataState.note}
                  onChange={handleChange}
                />
                <FormHelperText>Contoh: Share periode bulan ini sebesar 50%</FormHelperText>
              </FormControl>
            </div>
          )}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            CANCEL
          </Button>
          <Button onClick={() => handleAdd()} color="primary">
            ADD
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog open={openDelete} onClose={handleCloseDelete} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Delete Shareholder</DialogTitle>
        <DialogContent>
          <DialogContentText>Apakah Anda yakin akan menghapus shareholder?</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => handleDelete()} color="primary">
            YA
          </Button>
          <Button onClick={() => handleCloseDelete()} color="default">
            TIDAK
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

TabShareholders.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TabShareholders);
