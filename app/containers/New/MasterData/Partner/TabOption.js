import React from 'react';
import PropTypes from 'prop-types';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import _ from 'lodash';

import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 350,
  },
});

function TabOption(props) {
  const { classes } = props;

  return (
    <>
      <div className={classes.root}>
        <FormControl component="fieldset" className={classes.formControl}>
          <FormLabel component="legend">Status Partner</FormLabel>
          <RadioGroup
            row
            className={classes.group}
            value={props.dataState.option_RowStatus.toString()}
            onChange={props.handleChange}
            name="option_RowStatus"
          >
            <FormControlLabel value="1" control={<Radio />} label="Aktif" />
            <FormControlLabel value="0" control={<Radio />} label="Non-Aktif" />
          </RadioGroup>
        </FormControl>
      </div>
    </>
  );
}

TabOption.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TabOption);
