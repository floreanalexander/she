import React from 'react';
import PropTypes from 'prop-types';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import _ from 'lodash';

import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 350,
  },
});

const requiredValidation = value => (value === null || value === '' ? false : true);
const emailValidation = value => (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? false : true);

function TabGeneral(props) {
  const { classes } = props;

  return (
    <>
      <div className={classes.root}>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="bentuk-usaha">Bentuk Usaha</InputLabel>
          <Select
            value={props.dataState.general_PartnerForm}
            onChange={props.handleChange}
            inputProps={{
              name: 'general_PartnerForm',
              id: 'bentuk-usaha',
            }}
          >
            <MenuItem value="">
              <em>--Pilih Bentuk Usaha--</em>
            </MenuItem>
            {props.listPartnerType.map(item => {
              return (
                <MenuItem key={item.Id} value={item.Id}>
                  {item.Name}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="kepemilikan-usaha">Kepemilikan Usaha</InputLabel>
          <Select
            value={props.dataState.general_PartnerOwnership}
            onChange={props.handleChange}
            inputProps={{
              name: 'general_PartnerOwnership',
              id: 'kepemilikan-usaha',
            }}
          >
            <MenuItem value="">
              <em>--Pilih Kepemilikan Usaha--</em>
            </MenuItem>
            {props.listPartnerOwnership.map(item => {
              return (
                <MenuItem key={item.Id} value={item.Id}>
                  {item.Name}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
      </div>
      <div className={classes.root}>
        <FormControl className={classes.formControl} error={!emailValidation(props.dataState.general_Email)}>
          <InputLabel htmlFor="email">E-mail</InputLabel>
          <Input
            inputProps={{
              name: 'general_Email',
              id: 'email',
            }}
            value={props.dataState.general_Email}
            onChange={props.handleChange}
          />
          {!emailValidation(props.dataState.general_Email) ? (
            <FormHelperText>Format email salah</FormHelperText>
          ) : (
            <FormHelperText>Contoh: eclipse@email.com</FormHelperText>
          )}
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="phone">Nomor Telepon</InputLabel>
          <Input
            inputProps={{
              name: 'general_Phone',
              id: 'phone',
            }}
            value={props.dataState.general_Phone}
            onChange={props.handleChange}
          />
          <FormHelperText>Contoh: 08232145610</FormHelperText>
        </FormControl>
      </div>
    </>
  );
}

TabGeneral.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TabGeneral);
