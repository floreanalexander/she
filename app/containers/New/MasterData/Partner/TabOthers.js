import React from 'react';
import PropTypes from 'prop-types';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Checkbox from '@material-ui/core/Checkbox';
import _ from 'lodash';

import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 350,
  },
});

function TabOthers(props) {
  const { classes } = props;

  return (
    <>
      <div className={classes.root}>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="background-owner">Background Owner</InputLabel>
          <Input
            inputProps={{
              name: 'other_BgOwner',
              id: 'background-owner',
            }}
            value={props.dataState.other_BgOwner}
            onChange={props.handleChange}
          />
          <FormHelperText>Contoh: Services</FormHelperText>
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="karakter-owner">Karakter Owner</InputLabel>
          <Input
            inputProps={{
              name: 'other_ChrOwner',
              id: 'karakter-owner',
            }}
            value={props.dataState.other_ChrOwner}
            onChange={props.handleChange}
          />
          <FormHelperText>Contoh: Strong</FormHelperText>
        </FormControl>
      </div>
      <div className={classes.root}>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="previous-business">Previous Business</InputLabel>
          <Input
            inputProps={{
              name: 'other_PrevBsns',
              id: 'previous-business',
            }}
            value={props.dataState.other_PrevBsns}
            onChange={props.handleChange}
          />
          <FormHelperText>Contoh: Infrastructure</FormHelperText>
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="alasan-masuk-bisnis">Alasan Masuk Bisnis</InputLabel>
          <Input
            inputProps={{
              name: 'other_Reason',
              id: 'alasan-masuk-bisnis',
            }}
            value={props.dataState.other_Reason}
            onChange={props.handleChange}
          />
          <FormHelperText>Contoh: Expansion</FormHelperText>
        </FormControl>
      </div>
      <div className={classes.root}>
        <FormControlLabel
          control={
            <Checkbox
              checked={props.dataState.other_Politically === 1 ? true : false}
              onChange={props.handleChangeCB('other_Politically')}
              value="other_Politically"
              color="primary"
            />
          }
          label="Politically Exposed Person"
        />
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="average-payment-days">Average Payment Days</InputLabel>
          <Input
            inputProps={{
              name: 'other_PaymentDay',
              id: 'average-payment-days',
            }}
            value={props.dataState.other_PaymentDay}
            onChange={props.handleChange}
          />
          <FormHelperText>Contoh: 30</FormHelperText>
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="sap-no">SAP No.</InputLabel>
          <Input
            inputProps={{
              name: 'other_SAPNo',
              id: 'sap-no',
            }}
            value={props.dataState.other_SAPNo}
            onChange={props.handleChange}
          />
          <FormHelperText>Contoh: 20220100001</FormHelperText>
        </FormControl>
      </div>
    </>
  );
}

TabOthers.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TabOthers);
