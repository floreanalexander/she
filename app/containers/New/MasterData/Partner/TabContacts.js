import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import { PapperBlock } from 'dan-components';
import _ from 'lodash';
import useGet from '../../../../hooks/useGet';

import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 350,
  },
  button: {
    margin: theme.spacing(1),
  },
  leftIcon: {
    marginRight: theme.spacing(1),
  },
});

const emailValidation = value => (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? false : true);

function TabContacts(props) {
  const { classes } = props;
  const { responseGetData } = useGet();

  const initForm = {
    CNumber: '0', // nanti generate otomatis
    Email: '',
    Flag: 0,
    Id: '0', // nanti generate otomatis
    Name: '',
    Note: '',
    Phone: '',
    ShowName: '', // gabungan ShowTitle dan Name
    ShowTitle: '',
    Title: '', // kalo Mr. L, kaor Ms. P
  };

  const [loadingList, setLoadingList] = useState(true);
  const [dataState, setDataState] = useState(initForm);

  const [open, setOpen] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClickOpenDelete = item => {
    setDataState(item);
    setOpenDelete(true);
  };

  const handleClose = (event, reason) => {
    if (reason !== 'backdropClick') {
      setOpen(false);
      setDataState(initForm);
    }
  };
  const handleCloseDelete = (event, reason) => {
    if (reason !== 'backdropClick') {
      setOpenDelete(false);
      setDataState(initForm);
    }
  };

  const handleChangeTitle = (title, gender) => {
    setDataState({
      ...dataState,
      ShowTitle: title,
      Title: gender,
    });
  };

  const handleChange = event => {
    if (event.target.name === 'Name') {
      setDataState({
        ...dataState,
        [event.target.name]: event.target.value,
        ShowName: `${dataState.ShowTitle} ${event.target.value}`,
      });
    } else {
      setDataState({
        ...dataState,
        [event.target.name]: event.target.value,
      });
    }
  };

  const handleAdd = () => {
    const newData = [...props.dataState.contact];
    if (dataState.Name === '') {
      alert('data name belum diisi');
      return;
    }
    newData.push(dataState);
    props.handleChange(newData);
    handleClose();
  };

  const handleDelete = () => {
    const newData = [...props.dataState.contact];
    props.handleChange(
      _.filter(newData, function pick(n) {
        return n.Id !== dataState.Id;
      }),
    );
    handleCloseDelete();
  };

  return (
    <>
      <div style={{ textAlign: 'right' }}>
        <Button className={classes.button} variant="contained" color="secondary" onClick={handleClickOpen}>
          <AddIcon className={classes.leftIcon} />
          TAMBAH
        </Button>
      </div>
      {props.dataState.contact.map(item => {
        return (
          <PapperBlock title={item.CNumber} desc={item.ShowName}>
            <p style={{ display: 'flex' }}>
              <span style={{ width: '20%' }}>Phone </span>
              <span style={{ width: '80%' }}>: {item.Phone}</span>
            </p>
            <p style={{ display: 'flex' }}>
              <span style={{ width: '20%' }}>Mail </span>
              <span style={{ width: '80%' }}>: {item.Email}</span>
            </p>
            <p style={{ display: 'flex' }}>
              <span style={{ width: '20%' }}>Note </span>
              <span style={{ width: '80%' }}>: {item.Note}</span>
            </p>
            <div style={{ textAlign: 'right' }}>
              <Button
                className={classes.button}
                variant="contained"
                color="error"
                onClick={() => handleClickOpenDelete(item)}
              >
                <DeleteIcon />
              </Button>
            </div>
          </PapperBlock>
        );
      })}
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Tambah Contact</DialogTitle>
        <DialogContent>
          {/* <DialogContentText>
            To subscribe to this website, please enter your email address here. We will send updates occasionally.
          </DialogContentText> */}
          {/* <TextField autoFocus margin="dense" id="name" label="Email Address" type="email" fullWidth /> */}

          <div className={classes.root}>
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="partner">Partner</InputLabel>
              <Select
                value={dataState.ShowTitle}
                inputProps={{
                  name: 'ShowTitle',
                  id: 'partner',
                }}
              >
                <MenuItem value="">
                  <em>--Pilih Title--</em>
                </MenuItem>
                <MenuItem
                  value="Mr."
                  onClick={() => {
                    handleChangeTitle('Mr.', 'L');
                  }}
                >
                  <em>Mr.</em>
                </MenuItem>
                <MenuItem
                  value="Mrs. / Ms."
                  onClick={() => {
                    handleChangeTitle('Mrs. / Ms.', 'P');
                  }}
                >
                  <em>Mrs. / Ms.</em>
                </MenuItem>
              </Select>
            </FormControl>
            <FormControl className={classes.formControl} error={dataState.Name === ''}>
              <InputLabel htmlFor="name">Name</InputLabel>
              <Input
                inputProps={{
                  name: 'Name',
                  id: 'name',
                }}
                value={dataState.Name}
                onChange={handleChange}
              />
              {dataState.Name === '' ? (
                <FormHelperText>Harap isi name</FormHelperText>
              ) : (
                <FormHelperText>Contoh: John Doe</FormHelperText>
              )}
            </FormControl>
          </div>
          <div className={classes.root}>
            <FormControl className={classes.formControl} error={!emailValidation(dataState.Email)}>
              <InputLabel htmlFor="email">E-mail</InputLabel>
              <Input
                inputProps={{
                  name: 'Email',
                  id: 'email',
                }}
                value={dataState.Email}
                onChange={handleChange}
              />
              {!emailValidation(dataState.Email) ? (
                <FormHelperText>Format email salah</FormHelperText>
              ) : (
                <FormHelperText>Contoh: budianto@email.com</FormHelperText>
              )}
            </FormControl>
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="phone">Phone</InputLabel>
              <Input
                inputProps={{
                  name: 'Phone',
                  id: 'phone',
                }}
                value={dataState.Phone}
                onChange={handleChange}
              />
              <FormHelperText>Contoh: 08232145610</FormHelperText>
            </FormControl>
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="note">Note</InputLabel>
              <Input
                inputProps={{
                  name: 'Note',
                  id: 'note',
                }}
                value={dataState.Note}
                onChange={handleChange}
              />
              <FormHelperText>Contoh: Contact utama</FormHelperText>
            </FormControl>
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            CANCEL
          </Button>
          <Button onClick={() => handleAdd()} color="primary">
            ADD
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog open={openDelete} onClose={handleCloseDelete} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Delete Shareholder</DialogTitle>
        <DialogContent>
          <DialogContentText>Apakah Anda yakin akan menghapus shareholder?</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => handleDelete()} color="primary">
            YA
          </Button>
          <Button onClick={() => handleCloseDelete()} color="default">
            TIDAK
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

TabContacts.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TabContacts);
