import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Typography from '@material-ui/core/Typography';
import CheckIcon from '@material-ui/icons/Check';
import RefreshIcon from '@material-ui/icons/Refresh';
import _ from 'lodash';
import { useDispatch, useSelector } from 'react-redux';
import { NewPaperBlock, Loading } from 'dan-components';
import useGet from '../../../../hooks/useGet';
import usePost from '../../../../hooks/usePost';
import usePut from '../../../../hooks/usePut';
import { showNotifAction } from '../../../../redux/actions/notifActions';

import TabGeneral from './TabGeneral';
import TabLocation from './TabLocation';
import TabShareholders from './TabShareholders';
import TabContacts from './TabContacts';
import TabOthers from './TabOthers';
import TabOption from './TabOption';

import { withStyles } from '@material-ui/core/styles';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography component={'span'}>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node.isRequired,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`,
  };
}

const styles = theme => ({
  divider: {
    margin: `${theme.spacing(3)}px 0`,
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
  },
  rootTab: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 350,
  },
  button: {
    margin: theme.spacing(1),
  },
  leftIcon: {
    marginRight: theme.spacing(1),
  },
});

const requiredValidation = value => (value === null || value === '' ? false : true);

function DetailPartner(props) {
  const { classes } = props;
  const user = useSelector(state => state.login.usersLogin);
  const title = 'Detail Partner';
  const description = 'Halaman Detail Partner';
  const dispatch = useDispatch();
  const { responseGetData } = useGet();
  const { responsePostData } = usePost();
  const { responsePutData } = usePut();

  const initForm = {
    Id: '',
    main_PartnerNumber: '0000000000',
    main_PartnerName: '',
    general_PartnerForm: '',
    general_PartnerOwnership: '',
    general_Email: '',
    general_Phone: '',
    location_Country: '',
    location_Province: '',
    location_City: '',
    location_Street: '',
    location_BuildingNumber: '',
    location_PostalCode: '',
    other_BgOwner: '',
    other_ChrOwner: '',
    other_PrevBsns: '',
    other_Reason: '',
    other_Politically: '1',
    other_PaymentDay: '',
    other_SAPNo: '',
    shareHolder: [],
    contact: [],
    option_RowStatus: '0',
    CreateBy: user?.NRP,
  };

  const [loadingList, setLoadingList] = useState(true);
  const [initState, setInitState] = useState(initForm);
  const [dataState, setDataState] = useState(initForm);

  const [tabValue, setTabValue] = React.useState(0);

  const [listPartnerType, setListPartnerType] = useState([]);
  const [listPartnerOwnership, setListPartnerOwnership] = useState([]);
  const [listCountry, setListCountry] = useState([]);
  const [listProvince, setListProvince] = useState([]);

  const handleChangeTab = (event, newValue) => {
    setTabValue(newValue);
  };

  const fetchDetailPartner = async () => {
    const result = await responseGetData(`/api/partner/detail/${props.match.params.Id}`, false);
    if (!result) {
      return;
    }
    const temp = result.data;
    _.set(temp, 'CreateBy', user?.NRP);
    _.set(temp, 'shareHolder', result.data._shareholder);
    _.set(temp, 'contact', result.data._contactPerson);
    delete temp._shareholder;
    delete temp._contactPerson;
    setInitState(temp);
    setDataState(temp);
  };

  const fetchListPartnerType = async () => {
    const result = await responseGetData('/api/partner-type', false);
    if (!result) {
      setListPartnerType([]);
      return;
    }
    setListPartnerType(result);
  };

  const fetchListPartnerOwnership = async () => {
    const result = await responseGetData('/api/partner-ownership', false);
    if (!result) {
      setListPartnerOwnership([]);
      return;
    }
    setListPartnerOwnership(result);
  };

  const fetchListCountry = async () => {
    const result = await responseGetData('/api/country', false);
    if (!result) {
      setListCountry([]);
      return;
    }
    setListCountry(result);
  };

  const fetchListProvince = async () => {
    const result = await responseGetData('/api/province', false);
    if (!result) {
      setListProvince([]);
      return;
    }
    setListProvince(result);
  };

  useEffect(() => {
    async function fetchDropdown() {
      if (props.match.params.Id !== 'tambah') {
        fetchDetailPartner();
      }
      await fetchListPartnerType();
      await fetchListPartnerOwnership();
      await fetchListCountry();
      await fetchListProvince();
      setLoadingList(false);
    }
    fetchDropdown();
  }, []);

  const handleChange = event => {
    setDataState({
      ...dataState,
      [event.target.name]: event.target.value,
    });
  };

  const handleChangeCB = name => event => {
    setDataState({
      ...dataState,
      [name]: event.target.checked ? 1 : 0,
    });
  };

  const handleChangeTabShareholder = values => {
    setDataState({
      ...dataState,
      shareHolder: values,
    });
  };

  const handleChangeTabContact = values => {
    setDataState({
      ...dataState,
      contact: values,
    });
  };

  const onSubmit = async () => {
    if (dataState.main_PartnerNumber === '' || dataState.main_PartnerName === '') {
      dispatch(showNotifAction('Perhatikan lagi isian'));
      return;
    }
    let result = null;
    if (props.match.params.Id === 'tambah') {
      result = await responsePostData('/api/partner', dataState, true);
    } else {
      result = await responsePutData('/api/partner', dataState, true);
    }
    if (!result) {
      return;
    } else {
      props.history.push('/app/admin/master-data-partner');
    }
  };

  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
      {loadingList ? (
        <Loading />
      ) : (
        <NewPaperBlock style={{ paddingBottom: 0 }}>
          <div className={classes.root}>
            <FormControl className={classes.formControl} error={!requiredValidation(dataState.main_PartnerNumber)}>
              <InputLabel htmlFor="partner-number">Partner Number</InputLabel>
              <Input
                inputProps={{
                  name: 'main_PartnerNumber',
                  id: 'partner-number',
                }}
                value={dataState.main_PartnerNumber}
                onChange={handleChange}
                disabled
              />
              {!requiredValidation(dataState.main_PartnerNumber) ? (
                <FormHelperText>Harus diisi</FormHelperText>
              ) : (
                <FormHelperText>Partner number akan di-generate otomatis oleh sistem</FormHelperText>
              )}
            </FormControl>
            <FormControl className={classes.formControl} error={!requiredValidation(dataState.main_PartnerName)}>
              <InputLabel htmlFor="partner-name">Nama Partner</InputLabel>
              <Input
                inputProps={{
                  name: 'main_PartnerName',
                  id: 'partner-name',
                }}
                value={dataState.main_PartnerName}
                onChange={handleChange}
              />
              {!requiredValidation(dataState.main_PartnerName) ? (
                <FormHelperText>Harus diisi</FormHelperText>
              ) : (
                <FormHelperText>Contoh: Eclipse Indonesia</FormHelperText>
              )}
            </FormControl>
          </div>
          <br />
          <div className={classes.rootTab}>
            <AppBar position="static" color="default">
              <Tabs
                value={tabValue}
                onChange={handleChangeTab}
                indicatorColor="primary"
                textColor="primary"
                variant="scrollable"
                scrollButtons="auto"
                aria-label="scrollable auto tabs example"
              >
                <Tab label="General" {...a11yProps(0)} />
                <Tab label="Location" {...a11yProps(1)} />
                <Tab label="Shareholders" {...a11yProps(2)} />
                <Tab label="Contacts" {...a11yProps(3)} />
                <Tab label="Others" {...a11yProps(4)} />
                <Tab label="Option" {...a11yProps(5)} />
              </Tabs>
            </AppBar>
            <TabPanel value={tabValue} index={0}>
              <TabGeneral
                dataState={dataState}
                handleChange={handleChange}
                listPartnerType={listPartnerType}
                listPartnerOwnership={listPartnerOwnership}
              />
            </TabPanel>
            <TabPanel value={tabValue} index={1}>
              <TabLocation
                dataState={dataState}
                handleChange={handleChange}
                listCountry={listCountry}
                listProvince={listProvince}
              />
            </TabPanel>
            <TabPanel value={tabValue} index={2}>
              <TabShareholders dataState={dataState} handleChange={handleChangeTabShareholder} />
            </TabPanel>
            <TabPanel value={tabValue} index={3}>
              <TabContacts dataState={dataState} handleChange={handleChangeTabContact} />
            </TabPanel>
            <TabPanel value={tabValue} index={4}>
              <TabOthers dataState={dataState} handleChangeCB={e => handleChangeCB(e)} handleChange={handleChange} />
            </TabPanel>
            <TabPanel value={tabValue} index={5}>
              <TabOption dataState={dataState} handleChange={handleChange} />
            </TabPanel>
          </div>
          <div style={{ textAlign: 'center' }}>
            <Button
              className={classes.button}
              variant="contained"
              color="secondary"
              onClick={() => {
                onSubmit();
              }}
            >
              <CheckIcon className={classes.leftIcon} />
              SIMPAN
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              onClick={() => {
                setDataState(initState);
              }}
            >
              <RefreshIcon className={classes.leftIcon} />
              RESET
            </Button>
          </div>
        </NewPaperBlock>
      )}
    </div>
  );
}

DetailPartner.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DetailPartner);
