import React, { useState } from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import { NewPaperBlock } from 'dan-components';
import ListPartner from './ListPartner';

import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  divider: {
    margin: `${theme.spacing(3)}px 0`,
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 220,
  },
  picker: {
    margin: `${theme.spacing(3)}px 5px`,
  },
  button: {
    margin: theme.spacing(1),
  },
  leftIcon: {
    marginRight: theme.spacing(1),
  },
});

function Partner(props) {
  const { classes } = props;
  const title = 'Master Data Users';
  const description = 'Halaman Master Data Users';

  const [dataState, setDataState] = useState({
    age: '',
    name: 'hai',
  });
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [anchorEl, setAnchorEl] = useState(null);
  const [currentLocale, setCurrentLocale] = useState('id');

  const handleChange = event => {
    setDataState({
      ...dataState,
      [event.target.name]: event.target.value,
    });
  };
  const handleDateChange = date => {
    setSelectedDate(date);
  };
  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
      <NewPaperBlock style={{ paddingBottom: 0 }}>
        <div style={{ textAlign: 'right' }}>
          <Button
            className={classes.button}
            variant="contained"
            color="secondary"
          >
            <AddIcon className={classes.leftIcon} />
            TAMBAH BARU
          </Button>
        </div>
      </NewPaperBlock>
      <ListPartner />
    </div>
  );
}

Partner.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Partner);
