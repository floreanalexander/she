import React from 'react';
import PropTypes from 'prop-types';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import _ from 'lodash';

import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 350,
  },
});

const requiredValidation = value => (value === null || value === '' ? false : true);
const emailValidation = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? false : requiredValidation(value) ? true : false;

function TabLocation(props) {
  const { classes } = props;

  return (
    <>
      <div className={classes.root}>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="negara">Negara</InputLabel>
          <Select
            value={props.dataState.location_Country}
            onChange={props.handleChange}
            inputProps={{
              name: 'location_Country',
              id: 'negara',
            }}
          >
            <MenuItem value="">
              <em>--Pilih Negara--</em>
            </MenuItem>
            {props.listCountry.map(item => {
              return (
                <MenuItem key={item.Id} value={item.Id}>
                  {item.Name}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="provinsi">Provinsi</InputLabel>
          <Select
            value={props.dataState.location_Province}
            onChange={props.handleChange}
            inputProps={{
              name: 'location_Province',
              id: 'provinsi',
            }}
          >
            <MenuItem value="">
              <em>--Pilih Provinsi--</em>
            </MenuItem>
            {props.listProvince.map(item => {
              return (
                <MenuItem key={item.Id} value={item.Id}>
                  {item.Name}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
      </div>
      <div className={classes.root}>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="kota">Kota</InputLabel>
          <Input
            inputProps={{
              name: 'location_City',
              id: 'kota',
            }}
            value={props.dataState.location_City}
            onChange={props.handleChange}
          />
          <FormHelperText>Contoh: Depok</FormHelperText>
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="alamat">Alamat</InputLabel>
          <Input
            inputProps={{
              name: 'location_Street',
              id: 'alamat',
            }}
            value={props.dataState.location_Street}
            onChange={props.handleChange}
          />
          <FormHelperText>Contoh: Jl. Raya Bogor</FormHelperText>
        </FormControl>
      </div>
      <div className={classes.root}>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="nomor-gedung">Nomor Gedung</InputLabel>
          <Input
            inputProps={{
              name: 'location_BuildingNumber',
              id: 'nomor-gedung',
            }}
            value={props.dataState.location_BuildingNumber}
            onChange={props.handleChange}
          />
          <FormHelperText>Contoh: 25</FormHelperText>
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="kode-pos">Kode Pos</InputLabel>
          <Input
            inputProps={{
              name: 'location_PostalCode',
              id: 'kode-pos',
            }}
            value={props.dataState.location_PostalCode}
            onChange={props.handleChange}
          />
          <FormHelperText>Contoh: 13220</FormHelperText>
        </FormControl>
      </div>
    </>
  );
}

TabLocation.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TabLocation);
