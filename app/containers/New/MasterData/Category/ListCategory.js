import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListCategory(props) {
  const columns = [
    {
      name: 'Kode',
      options: {
        filter: true,
      },
    },
    {
      name: 'Nama Jenis Temuan',
      options: {
        filter: true,
      },
    },
    {
      name: 'Status',
      options: {
        filter: true,
      },
    },
    {
      name: 'Action',
      options: {
        filter: true,
        customBodyRender: value => {
          return (
            <div>
              <Button
                className={classes.button}
                variant="contained"
                color="secondary"
              >
                Choose
              </Button>
        </div>
          );
        },
      },
    },
  ];

  const data = [
    ['JO01', 'Alat Pelindung Diri', 'Active'],
    ['JO02', 'Scalfolding', 'Active'],
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Master Data Category" data={data} columns={columns} options={options} />
    </div>
  );
}

ListCategory.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListCategory);
