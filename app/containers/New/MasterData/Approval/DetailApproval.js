import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from '@material-ui/core/Select';
import CheckIcon from '@material-ui/icons/Check';
import RefreshIcon from '@material-ui/icons/Refresh';
import { useDispatch } from 'react-redux';
import { NewPaperBlock, Loading } from 'dan-components';
import useGet from '../../../../hooks/useGet';
import usePost from '../../../../hooks/usePost';
import usePut from '../../../../hooks/usePut';
import { showNotifAction } from '../../../../redux/actions/notifActions';

import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  divider: {
    margin: `${theme.spacing(3)}px 0`,
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 350,
  },
  picker: {
    margin: `${theme.spacing(3)}px 5px`,
  },
  button: {
    margin: theme.spacing(1),
  },
  leftIcon: {
    marginRight: theme.spacing(1),
  },
});

const initForm = {
  Id: '',
  AppType: '',
  CompanyId: '',
  StepNumber: '',
  StepType: '',
  EmployeeId: null,
  RowStatus: '',
  CreateBy: '',
};

function DetailApproval(props) {
  const { classes } = props;
  const title = 'Detail Approval';
  const description = 'Halaman Detail Approval';
  const dispatch = useDispatch();
  const { responseGetData } = useGet();
  const { responsePostData } = usePost();
  const { responsePutData } = usePut();

  const [loadingList, setLoadingList] = useState(true);
  const [initState, setInitState] = useState(initForm);
  const [dataState, setDataState] = useState(initForm);

  const [listPerusahaan, setListPerusahaan] = useState([]);
  const [listApproval, setListApproval] = useState([]);
  const [listPersonal, setListPersonal] = useState([]);

  const fetchDetailApproval = async () => {
    const result = await responseGetData(`/api/approval-step/detail/${props.match.params.Id}`, false);
    if (!result) {
      return;
    }
    setInitState(result.data);
    setDataState(result.data);
  };

  const fetchListPerusahaan = async () => {
    const result = await responseGetData('/api/company', false);
    if (!result) {
      setListPerusahaan([]);
      return;
    }
    setListPerusahaan(result);
  };

  const fetchListApproval = async () => {
    const result = await responseGetData('/api/pic', false);
    if (!result) {
      setListApproval([]);
      return;
    }
    setListApproval(result);
  };

  const fetchListPersonal = async () => {
    const result = await responsePostData('/api/users/maintain', { RowStatus: 1 }, false);
    if (!result) {
      setListPersonal([]);
      return;
    }
    setListPersonal(result.data);
  };

  useEffect(() => {
    async function fetchDropdown() {
      if (props.match.params.Id !== 'tambah') {
        fetchDetailApproval();
      }
      await fetchListPerusahaan();
      await fetchListApproval();
      await fetchListPersonal();
      setLoadingList(false);
    }
    fetchDropdown();
  }, []);

  const handleChange = event => {
    setDataState({
      ...dataState,
      [event.target.name]: event.target.value,
    });
  };

  const onSubmit = async () => {
    if (
      dataState.AppType === '' ||
      dataState.CompanyId === '' ||
      dataState.StepNumber === '' ||
      dataState.StepType === '' ||
      dataState.RowStatus === ''
    ) {
      dispatch(showNotifAction('Perhatikan lagi isian'));
      return;
    }
    if (dataState.StepType === 99 && !dataState.EmployeeId) {
      dispatch(showNotifAction('Harap isi Personel'));
      return;
    }
    let result = null;
    if (props.match.params.Id === 'tambah') {
      result = await responsePostData('/api/approval-step', dataState, true);
    } else {
      result = await responsePutData('/api/approval-step', dataState, true);
    }
    if (!result) {
      return;
    } else {
      props.history.push('/app/admin/master-data-approval');
    }
  };

  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
      {loadingList ? (
        <Loading />
      ) : (
        <NewPaperBlock style={{ paddingBottom: 0 }}>
          <div className={classes.root}>
            <FormControl className={classes.formControl} error={dataState.AppType === ''}>
              <InputLabel htmlFor="tipe">Tipe</InputLabel>
              <Select
                value={dataState.AppType}
                onChange={handleChange}
                inputProps={{
                  name: 'AppType',
                  id: 'tipe',
                }}
              >
                <MenuItem value="">
                  <em>--Pilih Tipe--</em>
                </MenuItem>
                <MenuItem value={'PROSPECTING'}>PROSPECTING</MenuItem>
              </Select>
              {dataState.AppType === '' && <FormHelperText>Harap Pilih Tipe</FormHelperText>}
            </FormControl>
            <FormControl className={classes.formControl} error={dataState.CompanyId === ''}>
              <InputLabel htmlFor="company">Perusahaan</InputLabel>
              <Select
                value={dataState.CompanyId}
                onChange={handleChange}
                inputProps={{
                  name: 'CompanyId',
                  id: 'company',
                }}
              >
                <MenuItem value="">
                  <em>--Pilih Perusahaan--</em>
                </MenuItem>
                {listPerusahaan.map(item => {
                  return <MenuItem value={item.Id}>{item.CompanyName}</MenuItem>;
                })}
              </Select>
              {dataState.CompanyId === '' && <FormHelperText>Harap Pilih Perusahaan</FormHelperText>}
            </FormControl>
          </div>
          <div className={classes.root}>
            <FormControl className={classes.formControl} error={dataState.StepNumber === ''}>
              <InputLabel htmlFor="step">Step Ke</InputLabel>
              <Select
                value={dataState.StepNumber}
                onChange={handleChange}
                inputProps={{
                  name: 'StepNumber',
                  id: 'step',
                }}
              >
                <MenuItem value="">
                  <em>--Pilih Step Ke--</em>
                </MenuItem>
                <MenuItem value={'1'}>1</MenuItem>
                <MenuItem value={'2'}>2</MenuItem>
                <MenuItem value={'3'}>3</MenuItem>
                <MenuItem value={'4'}>4</MenuItem>
                <MenuItem value={'5'}>5</MenuItem>
              </Select>
              {dataState.StepNumber === '' && <FormHelperText>Harap Pilih Step Ke</FormHelperText>}
            </FormControl>
            <FormControl className={classes.formControl} error={dataState.StepType === ''}>
              <InputLabel htmlFor="approval">Approval Oleh</InputLabel>
              <Select
                value={dataState.StepType}
                onChange={handleChange}
                inputProps={{
                  name: 'StepType',
                  id: 'approval',
                }}
              >
                <MenuItem value="">
                  <em>--Pilih Approval Oleh--</em>
                </MenuItem>
                {listApproval.map(item => {
                  return <MenuItem value={item.Id}>{item.Name}</MenuItem>;
                })}
                <MenuItem value={99}>Personal</MenuItem>
              </Select>
              {dataState.StepType === '' && <FormHelperText>Harap Pilih Approval Oleh</FormHelperText>}
            </FormControl>
          </div>
          <div className={classes.root}>
            <FormControl className={classes.formControl} error={dataState.StepType === 99 && !dataState.EmployeeId}>
              <InputLabel htmlFor="personel">Personal</InputLabel>
              <Select
                value={dataState.EmployeeId}
                onChange={handleChange}
                inputProps={{
                  name: 'EmployeeId',
                  id: 'personel',
                }}
              >
                <MenuItem value={null}>
                  <em>--Pilih Personal--</em>
                </MenuItem>
                {listPersonal.map(item => {
                  return <MenuItem value={item.Id}>{item.FullName}</MenuItem>;
                })}
              </Select>
              {dataState.StepType === 99 ? (
                <FormHelperText>Harap Pilih Personal</FormHelperText>
              ) : (
                <FormHelperText>Isi jika PIC Personal</FormHelperText>
              )}
            </FormControl>
            <FormControl className={classes.formControl} error={dataState.RowStatus === ''}>
              <InputLabel htmlFor="status">Status</InputLabel>
              <Select
                value={dataState.RowStatus}
                onChange={handleChange}
                inputProps={{
                  name: 'RowStatus',
                  id: 'status',
                }}
              >
                <MenuItem value="">
                  <em>--Pilih Status--</em>
                </MenuItem>
                <MenuItem value={'1'}>Aktif</MenuItem>
                <MenuItem value={'0'}>NonAktif</MenuItem>
              </Select>
              {dataState.RowStatus === '' && <FormHelperText>Harap Pilih Status</FormHelperText>}
            </FormControl>
          </div>
          <div style={{ textAlign: 'center' }}>
            <Button
              className={classes.button}
              variant="contained"
              color="secondary"
              onClick={() => {
                onSubmit();
              }}
            >
              <CheckIcon className={classes.leftIcon} />
              SIMPAN
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              onClick={() => {
                setDataState(initState);
              }}
            >
              <RefreshIcon className={classes.leftIcon} />
              RESET
            </Button>
          </div>
        </NewPaperBlock>
      )}
    </div>
  );
}

DetailApproval.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DetailApproval);
