import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListApproval(props) {
  const columns = [
    {
      name: 'Name',
      options: {
        filter: true,
      },
    },
    {
      name: 'Email',
      options: {
        filter: true,
      },
    },
    {
      name: 'Last Login',
      options: {
        filter: true,
      },
    },
    {
      name: 'Action',
      options: {
        filter: true,
        customBodyRender: value => {
          return (
            <div>
              <Button
                className={classes.button}
                variant="contained"
                color="secondary"
              >
                Choose
              </Button>
        </div>
          );
        },
      },
    },
  ];

  const data = [
    ['ABDULAH AMIN', 'abdulah.amin@acset.co', '30 Sep 2021'],
    ['Alan Damero Parhusip', 'alan.damero@acset.co', '18 Mei 2021'],
    ['Ardhitya Yudha Kusuma', 'ardhityayk@acset.co', '15 Agt 2022'],
    ['Bernardus Calvin','bernardius.calvin@acset.co', '01 Feb 1900'],
    ['Dicky Chandra Ardiansyah','dickyca@gmail.com', '27 Apr 2021'],
    ['EDI RIONO','edi.riono@acset.co', '13 Agt 2022'],
    ['HADI SASMITO','hadisasmito.dsm@gmail.com', '20 Agt 2022'],
    ['IMAM MUNANDAR RAHARJO','imammunandar03@gmail.com', '24 Agt 2022'],
    ['KARYADI','email37@gmail.com', '28 Jun 2022'],
    ['MOHD FADHIL BIN BURHAN','mohd.fadhil@acet.co', '20 Jun 2022'],
    ['Novera Meylinda','noveram@gmail.com', '05 Jan 2021'],
    ['Rendhy Octaviant Putra Y','rendhyhse@gmail.com', '27 Mei 2022'],
    ['RUSIN','rusin@acset.co', '19 Nov 2020'],
    ['SUGENG RIYANTO','sugengr@acset.co', '03 Des 2020'],
    ['Tri Sih Rachmanu','trisr@gmail.com', '29 Jan 2022']
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Master Data Users" data={data} columns={columns} options={options} />
    </div>
  );
}

ListApproval.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListApproval);
