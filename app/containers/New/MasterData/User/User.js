import React, { useState } from 'react';
import { Helmet } from 'react-helmet';
import brand from 'dan-api/dummy/brand';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import AddIcon from '@material-ui/icons/Add';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import { NewPaperBlock } from 'dan-components';
import ListUser from './ListUser';

import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  divider: {
    margin: `${theme.spacing(3)}px 0`,
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 220,
  },
  picker: {
    margin: `${theme.spacing(3)}px 5px`,
  },
  button: {
    margin: theme.spacing(1),
  },
  leftIcon: {
    marginRight: theme.spacing(1),
  },
});

function User(props) {
  const { classes } = props;
  const title = 'User';
  const description = 'Halaman User';

  const [dataState, setDataState] = useState({
    age: '',
    name: 'hai',
  });
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [anchorEl, setAnchorEl] = useState(null);
  const [currentLocale, setCurrentLocale] = useState('id');

  const handleChange = event => {
    setDataState({
      ...dataState,
      [event.target.name]: event.target.value,
    });
  };
  const handleDateChange = date => {
    setSelectedDate(date);
  };
  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
      <NewPaperBlock style={{ paddingBottom: 0 }}>
        <div style={{ textAlign: 'right' }}>
          <Button
            className={classes.button}
            variant="contained"
            color="secondary"
            onClick={() => {
              props.history.push('/app/admin/master-data-user/tambah');
            }}
          >
            <AddIcon className={classes.leftIcon} />
            TAMBAH BARU
          </Button>
        </div>
      </NewPaperBlock>
      <ListUser />
    </div>
  );
}

User.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(User));
