import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import Chip from '@material-ui/core/Chip';
import Checkbox from '@material-ui/core/Checkbox';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Select from '@material-ui/core/Select';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import CheckIcon from '@material-ui/icons/Check';
import RefreshIcon from '@material-ui/icons/Refresh';
import { useDispatch, useSelector } from 'react-redux';
import _ from 'lodash';
import { NewPaperBlock, Loading } from 'dan-components';
import useGet from '../../../../hooks/useGet';
import usePost from '../../../../hooks/usePost';
import { showNotifAction } from '../../../../redux/actions/notifActions';

import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  divider: {
    margin: `${theme.spacing(3)}px 0`,
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
  },
  multi: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 350,
  },
  picker: {
    margin: `${theme.spacing(3)}px 5px`,
  },
  button: {
    margin: theme.spacing(1),
  },
  leftIcon: {
    marginRight: theme.spacing(1),
  },
  group: {
    margin: `${theme.spacing(1)}px 0`,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const requiredValidation = value => (value === null || value === '' ? false : true);
const emailValidation = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? false : requiredValidation(value) ? true : false;

function DetailUser(props) {
  const { classes } = props;
  const user = useSelector(state => state.login.usersLogin);
  const title = 'Detail User';
  const description = 'Halaman Detail User';
  const dispatch = useDispatch();

  const initForm = {
    Id: '',
    Username: '',
    FullName: '',
    Gender: 'L',
    Phone: '',
    EmailPrivate: '',
    EmailOffice: '',
    CompanyID: '',
    RoleId: '',
    PicId: [],
    Locked: '0',
    RowStatus: '0',
    CreateBy: user?.NRP,
  };

  const { responseGetData } = useGet();
  const { responsePostData } = usePost();

  const [loadingList, setLoadingList] = useState(true);
  const [initState, setInitState] = useState(initForm);
  const [dataState, setDataState] = useState(initForm);

  const [listPerusahaan, setListPerusahaan] = useState([]);
  const [listRole, setListRole] = useState([]);
  const [listPIC, setListPIC] = useState([]);

  const fetchDetailUser = async () => {
    const result = await responseGetData(`/api/users/detail/${props.match.params.Id}`, false);
    if (!result) {
      return;
    }
    const tempPic = result.data.PIC.map(item => ({ item_id: item.masterPIC.Id, item_text: item.masterPIC.Name }));
    const temp = result.data;
    _.set(temp, 'PicId', tempPic);
    _.set(temp, 'CreateBy', user?.NRP);
    setInitState(temp);
    setDataState(temp);
  };

  const fetchListPerusahaan = async () => {
    const result = await responseGetData('/api/company', false);
    if (!result) {
      setListPerusahaan([]);
      return;
    }
    setListPerusahaan(result);
  };

  const fetchListRole = async () => {
    const result = await responseGetData('/api/role', false);
    if (!result) {
      setListRole([]);
      return;
    }
    setListRole(result);
  };

  const fetchListPIC = async () => {
    const result = await responseGetData('/api/pic', false);
    if (!result) {
      setListPIC([]);
      return;
    }
    setListPIC(result);
  };

  useEffect(() => {
    async function fetchDropdown() {
      if (props.match.params.Id !== 'tambah') {
        fetchDetailUser();
      }
      await fetchListPerusahaan();
      await fetchListRole();
      await fetchListPIC();
      setLoadingList(false);
    }
    fetchDropdown();
  }, []);

  const handleChange = event => {
    console.log('handle change', event.target.value);
    setDataState({
      ...dataState,
      [event.target.name]: event.target.value,
    });
  };

  const handleChangeMultiple = (event, selection) => {
    const newData = [...dataState.PicId];
    if (event.target.checked) {
      newData.push(selection);
      setDataState({
        ...dataState,
        [event.target.name]: newData,
      });
    } else {
      setDataState({
        ...dataState,
        [event.target.name]: _.filter(newData, function pick(n) {
          return n.item_id !== selection.item_id;
        }),
      });
    }
  };

  const onSubmit = async () => {
    if (
      dataState.Username === '' ||
      dataState.FullName === '' ||
      dataState.Phone === '' ||
      dataState.EmailPrivate === '' ||
      dataState.EmailOffice === '' ||
      dataState.CompanyID === '' ||
      dataState.RoleId === '' ||
      dataState.PicId.length === 0
    ) {
      dispatch(showNotifAction('Perhatikan lagi isian'));
      return;
    }
    let result = null;
    if (props.match.params.Id === 'tambah') {
      result = await responsePostData('/api/users/add', dataState, true);
    } else {
      result = await responsePostData('/api/users/update', dataState, true);
    }
    if (!result) {
      return;
    } else {
      props.history.push('/app/admin/master-data-user');
    }
  };

  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
      {loadingList ? (
        <Loading />
      ) : (
        <NewPaperBlock style={{ paddingBottom: 0 }}>
          <div className={classes.root}>
            <FormControl className={classes.formControl} error={!requiredValidation(dataState.Username)}>
              <InputLabel htmlFor="nrp">NRP</InputLabel>
              <Input
                inputProps={{
                  name: 'Username',
                  id: 'nrp',
                }}
                value={dataState.Username}
                onChange={handleChange}
                disabled={props.match.params.Id !== 'tambah'}
              />
              {!requiredValidation(dataState.Username) ? (
                <FormHelperText>Harus diisi</FormHelperText>
              ) : (
                <FormHelperText>NRP berupa 8 digit angka</FormHelperText>
              )}
            </FormControl>
            <FormControl className={classes.formControl} error={!requiredValidation(dataState.FullName)}>
              <InputLabel htmlFor="fullname">Nama Lengkap</InputLabel>
              <Input
                inputProps={{
                  name: 'FullName',
                  id: 'fullname',
                }}
                value={dataState.FullName}
                onChange={handleChange}
              />
              {!requiredValidation(dataState.FullName) ? (
                <FormHelperText>Harus diisi</FormHelperText>
              ) : (
                <FormHelperText>Contoh: John Doe</FormHelperText>
              )}
            </FormControl>
          </div>
          <div className={classes.root}>
            <FormControl component="fieldset" className={classes.formControl}>
              <FormLabel component="legend">Gender</FormLabel>
              <RadioGroup row className={classes.group} value={dataState.Gender} onChange={handleChange} name="Gender">
                <FormControlLabel value="L" control={<Radio />} label="Laki-laki" />
                <FormControlLabel value="P" control={<Radio />} label="Perempuan" />
              </RadioGroup>
            </FormControl>
            <FormControl className={classes.formControl} error={!requiredValidation(dataState.Phone)}>
              <InputLabel htmlFor="phone">Nomor HP</InputLabel>
              <Input
                inputProps={{
                  name: 'Phone',
                  id: 'phone',
                }}
                value={dataState.Phone}
                onChange={handleChange}
              />
              {!requiredValidation(dataState.Phone) ? (
                <FormHelperText>Harus diisi</FormHelperText>
              ) : (
                <FormHelperText>Contoh: 08232145610</FormHelperText>
              )}
            </FormControl>
          </div>
          <div className={classes.root}>
            <FormControl className={classes.formControl} error={!emailValidation(dataState.EmailPrivate)}>
              <InputLabel htmlFor="email-private">E-mail Pribadi</InputLabel>
              <Input
                inputProps={{
                  name: 'EmailPrivate',
                  id: 'email-private',
                }}
                value={dataState.EmailPrivate}
                onChange={handleChange}
              />
              {!emailValidation(dataState.EmailPrivate) ? (
                <FormHelperText>Harus diisi</FormHelperText>
              ) : (
                <FormHelperText>Contoh: johndoe@email.com</FormHelperText>
              )}
            </FormControl>
            <FormControl className={classes.formControl} error={!emailValidation(dataState.EmailOffice)}>
              <InputLabel htmlFor="email-office">E-mail Kantor</InputLabel>
              <Input
                inputProps={{
                  name: 'EmailOffice',
                  id: 'email-office',
                }}
                value={dataState.EmailOffice}
                onChange={handleChange}
              />
              {!emailValidation(dataState.EmailOffice) ? (
                <FormHelperText>Harus diisi</FormHelperText>
              ) : (
                <FormHelperText>Contoh: johndoe@email.com</FormHelperText>
              )}
            </FormControl>
          </div>
          <div className={classes.root}>
            <FormControl className={classes.formControl} error={dataState.CompanyID === ''}>
              <InputLabel htmlFor="company">Perusahaan</InputLabel>
              <Select
                value={dataState.CompanyID}
                onChange={handleChange}
                inputProps={{
                  name: 'CompanyID',
                  id: 'company',
                }}
              >
                <MenuItem value="">
                  <em>--Pilih Perusahaan--</em>
                </MenuItem>
                {listPerusahaan.map(item => {
                  return <MenuItem value={item.Id}>{item.CompanyName}</MenuItem>;
                })}
              </Select>
              {!requiredValidation(dataState.CompanyID) && <FormHelperText>Harap Pilih Perusahaan</FormHelperText>}
            </FormControl>
            <FormControl className={classes.formControl} error={dataState.RoleId === ''}>
              <InputLabel htmlFor="role">Role Akun</InputLabel>
              <Select
                value={dataState.RoleId}
                onChange={handleChange}
                inputProps={{
                  name: 'RoleId',
                  id: 'role',
                }}
              >
                <MenuItem value="">
                  <em>--Pilih Role Akun--</em>
                </MenuItem>
                {listRole.map(item => {
                  return <MenuItem value={item.Id}>{item.RoleName}</MenuItem>;
                })}
              </Select>
              {!requiredValidation(dataState.RoleId) && <FormHelperText>Harap Pilih Role Akun</FormHelperText>}
            </FormControl>
          </div>
          <div className={classes.root}>
            <FormControl
              className={classes.formControl}
              style={{ width: '100%', marginLeft: '10%', marginRight: '10%' }}
              error={dataState.PicId.length === 0}
            >
              <InputLabel id="pic-label">PIC</InputLabel>
              <Select
                labelId="pic-label"
                id="pic"
                multiple
                value={dataState.PicId}
                input={<Input id="select-multiple-pic" />}
                renderValue={selected => (
                  <div className={classes.chips}>
                    {selected.map(value => (
                      <Chip key={value.item_id} label={value.item_text} className={classes.chip} />
                    ))}
                  </div>
                )}
                MenuProps={MenuProps}
              >
                {listPIC.map(item => (
                  <div className={classes.multi}>
                    <Checkbox
                      checked={_.some(dataState.PicId, { item_id: item.Id, item_text: item.Name })}
                      onChange={e => handleChangeMultiple(e, { item_id: item.Id, item_text: item.Name })}
                      name="PicId"
                    />
                    <MenuItem key={item.Id} disabled>
                      {item.Name}
                    </MenuItem>
                  </div>
                ))}
              </Select>
              {dataState.PicId.length === 0 && <FormHelperText>PIC akun wajib diisi</FormHelperText>}
            </FormControl>
          </div>
          <div className={classes.root}>
            <FormControl component="fieldset" className={classes.formControl}>
              <FormLabel component="legend">Status Aktivasi</FormLabel>
              <RadioGroup
                row
                className={classes.group}
                value={dataState.RowStatus.toString()}
                onChange={handleChange}
                name="RowStatus"
              >
                <FormControlLabel value="1" control={<Radio />} label="Aktif" />
                <FormControlLabel value="0" control={<Radio />} label="Non-Aktif" />
              </RadioGroup>
            </FormControl>
            <FormControl component="fieldset" className={classes.formControl}>
              <FormLabel component="legend">Status Akun</FormLabel>
              <RadioGroup
                row
                className={classes.group}
                value={dataState.Locked.toString()}
                onChange={handleChange}
                name="Locked"
              >
                <FormControlLabel value="1" control={<Radio />} label="Terkunci" />
                <FormControlLabel value="0" control={<Radio />} label="Tidak Terkunci" />
              </RadioGroup>
            </FormControl>
          </div>
          <div style={{ textAlign: 'center' }}>
            <Button
              className={classes.button}
              variant="contained"
              color="secondary"
              onClick={() => {
                onSubmit();
              }}
            >
              <CheckIcon className={classes.leftIcon} />
              SIMPAN
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              onClick={() => {
                setDataState(initState);
              }}
            >
              <RefreshIcon className={classes.leftIcon} />
              RESET
            </Button>
          </div>
        </NewPaperBlock>
      )}
    </div>
  );
}

DetailUser.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DetailUser);
