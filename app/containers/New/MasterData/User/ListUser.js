import React, { useState, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import Chip from '@material-ui/core/Chip';
import MUIDataTable from 'mui-datatables';
import Loading from 'dan-components/Loading';
import usePost from '../../../../hooks/usePost';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListUser(props) {
  const { responsePostData } = usePost();
  const [loading, setLoading] = useState(true);
  const [list, setList] = useState([[]]);
  const columns = [
    {
      name: 'NRP',
      options: {
        filter: true,
      },
    },
    {
      name: 'Nama',
      options: {
        filter: true,
      },
    },
    {
      name: 'Perusahaan',
      options: {
        filter: true,
      },
    },
    {
      name: 'Status',
      options: {
        filter: true,
        customBodyRender: value => {
          if (value === 'Aktif') {
            return <Chip label="Aktif" color="secondary" />;
          }
          if (value === 'Non-aktif') {
            return <Chip label="Non-Aktif" color="primary" />;
          }
          return <Chip label="Unknown" color="primary" />;
        },
      },
    },
    {
      name: 'Action',
      options: {
        filter: false,
        customBodyRender: value => {
          return (
            <>
              <Tooltip title="Detail">
                <IconButton
                  color="primary"
                  onClick={() => {
                    props.history.push(`/app/admin/master-data-user/${value}`);
                  }}
                  className={classes.button}
                  aria-label="Detail"
                >
                  <Icon>edit</Icon>
                </IconButton>
              </Tooltip>
            </>
          );
        },
      },
    },
  ];

  const fetchList = async () => {
    const result = await responsePostData('/api/users/maintain', {}, false);
    if (!result) {
      setList([]);
      return;
    }
    var tempList = [];
    result.data.map(item =>
      tempList.push([item.NRP, item.FullName, item.Company.CompanyName, item.RowStatus, item.Id]),
    );
    console.log(tempList);
    setList(tempList);
  };

  useEffect(() => {
    async function fetch() {
      await fetchList();
      setLoading(false);
    }
    fetch();
  }, []);

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
    selectableRows: 'single', // multiple, single, none
    selectableRowsHideCheckboxes: true,
    selectableRowsOnClick: false,
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      {loading ? <Loading /> : <MUIDataTable title="List User" data={list} columns={columns} options={options} />}
    </div>
  );
}

ListUser.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(ListUser));
