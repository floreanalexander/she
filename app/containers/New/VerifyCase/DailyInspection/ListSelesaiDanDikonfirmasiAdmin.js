import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListSelesaiDanDikonfirmasiAdmin(props) {
  const columns = [
    {
      name: 'Ticket No',
      options: {
        filter: true,
      },
    },
    {
      name: 'Project',
      options: {
        filter: true,
      },
    },
    {
      name: 'Category',
      options: {
        filter: true,
      },
    },
    {
      name: 'Submitted Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Inspection Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Target Close',
      options: {
        filter: true,
      },
    },
    {
      name: 'Reported By',
      options: {
        filter: true,
      },
    },
    {
      name: 'PIC',
      options: {
        filter: true,
      },
    },
    {
      name: 'Action',
      options: {
        filter: true,
      },
    },
  ];

  const data = [
    ['C0JRD4BWLIVCN','JALAN TOL JAKARTA-CIKAMPEK II ELEVATED', 'Penggalian', '01 Okt 2018 20:20:54', '01 Okt 2018 20:21:15', '01 Okt 2018 20:19:54', 'Eindo Hanan P', 'Danang'],
    ['C0JRE2NPGGY1B','JALAN TOL JAKARTA-CIKAMPEK II ELEVATED', 'Bahaya Tertimpa Barang Jatuh', '02 Okt 2018 12:40:37', '02 Okt 2018 12:40:58', '02 Okt 2018 12:39:00', 'Eindo Hanan P', 'Danang'],
    ['C0JRE2OSQB144','JALAN TOL JAKARTA-CIKAMPEK II ELEVATED', 'Bahaya Kebakaran/Ledakan', '02 Okt 2018 12:54:51', '02 Okt 2018 12:55:12', '02 Okt 2018 12:54:00', 'Eindo Hanan P', 'Danang'],
    ['C0JUW32D418LM','MILLENIUM CENTENNIAL CENTER', 'Bahaya Tertimpa Barang Jatuh', '28 Nov 2018 14:30:32', '28 Nov 2018 14:30:26',  '28 Nov 2018 14:23:00', 'Mamun Munandar', 'Prasetyo Ari Widagdo'],
    ['C0JUY20LZ6BRZ','GRAND MANSION APARTMENT', 'Bahaya Orang Terjatuh', '30 Nov 2018 09:41:03', '30 Nov 2018 09:40:46', '30 Nov 2018 09:24:21', 'Eddy Budhiyanto', 'Imam Pujiono'],
    ['C0JUY26SHJ8D8','MILLENIUM CENTENNIAL CENTER', 'Housekeeping', '30 Nov 2018 10:21:14', '30 Nov 2018 10:21:14', '30 Nov 2018 10:16:14', 'Mamun Munandar', 'Prasetyo Ari Widagdo'],
    ['C0JWX2KWW9EL5','GRAND MANSION APARTMENT', 'Alat Pelindung Diri (APD)', '01 Des 2018 12:04:16', '30 Nov 2018 10:44:46', '30 Nov 2018 10:43:17', 'Iqbal Cahyadi', 'Imam Pujiono'],
    ['C0JWX2L2OEV9A','GRAND MANSION APARTMENT', 'Housekeeping', '01 Des 2018 12:06:25', '01 Des 2018 12:06:25', '01 Des 2018 12:03:29', 'Iqbal Cahyadi', 'Imam Pujiono'],
    ['C0JWX2LD6E1VK','GRAND MANSION APARTMENT', 'Bahaya Alat dan Perkakas', '01 Des 2018 12:10:03', '01 Des 2018 12:10:03', '01 Des 2018 12:06:34', 'Iqbal Cahyadi', 'Imam Pujiono'],
    ['C0JWZ2D872DM5','GRAND MANSION APARTMENT', 'Bahaya Orang Terjatuh', '03 Des 2018 11:04:55', '03 Des 2018 11:04:27', '03 Des 2018 10:59:25', 'Iqbal Cahyadi', 'Imam Pujiono'],  
    ['C0JWZ2DAIL8FA','GRAND MANSION APARTMENT', 'Bahaya Orang Terjatuh', '03 Des 2018 11:05:39', '03 Des 2018 11:04:27', '03 Des 2018 10:59:25', 'Iqbal Cahyadi', 'Imam Pujiono'],
    ['C0JWZ2DRA3LJK','MILLENIUM CENTENNIAL CENTER', 'Bahaya Tertimpa Barang Jatuh', '03 Des 2018 11:11:42', '03 Des 2018 11:11:45', '03 Des 2018 11:09:39', 'Mamun Munandar', 'Anang Setiawan	'],
    ['C0JWZ2KZV7M8Q','GRAND MANSION APARTMENT', 'Bahaya Alat dan Perkakas', '03 Des 2018 12:05:23', '03 Des 2018 12:05:25', '03 Des 2018 12:02:45', 'Iqbal Cahyadi', 'Imam Pujiono'],
    ['C0JX02MPOJRTR','GRAND MANSION APARTMENT', 'Terpapar/Bekerja dengan Bahaya Listrik', '04 Des 2018 12:27:49', '04 Des 2018 12:27:30', '04 Des 2018 12:25:49', 'Iqbal Cahyadi', 'Imam Pujiono'],
    ['C0JX11TMG3AXJ','GRAND MANSION APARTMENT', 'Terpapar/Bekerja dengan Bahaya Listrik	', '05 Des 2018 08:50:48', '05 Des 2018 08:50:42', '05 Des 2018 08:48:14', 'Iqbal Cahyadi', 'Imam Pujiono'],
    ['C0JRD4BWLIVCN','JALAN TOL JAKARTA-CIKAMPEK II ELEVATED', 'Penggalian', '01 Okt 2018 20:20:54', '01 Okt 2018 20:21:15', '01 Okt 2018 20:19:54', 'Eindo Hanan P', 'Danang'],
    ['C0JRE2NPGGY1B','JALAN TOL JAKARTA-CIKAMPEK II ELEVATED', 'Bahaya Tertimpa Barang Jatuh', '02 Okt 2018 12:40:37', '02 Okt 2018 12:40:58', '02 Okt 2018 12:39:00', 'Eindo Hanan P', 'Danang'],
    ['C0JRE2OSQB144','JALAN TOL JAKARTA-CIKAMPEK II ELEVATED', 'Bahaya Kebakaran/Ledakan', '02 Okt 2018 12:54:51', '02 Okt 2018 12:55:12', '02 Okt 2018 12:54:00', 'Eindo Hanan P', 'Danang'],
    ['C0JUW32D418LM','MILLENIUM CENTENNIAL CENTER', 'Bahaya Tertimpa Barang Jatuh', '28 Nov 2018 14:30:32', '28 Nov 2018 14:30:26', '28 Nov 2018 14:23:00', 'Mamun Munandar', 'Prasetyo Ari Widagdo'],
    ['C0JUY20LZ6BRZ','GRAND MANSION APARTMENT', 'Bahaya Orang Terjatuh', '30 Nov 2018 09:41:03', '30 Nov 2018 09:40:46', '30 Nov 2018 09:24:21', 'Eddy Budhiyanto', 'Imam Pujiono'],
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Selesai dan Dikonfirmasi Admin" data={data} columns={columns} options={options} />
    </div>
  );
}

ListSelesaiDanDikonfirmasiAdmin.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListSelesaiDanDikonfirmasiAdmin);