import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListDitugaskanKePIC(props) {
  const columns = [
    {
      name: 'Ticket No',
      options: {
        filter: true,
      },
    },
    {
      name: 'Project',
      options: {
        filter: true,
      },
    },
    {
      name: 'Category',
      options: {
        filter: true,
      },
    },
    {
      name: 'Submitted Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Inspection Date',
      options: {
        filter: true,
      },
    },{
      name: 'Target Close',
      options: {
        filter: true,
      },
    },
    {
      name: 'Reported By',
      options: {
        filter: true,
      },
    },
    {
      name: 'PIC',
      options: {
        filter: true,
      },
    },
    {
      name: 'Action',
      options: {
        filter: true,
      },
    },
  ];

  const data = [
    ['C0JRE3NIWKYOB','JALAN TOL JAKARTA-CIKAMPEK II ELEVATED', 'Terpapar/Bersentuhan dengan Bahan Berbahaya Beracun', '02 Okt 2018 17:04:57', '02 Okt 2018 17:05:18', '02 Okt 2018 17:04:03', 'Edi S', 'Danang'],
    ['C0JRS286MAZ6S','JALAN TOL KUNCIRAN SERPONG', 'Housekeeping', '16 Okt 2018 10:39:19', '16 Okt 2018 10:39:18', '16 Okt 2018 10:35:43', 'Herdy', 'Asep Cahyana'],
    ['C0JRS28STDZ3M','JALAN TOL KUNCIRAN SERPONG', 'Alat Pelindung Diri (APD)', '16 Okt 2018 10:47:18', '16 Okt 2018 10:47:17', '16 Okt 2018 10:39:23', 'Herdy', 'Eka Puspitasari'],
    ['C0JRT21XAG1PN','JALAN TOL KUNCIRAN SERPONG', 'Housekeeping', '17 Okt 2018 09:58:07', '17 Okt 2018 09:58:08',  '17 Okt 2018 09:40:02', 'Herdy', 'Asep Cahyana'],
    ['C0JRT25CQGIRP','JALAN TOL KUNCIRAN SERPONG', 'Bahaya Orang Terjatuh', '17 Okt 2018 10:02:51', '17 Okt 2018 10:02:52', '17 Okt 2018 09:58:19', 'Herdy', 'Asep Cahyana'],
    ['C0JRU2G5CIHSR','JALAN TOL KUNCIRAN SERPONG', 'Alat Pelindung Diri (APD)', '18 Okt 2018 11:42:41', '18 Okt 2018 11:42:44', '18 Okt 2018 11:38:32	', 'Herdy', 'Eka Puspitasari'],
    ['C0JRU2GPAH028','JALAN TOL KUNCIRAN SERPONG', 'Scaffolding/Pekerjaan Struktur Sementara', '18 Okt 2018 11:49:59', '18 Okt 2018 11:50:02', '18 Okt 2018 11:42:48', 'Herdy', 'Eka Puspitasari'],
    ['C0JRV2DQEJKNP','JALAN TOL KUNCIRAN SERPONG', 'Bahaya Orang Terjatuh', '19 Okt 2018 11:11:11', '19 Okt 2018 11:11:15', '19 Okt 2018 11:07:39', 'Herdy', 'Asep Cahyana'],
    ['C0JRV2E4T1E9N','JALAN TOL KUNCIRAN SERPONG', 'Alat Pelindung Diri (APD)', '19 Okt 2018 11:16:29', '19 Okt 2018 11:16:33', '19 Okt 2018 11:11:55', 'Herdy', 'Eka Puspitasari'],
    ['C0JRV2ELLAAWR','JALAN TOL KUNCIRAN SERPONG', 'Alat Pelindung Diri (APD)', '19 Okt 2018 11:22:33', '19 Okt 2018 11:22:38', '19 Okt 2018 11:16:41', 'Herdy', 'Eka Puspitasari'],
    ['C0JRW2DDQCQ8V','JALAN TOL KUNCIRAN SERPONG', 'Housekeeping', '20 Okt 2018 11:06:55', '20 Okt 2018 11:07:01', '20 Okt 2018 11:04:59', 'Herdy', 'Eka Puspitasari'],
    ['C0JRW2DLFFNL6','JALAN TOL KUNCIRAN SERPONG', 'Scaffolding/Pekerjaan Struktur Sementara', '20 Okt 2018 11:09:32', '20 Okt 2018 11:09:38', '20 Okt 2018 11:07:50', 'Herdy', 'Arezwan Mutiar'],
    ['C0JRY2NRB9JEF','JALAN TOL KUNCIRAN SERPONG', 'Housekeeping', '22 Okt 2018 12:41:03', '22 Okt 2018 12:41:13', '22 Okt 2018 12:38:48', 'Herdy', 'Asep Cahyana'],
    ['C0JRY2O0H2F5X','JALAN TOL KUNCIRAN SERPONG', 'Alat Pelindung Diri (APD)', '22 Okt 2018 12:44:33', '22 Okt 2018 12:44:42', '22 Okt 2018 12:41:19', 'Herdy', 'Eka Puspitasari'],
    ['C0JRY2OG9IYLP','JALAN TOL KUNCIRAN SERPONG', 'Alat Pelindung Diri (APD)', '22 Okt 2018 12:50:02', '22 Okt 2018 12:50:11', '22 Okt 2018 12:44:46', 'Herdy', 'Eka Puspitasari'],
    ['C0JRZ1RM88CDD','JALAN TOL KUNCIRAN SERPONG', 'Housekeeping', '23 Okt 2018 08:24:48', '23 Okt 2018 08:24:48', '23 Okt 2018 08:21:58', 'Herdy', 'Arezwan Mutiar'],
    ['C0JRZ1S7B8WIF','JALAN TOL KUNCIRAN SERPONG', 'Fasilitas Umum', '23 Okt 2018 08:32:07', '23 Okt 2018 08:32:07', '23 Okt 2018 08:26:03', 'Herdy', 'Arezwan Mutiar'],
    ['C0JS02DQN9ULN','JALAN TOL KUNCIRAN SERPONG', 'Scaffolding/Pekerjaan Struktur Sementara', '24 Okt 2018 11:11:19', '24 Okt 2018 11:11:20', '24 Okt 2018 11:00:56', 'Herdy', 'Asep Cahyana'],
    ['C0JS128CPC60I','JALAN TOL KUNCIRAN SERPONG', 'Alat Pelindung Diri (APD)', '25 Okt 2018 10:41:38', '25 Okt 2018 10:41:40', '25 Okt 2018 10:39:02', 'Herdy', 'Eka Puspitasari'],
    ['C0JS128O5GDZS','JALAN TOL KUNCIRAN SERPONG', 'Housekeeping', '25 Okt 2018 10:45:50', '25 Okt 2018 10:45:52', '25 Okt 2018 10:43:42', 'Herdy', 'Arezwan Mutiar'],
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Ditugaskan ke PIC" data={data} columns={columns} options={options} />
    </div>
  );
}

ListDitugaskanKePIC.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListDitugaskanKePIC);