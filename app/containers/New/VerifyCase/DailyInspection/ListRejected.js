import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListRejected(props) {
  const columns = [
    {
      name: 'Ticket No',
      options: {
        filter: true,
      },
    },
    {
      name: 'Project',
      options: {
        filter: true,
      },
    },
    {
      name: 'Category',
      options: {
        filter: true,
      },
    },
    {
      name: 'Submitted Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Inspection Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Target Close',
      options: {
        filter: true,
      },
    },
    {
      name: 'Reported By',
      options: {
        filter: true,
      },
    },
    {
      name: 'Action',
      options: {
        filter: true,
      },
    },
  ];

  const data = [
    ['C0JRI2LLRG2NJ','WORKSHOP CILEUNGSI', 'Bahaya Orang Terjatuh', '06 Okt 2018 12:13:12', '06 Okt 2018 12:13:40', '06 Okt 2018 12:08:42', 'Imam AsmuI'],
    ['C0JRM301V6JP3','WORKSHOP CILEUNGSI', 'Bahaya Tertimpa Barang Jatuh', '10 Okt 2018 14:00:35', '10 Okt 2018 14:00:32', '10 Okt 2018 13:57:47', 'Riyanto'],
    ['C0JRT39LU8414','JALAN TOL JAKARTA-CIKAMPEK II ELEVATED', 'Bahaya Orang Terjatuh', '17 Okt 2018 15:24:19', '17 Okt 2018 15:24:00', '18 Okt 2018 15:22:52', 'Danang'],
    ['C0JRV2N5ZKX00','HEAD OFFICE', 'Penggalian', '19 Okt 2018 12:33:36', '19 Okt 2018 12:33:40',  '19 Okt 2018 12:32:51', 'Wijanarko'],
    ['C0JRZ3G1AF1K0','GRAND MANSION APARTMENT', 'Penggalian', '23 Okt 2018 16:07:51', '23 Okt 2018 16:07:49', '23 Okt 2018 16:06:28', 'Edi S'],
    ['C0JUA34MDEJ33','MILLENIUM CENTENNIAL CENTER', 'Bahaya Pengangkatan', '06 Nov 2018 14:59:58', '06 Nov 2018 14:59:52', '06 Nov 2018 14:58:37', 'Hastia Tira Apecta'],
    ['C0JUA3JYIJN29','THAMRIN NINE', 'Bahaya Orang Terjatuh', '06 Nov 2018 16:58:35', '06 Nov 2018 16:58:31', '06 Nov 2018 16:57:24', 'Bambang S'],
    ['C0JWZ1Y3G1BE4','MILLENIUM CENTENNIAL CENTER', 'Housekeeping', '03 Des 2018 09:08:44', '03 Des 2018 11:02:51', '03 Des 2018 09:05:08', 'Moch "dailyinspection"."tickets" Koiru Iksanudin'],
    ['C0JX21S9WI23B','MILLENIUM CENTENNIAL CENTER', 'Housekeeping', '06 Des 2018 08:33:01', '06 Des 2018 08:32:58', '06 Des 2018 08:31:45', 'Mamun Munandar'],
    ['C0JXH301O1T46','GRAND MANSION APARTMENT', 'Bahaya Orang Terjatuh', '21 Des 2018 14:00:28', '21 Des 2018 14:00:10', '21 Des 2018 13:58:04', 'Eddy Budhiyanto'],
    ['C0JRI2LLRG2NJ','WORKSHOP CILEUNGSI', 'Bahaya Orang Terjatuh', '06 Okt 2018 12:13:12', '06 Okt 2018 12:13:40', '06 Okt 2018 12:08:42', 'Imam AsmuI'],
    ['C0JRM301V6JP3','WORKSHOP CILEUNGSI', 'Bahaya Tertimpa Barang Jatuh', '10 Okt 2018 14:00:35', '10 Okt 2018 14:00:32', '10 Okt 2018 13:57:47', 'Riyanto'],
    ['C0JRT39LU8414','JALAN TOL JAKARTA-CIKAMPEK II ELEVATED', 'Bahaya Orang Terjatuh', '17 Okt 2018 15:24:19', '17 Okt 2018 15:24:00', '18 Okt 2018 15:22:52', 'Danang'],
    ['C0JRV2N5ZKX00','HEAD OFFICE', 'Penggalian', '19 Okt 2018 12:33:36', '19 Okt 2018 12:33:40',  '19 Okt 2018 12:32:51', 'Wijanarko'],
    ['C0JRZ3G1AF1K0','GRAND MANSION APARTMENT', 'Penggalian', '23 Okt 2018 16:07:51', '23 Okt 2018 16:07:49', '23 Okt 2018 16:06:28', 'Edi S'],
    ['C0JUA34MDEJ33','MILLENIUM CENTENNIAL CENTER', 'Bahaya Pengangkatan', '06 Nov 2018 14:59:58', '06 Nov 2018 14:59:52', '06 Nov 2018 14:58:37', 'Hastia Tira Apecta'],
    ['C0JUA3JYIJN29','THAMRIN NINE', 'Bahaya Orang Terjatuh', '06 Nov 2018 16:58:35', '06 Nov 2018 16:58:31', '06 Nov 2018 16:57:24', 'Bambang S'],
    ['C0JWZ1Y3G1BE4','MILLENIUM CENTENNIAL CENTER', 'Housekeeping', '03 Des 2018 09:08:44', '03 Des 2018 11:02:51', '03 Des 2018 09:05:08', 'Moch "dailyinspection"."tickets" Koiru Iksanudin'],
    ['C0JX21S9WI23B','MILLENIUM CENTENNIAL CENTER', 'Housekeeping', '06 Des 2018 08:33:01', '06 Des 2018 08:32:58', '06 Des 2018 08:31:45', 'Mamun Munandar'],
    ['C0JXH301O1T46','GRAND MANSION APARTMENT', 'Bahaya Orang Terjatuh', '21 Des 2018 14:00:28', '21 Des 2018 14:00:10', '21 Des 2018 13:58:04', 'Eddy Budhiyanto'],
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Rejected" data={data} columns={columns} options={options} />
    </div>
  );
}

ListRejected.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListRejected);