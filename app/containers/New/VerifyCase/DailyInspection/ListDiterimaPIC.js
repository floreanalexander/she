import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListDiterimaPIC(props) {
  const columns = [
    {
      name: 'Ticket No',
      options: {
        filter: true,
      },
    },
    {
      name: 'Project',
      options: {
        filter: true,
      },
    },
    {
      name: 'Category',
      options: {
        filter: true,
      },
    },
    {
      name: 'Submitted Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Inspection Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Target Close',
      options: {
        filter: true,
      },
    },
    {
      name: 'Reported By',
      options: {
        filter: true,
      },
    },
    {
      name: 'PIC',
      options: {
        filter: true,
      },
    },
    {
      name: 'Action',
      options: {
        filter: true,
      },
    },
  ];

  const data = [
    ['C0JS128I8DE9M','JALAN TOL KUNCIRAN SERPONG', 'Scaffolding/Pekerjaan Struktur Sementara', '25 Okt 2018 10:43:37', '25 Okt 2018 10:43:39', '25 Okt 2018 10:41:44', 'Herdy', 'Asep Cahyana'],
    ['C0JUU2FF87Q0J','JALAN TOL KUNCIRAN SERPONG', 'Scaffolding/Pekerjaan Struktur Sementara', '26 Nov 2018 11:33:00', '26 Nov 2018 11:33:08', '26 Nov 2018 11:23:23', 'Herdy', 'Asep Cahyana'],
    ['C0JUU2GD67VDO','JALAN TOL KUNCIRAN SERPONG', 'Penggalian', '26 Nov 2018 11:45:22', '26 Nov 2018 11:45:30', '26 Nov 2018 11:33:53', 'Herdy', 'Asep Cahyana'],
    ['C0JUX27W94DMI','MILLENIUM CENTENNIAL CENTER', 'Bahaya Tertimpa Barang Jatuh', '29 Nov 2018 10:35:45', '29 Nov 2018 10:35:43',  '29 Nov 2018 10:32:55', 'Mamun Munandar', 'Prasetyo Ari Widagdo'],
    ['C0JUX281G4BLD','MILLENIUM CENTENNIAL CENTER', 'Bahaya Tertimpa Barang Jatuh', '29 Nov 2018 10:37:32', '29 Nov 2018 10:37:29', '29 Nov 2018 10:35:53', 'Mamun Munandar', 'Prasetyo Ari Widagdo'],
    ['C0JUX29EOJ84A','MILLENIUM CENTENNIAL CENTER', 'Bahaya Tertimpa Barang Jatuh', '29 Nov 2018 10:55:05', '29 Nov 2018 10:54:53', '29 Nov 2018 10:53:24', 'Moch "dailyinspection"."tickets" Koiru Iksanudin', 'Prasetyo Ari Widagdo'],
    ['C0JUY25KW80BZ','MILLENIUM CENTENNIAL CENTER', 'Housekeeping', '30 Nov 2018 10:05:44', '30 Nov 2018 10:05:42', '30 Nov 2018 10:02:36', 'Mamun Munandar', 'Prasetyo Ari Widagdo'],
    ['C0JUY25Y52DDB','MILLENIUM CENTENNIAL CENTER', 'Bahaya Tertimpa Barang Jatuh', '30 Nov 2018 10:10:21', '30 Nov 2018 10:10:16', '30 Nov 2018 10:06:01', 'Mamun Munandar', 'Prasetyo Ari Widagdo'],
    ['C0JWX1YZM3LKU','MILLENIUM CENTENNIAL CENTER', 'Bahaya Tertimpa Barang Jatuh', '01 Des 2018 09:20:02', '01 Des 2018 09:20:03', '01 Des 2018 09:16:52', 'Mamun Munandar', 'Anang Setiawan'],
    ['C0JWX1Z3R8KTP','MILLENIUM CENTENNIAL CENTER', 'Bahaya Tertimpa Barang Jatuh', '01 Des 2018 09:21:51', '01 Des 2018 09:21:56', '01 Des 2018 09:20:13', 'Mamun Munandar', 'Anang Setiawan'],  
    ['C0JX01SUZHNRY','MILLENIUM CENTENNIAL CENTER', 'Bahaya Tertimpa Barang Jatuh', '04 Des 2018 08:41:00', '04 Des 2018 08:39:06', '04 Des 2018 08:37:17', 'Mamun Munandar', 'Anang Setiawan'],
    ['C0JX01U66AT0C','MILLENIUM CENTENNIAL CENTER', 'Bahaya Tertimpa Barang Jatuh', '04 Des 2018 08:57:59', '04 Des 2018 08:58:03', '04 Des 2018 08:56:42', 'Mamun Munandar', 'Anang Setiawan'],
    ['C0JX02MH679BX','GRAND MANSION APARTMENT', 'Bahaya Terinjak, tersandung, terbentur oleh Benda', '04 Des 2018 12:24:42', '04 Des 2018 12:24:50', '04 Des 2018 12:23:00', 'Iqbal Cahyadi', 'Deni Irawan'],
    ['C0JX22NVLJU4K','JALAN TOL KUNCIRAN SERPONG', 'Housekeeping', '06 Des 2018 12:42:58', '06 Des 2018 12:42:58', '06 Des 2018 12:41:17', 'Herdy', 'Asep Cahyana'],
    ['C0JX32D5H3Y81','MILLENIUM CENTENNIAL CENTER', 'Housekeeping', '07 Des 2018 11:03:57', '07 Des 2018 11:03:49', '07 Des 2018 11:01:55', 'Mamun Munandar', 'Prasetyo Ari Widagdo'],
    ['C0JX32DB3HUKP','MILLENIUM CENTENNIAL CENTER', 'Bahaya Tertimpa Barang Jatuh', '07 Des 2018 11:06:00', '07 Des 2018 11:05:53', '07 Des 2018 11:04:27', 'Mamun Munandar', 'Anang Setiawan'],
    ['C0JX32E2TCFM9','MILLENIUM CENTENNIAL CENTER', 'Bahaya Tertimpa Barang Jatuh', '07 Des 2018 11:15:58', '07 Des 2018 11:15:47', '07 Des 2018 11:12:08', 'Mamun Munandar', 'Prasetyo Ari Widagdo'],
    ['C0JX32EA345BI','MILLENIUM CENTENNIAL CENTER', 'Terpapar/Bekerja dengan Bahaya Listrik', '07 Des 2018 11:18:19', '07 Des 2018 11:18:15', '07 Des 2018 11:16:42', 'Ni Made Ayu', 'Prasetyo Ari Widagdo'],
    ['C0JX41SRBL0NT','MILLENIUM CENTENNIAL CENTER', 'Bahaya Tertimpa Barang Jatuh', '08 Des 2018 08:39:28', '08 Des 2018 08:39:28', '08 Des 2018 08:37:28', 'Mamun Munandar', 'Anang Setiawan'],
    ['C0JX41Z2IJCHM','MILLENIUM CENTENNIAL CENTER', 'Bahaya Tertimpa Barang Jatuh', '08 Des 2018 09:21:07', '08 Des 2018 09:21:06', '08 Des 2018 08:41:52', 'Mamun Munandar', 'Anang Setiawan'],
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Diterima PIC" data={data} columns={columns} options={options} />
    </div>
  );
}

ListDiterimaPIC.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListDiterimaPIC);