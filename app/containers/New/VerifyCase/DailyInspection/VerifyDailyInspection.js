import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Hidden from '@material-ui/core/Hidden';
import Badge from '@material-ui/core/Badge';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import styles from '../../../../components/Widget/widget-jss';

import { Helmet } from 'react-helmet';
import { NewPaperBlock } from 'dan-components';
import ListTerkirimKeAdmin from './ListTerkirimKeAdmin';
import ListDitugaskanKePIC from './ListDitugaskanKePIC';
import ListDiterimaPIC from './ListDiterimaPIC';
import ListDiselesaikanPIC from './ListDiselesaikanPIC';
import ListSelesaiDanDikonfirmasiAdmin from './ListSelesaiDanDikonfirmasiAdmin';
import ListRejected from './ListRejected';


/* Tab Container */
function TabContainer(props) {
  const { children } = props;
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};
/* END Tab Container */

/* Terkirim ke Admin */
function TerkirimKeAdmin(props) {
  const title = 'Verify Daily Inspection - Terkirim ke Admin';
  const description = 'Halaman Verify Daily Inspection';
  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
    <NewPaperBlock>
      <ListTerkirimKeAdmin />
    </NewPaperBlock>
    </div>
  );
}

TerkirimKeAdmin.propTypes = {
  classes: PropTypes.object.isRequired,
  openMenu: PropTypes.func.isRequired,
};

const TerkirimKeAdminStyled = withStyles(styles)(TerkirimKeAdmin);
/* END Terkirim ke Admin */

/* Ditugaskan ke PIC */
function DitugaskanKePIC(props) {
  const title = 'Verify Daily Inspection - Ditugaskan ke PIC';
  const description = 'Halaman Verify Daily Inspection';
  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
    <NewPaperBlock>
      <ListDitugaskanKePIC />
    </NewPaperBlock>
    </div>
  );
}

DitugaskanKePIC.propTypes = {
  classes: PropTypes.object.isRequired,
};

const DitugaskanKePICStyled = withStyles(styles)(DitugaskanKePIC);
/* END Ditugaskan ke PIC */

/* Diterima PIC */
function DiterimaPIC(props) {
  const title = 'Verify Daily Inspection - Diterima PIC';
  const description = 'Halaman Verify Daily Inspection';
  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
    <NewPaperBlock>
      <ListDiterimaPIC />
    </NewPaperBlock>
    </div>
  );
}

DiterimaPIC.propTypes = {
  classes: PropTypes.object.isRequired,
  openMenu: PropTypes.func.isRequired,
};

const DiterimaPICStyled = withStyles(styles)(DiterimaPIC);
/* Diterima PIC */

/* Diselesaikan PIC */
function DiselesaikanPIC(props) {
  const title = 'Verify Daily Inspection - Diselesaikan PIC';
  const description = 'Halaman Verify Daily Inspection';
  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
    <NewPaperBlock>
      <ListDiselesaikanPIC />
    </NewPaperBlock>
    </div>
  );
}

DiselesaikanPIC.propTypes = {
  classes: PropTypes.object.isRequired,
  openMenu: PropTypes.func.isRequired,
};

const DiselesaikanPICStyled = withStyles(styles)(DiselesaikanPIC);
/* Diselesaikan PIC */

/* Selesai dan Dikonfirmasi Admin */
function SelesaiDanDikonfirmasiAdmin(props) {
  const title = 'Verify Daily Inspection - Selesai dan Dikonfirmasi Admin';
  const description = 'Halaman Verify Daily Inspection';
  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
    <NewPaperBlock>
      <ListSelesaiDanDikonfirmasiAdmin />
    </NewPaperBlock>
    </div>
  );
}

SelesaiDanDikonfirmasiAdmin.propTypes = {
  classes: PropTypes.object.isRequired,
  openMenu: PropTypes.func.isRequired,
};

const SelesaiDanDikonfirmasiAdminStyled = withStyles(styles)(SelesaiDanDikonfirmasiAdmin);
/* Selesai dan Dikonfirmasi Admin */

/* Rejected */
function Rejected(props) {
  const title = 'Verify Daily Inspection - Rejected';
  const description = 'Halaman Verify Daily Inspection';
  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
    <NewPaperBlock>
      <ListRejected />
    </NewPaperBlock>
    </div>
  );
}

Rejected.propTypes = {
  classes: PropTypes.object.isRequired,
  openMenu: PropTypes.func.isRequired,
};

const RejectedStyled = withStyles(styles)(Rejected);
/* Rejected */

function VerifyDailyInspection(props) {
  const [value, setValue] = useState(0);
  const [anchorEl, setAnchorEl] = useState(null);
  const [anchorElAction, setAnchorElAction] = useState(null);

  const handleChange = (event, val) => {
    setValue(val);
  };

  const handleOpen = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleOpenAction = event => {
    setAnchorElAction(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
    setAnchorElAction(null);
  };

  const { classes } = props;
  return (
    <Fragment>
      <Paper className={classes.rootContact}>
        <AppBar position="static" color="default">
          <Hidden smDown>
            <Tabs
              value={value}
              onChange={handleChange}
              indicatorColor="primary"
              textColor="primary"
              variant="fullWidth"
            >
              <Tab label="Terkirim ke Admin"/>
              <Tab
                label={(
                  <Badge className={classes.tabNotif} color="secondary">
                    Ditugaskan ke PIC
                  </Badge>
                )}
              />
              <Tab
                label={(
                  <Badge className={classes.tabNotif} color="secondary">
                    Diterima PIC
                  </Badge>
                )}
              />
              <Tab
                label={(
                  <Badge className={classes.tabNotif} color="secondary">
                    Diselesaikan PIC
                  </Badge>
                )}
              />
              <Tab
                label={(
                  <Badge className={classes.tabNotif} color="secondary">
                    Selesai dan Dikonfirmasi Admin
                  </Badge>
                )}
              />
              <Tab
                label={(
                  <Badge className={classes.tabNotif} color="secondary">
                    Rejecetd
                  </Badge>
                )}
              />
            </Tabs>
          </Hidden>
        </AppBar>
        {value === 0 && <TabContainer><TerkirimKeAdminStyled openMenu={handleOpen} /></TabContainer>}
        {value === 1 && <TabContainer><DitugaskanKePICStyled /></TabContainer>}
        {value === 2 && <TabContainer><DiterimaPICStyled openMenu={handleOpenAction} /></TabContainer>}
        {value === 3 && <TabContainer><DiselesaikanPICStyled openMenu={handleOpenAction} /></TabContainer>}
        {value === 4 && <TabContainer><SelesaiDanDikonfirmasiAdminStyled openMenu={handleOpenAction} /></TabContainer>}
        {value === 5 && <TabContainer><RejectedStyled openMenu={handleOpenAction} /></TabContainer>}
      </Paper>
    </Fragment>
  );
}

VerifyDailyInspection.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(VerifyDailyInspection);