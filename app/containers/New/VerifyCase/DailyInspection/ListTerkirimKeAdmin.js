import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListTerkirimKeAdmin(props) {
  const columns = [
    {
      name: 'Ticket No',
      options: {
        filter: true,
      },
    },
    {
      name: 'Project',
      options: {
        filter: true,
      },
    },
    {
      name: 'Category',
      options: {
        filter: true,
      },
    },
    {
      name: 'Submitted Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Inspection Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Target Close',
      options: {
        filter: true,
      },
    },
    {
      name: 'Reported By',
      options: {
        filter: true,
      },
    },
    {
      name: 'Action',
      options: {
        filter: true,
      },
    },
  ];

  const data = [
    ['C0JRT39LSKTY8','JALAN TOL JAKARTA-CIKAMPEK II ELEVATED', 'Bahaya Orang Terjatuh', '17 Okt 2018 15:24:17', '17 Okt 2018 15:24:00', '18 Okt 2018 15:22:52', 'Danang'],
    ['C0JRT39R6EX23','JALAN TOL JAKARTA-CIKAMPEK II ELEVATED', 'Terpapar/Bekerja dengan Bahaya Listrik', '17 Okt 2018 15:26:11', '17 Okt 2018 15:26:11', '18 Okt 2018 15:25:08', 'Danang'],
    ['C0JRT3A02HNJN','JALAN TOL JAKARTA-CIKAMPEK II ELEVATED', 'Bahaya Kebakaran/Ledakan', '17 Okt 2018 15:29:31', '17 Okt 2018 15:28:50', '17 Okt 2018 15:27:47', 'Danang'],
    ['C0JRV29FTDTRB','HEAD OFFICE', 'Penggalian', '19 Okt 2018 10:55:46', '19 Okt 2018 10:55:49',  '19 Okt 2018 10:54:56', 'Alan Sanusi'],
    ['C0JU5311Y4C6W','JALAN TOL JAKARTA-CIKAMPEK II ELEVATED', 'Bahaya Kebakaran/Ledakan', '01 Nov 2018 14:13:34', '01 Nov 2018 14:12:16', '01 Nov 2018 14:11:07', 'Samsul Rizal'],
    ['C0JU53148JAV8','JALAN TOL JAKARTA-CIKAMPEK II ELEVATED', 'Bahaya Tertimpa Barang Jatuh', '01 Nov 2018 14:14:17', '01 Nov 2018 14:14:15', '01 Nov 2018 14:13:27', 'Samsul Rizal'],
    ['C0JUR21BGHTEC','WEST VISTA RECIDENCE', 'Housekeeping', '23 Nov 2018 09:50:21', '23 Nov 2018 09:50:22', '23 Nov 2018 11:00:00', 'Vicky Andrianto'],
    ['C0JUX2EIK88I3','WEST VISTA RECIDENCE', 'Housekeeping', '29 Nov 2018 11:21:24', '29 Nov 2018 11:21:22', '29 Nov 2018 11:19:05', 'Vicky Andrianto'],
    ['C0JUX30J3JY1X','WEST VISTA RECIDENCE', 'Housekeeping', '29 Nov 2018 14:06:56', '29 Nov 2018 14:06:53', '29 Nov 2018 15:00:00', 'Vicky Andrianto'],
    ['C0JUX30OA6G23','WEST VISTA RECIDENCE', 'Housekeeping', '29 Nov 2018 14:08:42', '29 Nov 2018 14:08:42', '29 Nov 2018 14:07:02', 'Vicky Andrianto'],
    ['C0JX626A4QYG','WEST VISTA RECIDENCE', 'Housekeeping', '10 Des 2018 10:14:52', '10 Des 2018 10:14:58', '10 Des 2018 10:11:24', 'Vicky Andrianto'],
    ['C0JX627NO7Z3F','WEST VISTA RECIDENCE', 'Housekeeping', '10 Des 2018 10:32:36', '10 Des 2018 10:32:43', '10 Des 2018 01:30:00', 'Vicky Andrianto'],
    ['C0JX927KGCM9B','WEST VISTA RECIDENCE', 'Housekeeping', '13 Des 2018 10:31:21', '13 Des 2018 10:31:18', '13 Des 2018 01:00:00', 'Vicky Andrianto'],
    ['C0JX927QU9SU4','WEST VISTA RECIDENCE', 'Housekeeping', '13 Des 2018 10:33:50', '13 Des 2018 10:33:47', '13 Des 2018 00:30:00', 'Vicky Andrianto'],
    ['C0JX927VRBSZF','WEST VISTA RECIDENCE', 'Bahaya Orang Terjatuh', '13 Des 2018 10:35:28', '13 Des 2018 10:35:27', '13 Des 2018 10:33:55', 'Vicky Andrianto'],
    ['C0QS91RALC2XV','INDONESIA 1', 'Bahaya Orang Terjatuh', '05 Jan 2019 08:20:30', '04 Jan 2019 14:10:07', '04 Jan 2019 14:04:04', 'Iswanto'],
    ['C0QS91RWUCB2Y','INDONESIA 1', 'Housekeeping', '05 Jan 2019 08:28:31', '05 Jan 2019 08:24:50', '05 Jan 2019 08:21:09', 'Iswanto'],
    ['C0QSI21BFGGPI','INDONESIA 1', 'Housekeeping', '14 Jan 2019 09:50:20', '14 Jan 2019 09:50:27', '14 Jan 2019 09:47:19', 'Iswanto'],
    ['C0QSI26HV5QOZ','INDONESIA 1', 'Terpapar/Bekerja dengan Bahaya Listrik', '14 Jan 2019 10:17:31', '14 Jan 2019 10:17:39', '14 Jan 2019 10:13:51', 'Iswanto'],
    ['C0QSP1TUHEXNO','WORKSHOP CILEUNGSI', 'Housekeeping', '21 Jan 2019 08:53:38', '21 Jan 2019 08:53:44', '21 Jan 2019 08:52:17', 'Kokoh Dwiyan'],
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Terkirim ke Admin" data={data} columns={columns} options={options} />
    </div>
  );
}

ListTerkirimKeAdmin.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListTerkirimKeAdmin);
