import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListDiselesaikanPIC(props) {
  const columns = [
    {
      name: 'Ticket No',
      options: {
        filter: true,
      },
    },
    {
      name: 'Project',
      options: {
        filter: true,
      },
    },
    {
      name: 'Category',
      options: {
        filter: true,
      },
    },
    {
      name: 'Submitted Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Inspection Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Target Close',
      options: {
        filter: true,
      },
    },
    {
      name: 'Reported By',
      options: {
        filter: true,
      },
    },
    {
      name: 'PIC',
      options: {
        filter: true,
      },
    },
    {
      name: 'Action',
      options: {
        filter: true,
      },
    },
  ];

  const data = [
    ['C0JX22VCFDN8U','GRAND MANSION APARTMENT', 'Housekeeping', '06 Des 2018 13:39:36', '06 Des 2018 13:39:28', '06 Des 2018 13:37:36', 'Iqbal Cahyadi', 'Imam Pujiono'],
    ['C0JX325TAK8GN','GRAND MANSION APARTMENT', 'Housekeeping', '07 Des 2018 10:08:47', '07 Des 2018 10:08:45', '07 Des 2018 10:06:39', 'Iqbal Cahyadi', 'Imam Pujiono'],
    ['C0JX727C09UL4','GRAND MANSION APARTMENT', 'Housekeeping', '11 Des 2018 10:28:16', '11 Des 2018 10:28:07', '11 Des 2018 10:22:28', 'Iqbal Cahyadi', 'Imam Pujiono'],
    ['C0JX727JW3BBM','GRAND MANSION APARTMENT', 'Bahaya Terinjak, tersandung, terbentur oleh Benda', '11 Des 2018 10:31:00', '11 Des 2018 10:30:50',  '11 Des 2018 10:29:13', 'Iqbal Cahyadi', 'Imam Pujiono'],
    ['C0JX727WM6HTB','GRAND MANSION APARTMENT', 'Housekeeping', '09 Okt 2021 09:11:45', '11 Des 2018 10:35:58', '11 Des 2018 10:33:46', 'Iqbal Cahyadi', 'Imam Pujiono'],
    ['C0JXG33ZVIW6U','GRAND MANSION APARTMENT', 'APD, Alat Pelindung Diri', '20 Des 2018 14:51:48', '20 Des 2018 14:51:28', '20 Des 2018 14:46:28', 'Eddy Budhiyanto', 'Imam Pujiono'],
    ['C0QSC2CYCHQ1N','ARUMAYA RESIDENCE', 'Bahaya Alat dan Perkakas', '08 Jan 2019 11:01:01', '08 Jan 2019 10:59:05', '09 Jan 2019 10:53:33', 'Widi Yuni H', 'Dedi Suryadi'],
    ['C0QSM2132IXKZ','GRAND MANSION APARTMENT', 'APD, Alat Pelindung Diri', '18 Jan 2019 09:47:19', '18 Jan 2019 09:47:14', '18 Jan 2019 09:45:36', 'Iqbal Cahyadi', 'Imam Pujiono'],
    ['C0QSP25XRHHCK','GRAND MANSION APARTMENT', 'Scaffolding/Pekerjaan Struktur Sementara', '21 Jan 2019 10:10:08', '21 Jan 2019 10:01:55', '21 Jan 2019 09:59:50', 'Eddy Budhiyanto', 'Imam Pujiono'],
    ['C0QSP25Y8L68X','GRAND MANSION APARTMENT', 'Bahaya Kebakaran/Ledakan', '21 Jan 2019 10:10:25', '21 Jan 2019 10:07:30', '21 Jan 2019 10:05:11', 'Eddy Budhiyanto', 'Imam Pujiono'],  
    ['C0R932V9BB7DO','PLTU JEPARA', 'Bahaya Terinjak, tersandung, terbentur oleh Benda', '11 Jul 2019 13:38:24', '11 Jul 2019 13:40:28', '11 Jul 2019 15:00:00', 'Hashif Al Hakim', 'Riyanto'],
    ['C0RBP2692LBWV','AHEMCE Employee Center', 'Housekeeping', '05 Agt 2019 10:14:15', '05 Agt 2019 10:16:49', '05 Agt 2019 10:15:12', 'Rafhan Azmi', 'Resimas Friskyansyah'],
    ['DLI3598','INNOTECH - Arumaya', 'Bahaya Tertimpa Barang Jatuh', '24 Des 2020 16:30:48', '24 Des 2020 16:30:16', '24 Des 2020 16:30:16', 'Kiki Supriadi', 'Indra Irawan'],
    ['DLI3619','INNOTECH - Arumaya', 'Bahaya Terinjak, tersandung, terbentur oleh Benda', '26 Des 2020 15:42:21', '26 Des 2020 15:42:15', '26 Des 2020 15:42:15', 'Kiki Supriadi', 'Indra Irawan'],
    ['DLI3628','INNOTECH - Arumaya', 'Terpapar/Bekerja dengan Bahaya Listrik', '27 Des 2020 11:24:12', '27 Des 2020 11:23:11', '27 Des 2020 13:23:13', 'Kiki Supriadi', 'Indra Irawan'],
    ['DLI3646','INNOTECH - Arumaya', 'Bahaya Tertimpa Barang Jatuh', '28 Des 2020 11:38:43', '28 Des 2020 11:36:11', '28 Des 2020 13:00:13', 'Kiki Supriadi', 'Didin Alipudin'],
    ['DLI3663','INNOTECH - Arumaya', 'Bahaya Orang Terjatuh', '29 Des 2020 16:23:17', '29 Des 2020 13:21:13', '29 Des 2020 16:23:16', 'Kiki Supriadi', 'Didin Alipudin'],
    ['DLI3677','INNOTECH - Arumaya', 'Bahaya Orang Terjatuh', '30 Des 2020 15:36:49', '30 Des 2020 11:36:11', '30 Des 2020 15:36:15', 'Kiki Supriadi', 'Didin Alipudin'],
    ['DLI3697','INNOTECH - Arumaya', 'Bahaya Terinjak, tersandung, terbentur oleh Benda', '02 Jan 2021 10:32:49', '02 Jan 2021 10:32:10', '02 Jan 2021 12:00:12', 'Kiki Supriadi', 'Didin Alipudin'],
    ['DLI3705','INNOTECH - Arumaya', 'Housekeeping', '03 Jan 2021 09:07:14', '03 Jan 2021 09:06:09', '03 Jan 2021 13:00:13', 'Kiki Supriadi', 'Didin Alipudin'],
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Diselesaikan PIC" data={data} columns={columns} options={options} />
    </div>
  );
}

ListDiselesaikanPIC.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListDiselesaikanPIC);