import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import LinearProgress from '@material-ui/core/LinearProgress';
import Chip from '@material-ui/core/Chip';
import MUIDataTable from 'mui-datatables';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListDiterimaPIC(props) {
  const columns = [
    {
      name: 'Ticket No',
      options: {
        filter: true,
      },
    },
    {
      name: 'Project',
      options: {
        filter: true,
      },
    },
    {
      name: 'Hazard Type',
      options: {
        filter: true,
      },
    },
    {
      name: 'Submitted Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Identified Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Reported By',
      options: {
        filter: true,
      },
    },
    {
      name: 'PIC',
      options: {
        filter: true,
      },
    },
    {
      name: 'Action',
      options: {
        filter: true,
      },
    },
  ];

  const data = [
    //DATA DITERIMA PIC BELUM ADA 
    ['GRC9897','ARUMAYA RESIDENCE', 'Scaffolding/formwork', '12 Okt 2021 11:23:41', '12 Okt 2021 11:22:11', 'RAFHAN AZMI', 'Erlangga Herditrianto'],
    ['GRC9885','WORKSHOP JONGGOL', 'Housekeeping/Tata Griya', '12 Okt 2021 09:13:18', '12 Okt 2021 09:10:09', 'Handoko', 'Namuy Kurniawan'],
    ['GRC9768','ARUMAYA RESIDENCE', 'Lainnya: ……', '04 Okt 2021 11:49:55', '04 Okt 2021 11:48:11', 'Iswanto', 'Erlangga Herditrianto'],
    ['GRC9727','KERTAJATI', 'Melanggar Izin Kerja', '30 Sep 2021 18:30:54', '30 Sep 2021 18:28:18',  'MUHAMAD RAKHMARDHIAN', 'Suyanto'],
    ['GRC9726','KERTAJATI', 'Housekeeping', '30 Sep 2021 18:27:57', '30 Sep 2021 18:26:18', 'MUHAMAD RAKHMARDHIAN', 'Suyanto'],
    ['GRC9725','KERTAJATI', 'Bahaya Listrik', '30 Sep 2021 18:25:08', '30 Sep 2021 18:21:18', 'MUHAMAD RAKHMARDHIAN', 'Suyanto'],
    ['GRC9724','KERTAJATI', 'Housekeeping', '30 Sep 2021 18:20:20', '30 Sep 2021 18:18:18', 'MUHAMAD RAKHMARDHIAN', 'Suyanto'],
    ['GRC9550','ARUMAYA RESIDENCE', 'Tidak Menggunakan APD', '22 Sep 2021 20:54:49', '22 Sep 2021 20:52:20', 'ARIF TRI HARMOKO', 'Erlangga Herditrianto'],
    ['GRC954','CFPP Cirebon 2', 'Bahaya Benda Jatuh', '20 Jul 2020 15:21:32', '20 Jul 2020 15:19:15', 'ALFI SAHRI JAMIL MUNTHE', 'Andi Ramdani'],
    ['GRC91','ARUMAYA RESIDENCE', 'Housekeeping', '02 Des 2019 08:43:09', '02 Des 2019 08:40:08', 'Handy Handoyo', 'M. Gusnaldi'],  
    ['GRC90','ARUMAYA RESIDENCE', 'Housekeeping', '01 Des 2019 16:25:35', '01 Des 2019 16:22:16', 'Edy Sudarmanto', 'M. Gusnaldi'],
    ['GRC898','THAMRIN NINE', 'Bahaya Listrik', '18 Jul 2020 09:36:21', '18 Jul 2020 09:34:09', 'Opik Taufik', 'Dedi Suryadi'],
    ['GRC89','ARUMAYA RESIDENCE', 'Housekeeping', '30 Nov 2019 17:23:00', '30 Nov 2019 17:19:17', 'Edy Sudarmanto', 'M. Gusnaldi'],
    ['GRC87','ARUMAYA RESIDENCE', 'Housekeeping', '29 Nov 2019 09:19:46', '29 Nov 2019 09:16:09', 'Edy Sudarmanto', 'M. Gusnaldi'],
    ['GRC8637','KERTAJATI', 'Housekeeping', '21 Agt 2021 11:35:51', '21 Agt 2021 11:32:11', 'MUHAMAD RAKHMARDHIAN', 'REZKI PRATAMA'],
    ['GRC86','ARUMAYA RESIDENCE', 'Bahaya Longsor', '28 Nov 2019 11:09:40', '28 Nov 2019 11:06:11', 'Gunawan M R', 'M. Gusnaldi'],
    ['GRC8591','ARUMAYA RESIDENCE', 'Bahaya Jatuh Dari Ketinggian', '18 Agt 2021 11:34:01', '18 Agt 2021 11:32:11', 'Erlangga Herditrianto', 'RAFHAN AZMI'],
    ['GRC858','CFPP Cirebon 2', 'Bahaya Lainnya', '17 Jul 2020 10:15:16', '17 Jul 2020 10:11:10', 'ALFI SAHRI JAMIL MUNTHE', 'Andi Ramdani'],
    ['GRC8562','KERTAJATI', 'Bahaya Lainnya', '14 Agt 2021 10:19:41', '14 Agt 2021 10:16:10', 'MUHAMAD RAKHMARDHIAN', 'ADE HAMDAN'],
    ['GRC8560','KERTAJATI', 'Bahaya Tersandung/ Terpeleset', '14 Agt 2021 10:13:30', '14 Agt 2021 10:04:10', 'MUHAMAD RAKHMARDHIAN', 'ADE HAMDAN'],
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Diterima PIC" data={data} columns={columns} options={options} />
    </div>
  );
}

ListDiterimaPIC.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListDiterimaPIC);