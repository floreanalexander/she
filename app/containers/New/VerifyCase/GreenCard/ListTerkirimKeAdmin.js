import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import LinearProgress from '@material-ui/core/LinearProgress';
import Chip from '@material-ui/core/Chip';
import MUIDataTable from 'mui-datatables';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListTerkirimKeAdmin(props) {
  const columns = [
    {
      name: 'Ticket No',
      options: {
        filter: true,
      },
    },
    {
      name: 'Project',
      options: {
        filter: true,
      },
    },
    {
      name: 'Hazard Type',
      options: {
        filter: true,
      },
    },
    {
      name: 'Submitted Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Identified Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Reported By',
      options: {
        filter: true,
      },
    },
    {
      name: 'Action',
      options: {
        filter: true,
      },
    },
  ];

  const data = [
    ['GRC983','CFPP Cirebon 2', 'Bahaya Jatuh Dari Ketinggian', '21 Jul 2020 15:57:30', '21 Jul 2020 15:54:15', 'AFIF ELFIYAN'],
    ['GRC97','HEAD OFFICE', 'Bahaya Tersandung/ Terpeleset', '06 Des 2019 13:20:16', '06 Des 2019 13:12:13', 'Alicia Carine'],
    ['GRC96','HEAD OFFICE', 'Bahaya Benda Jatuh', '06 Des 2019 13:20:16', '06 Des 2019 11:29:11', 'Andika I. Syifa'],
    ['GRC95','HEAD OFFICE', 'Bahaya Ergonomi', '06 Des 2019 10:47:17', '06 Des 2019 10:44:10',  'Bambang Haryo Pratama'],
    ['GRC949','CFPP Cirebon 2', 'Bahaya Lainnya', '20 Jul 2020 11:52:27', '20 Jul 2020 11:50:11', 'Hanif Oktora'],
    ['GRC8886','THAMRIN NINE', 'Housekeeping', '30 Agt 2021 14:31:43', '09 Agt 2021 09:30:09', 'Widi Yuni H'],
    ['GRC873','CFPP Cirebon 2', 'Housekeeping', '17 Jul 2020 16:43:26', '17 Jul 2020 16:39:16', 'AFIF ELFIYAN'],
    ['GRC8676','PROYEK JALUR GANDA KA BOGOR-SUKABUMI', 'Bahaya Jatuh Dari Ketinggian', '24 Agt 2021 11:22:11', '24 Agt 2021 11:22:11', 'Chandra Rama S'],
    ['GRC802','CFPP Cirebon 2', 'Bahaya Longsor', '15 Jul 2020 11:26:25', '15 Jul 2020 11:22:11', 'ORYZA RACHMAHATI'],
    ['GRC769','CFPP Cirebon 2', 'Kesalahan Pemasangan Scaffolding', '14 Jul 2020 12:37:38', '14 Jul 2020 12:31:12', 'ALFI SAHRI JAMIL MUNTHE'],
    ['GRC724','CFPP Cirebon 2', 'Bahaya Terpapar Bahan Berbahaya', '12 Jul 2020 22:09:27', '12 Jul 2020 22:04:22', 'AFIF ELFIYAN'],
    ['GRC72','HEAD OFFICE', 'Housekeeping', '20 Nov 2019 10:26:28', '20 Nov 2019 10:23:10', 'Muhammad Rakhmardhian'],
    ['GRC694','CFPP Cirebon 2', 'Bahaya Jatuh Dari Ketinggian', '10 Jul 2020 15:37:06', '10 Jul 2020 15:32:15', 'AFIF ELFIYAN'],
    ['GRC6770','Project Testing', 'Bahaya Benda Jatuh', '10 Mei 2021 15:11:08', '10 Mei 2021 15:09:15', 'Employee Testing'],
    ['GRC6499','BALARAJA BARAT-CIKANDE', 'Bahaya Kesehatan Kerja', '20 Mar 2021 07:31:20', '20 Mar 2021 07:31:07', 'Handoko'],
    ['GRC6452','BALARAJA BARAT-CIKANDE', 'Bahaya Listrik', '16 Mar 2021 16:13:08', '16 Mar 2021 16:12:16', 'Handoko'],
    ['GRC6398','BALARAJA BARAT-CIKANDE', 'Tidak Menggunakan APD', '12 Mar 2021 07:37:02', '12 Mar 2021 07:37:07', 'Handoko'],
    ['GRC6339','BALARAJA BARAT-CIKANDE', 'Bahaya Jatuh Dari Ketinggian', '07 Mar 2021 06:21:41', '07 Mar 2021 06:21:06', 'Handoko'],
    ['GRC59','HEAD OFFICE', 'Bahaya Lainnya', '15 Nov 2019 17:38:12', '15 Nov 2019 17:31:17', 'Muhammad Rakhmardhian'],
    ['GRC583','CFPP Cirebon 2', 'Housekeeping', '07 Jul 2020 17:16:04', '07 Jul 2020 17:12:17', 'ALFI SAHRI JAMIL MUNTHE'],
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Terkirim ke Admin" data={data} columns={columns} options={options} />
    </div>
  );
}

ListTerkirimKeAdmin.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListTerkirimKeAdmin);