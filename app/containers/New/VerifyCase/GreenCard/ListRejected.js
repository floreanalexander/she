import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import LinearProgress from '@material-ui/core/LinearProgress';
import Chip from '@material-ui/core/Chip';
import MUIDataTable from 'mui-datatables';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListRejected(props) {
  const columns = [
    {
      name: 'Ticket No',
      options: {
        filter: true,
      },
    },
    {
      name: 'Project',
      options: {
        filter: true,
      },
    },
    {
      name: 'Hazard Type',
      options: {
        filter: true,
      },
    },
    {
      name: 'Submitted Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Identified Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Reported By',
      options: {
        filter: true,
      },
    },
    {
      name: 'Action',
      options: {
        filter: true,
      },
    },
  ];

  const data = [
    ['GRC98','HEAD OFFICE', 'Tidak Ada Barikade', '06 Des 2019 14:57:34', '06 Des 2019 14:55:14', 'Justhanto Mongan'],
    ['GRC975','ARUMAYA RESIDENCE', 'Bahaya Lainnya', '21 Jul 2020 10:53:20', '30 Apr 2020 10:38:10', 'Bambang Irianto'],
    ['GRC974','ARUMAYA RESIDENCE', 'Bahaya Lainnya', '21 Jul 2020 10:53:15', '30 Apr 2020 10:42:10', 'Bambang Irianto'],
    ['GRC940','WORKSHOP JONGGOL', 'Tidak Ada Barikade', '20 Jul 2020 09:37:18', '20 Jul 2020 09:35:09',  'Ayi'],
    ['GRC94','ARUMAYA RESIDENCE', 'Bahaya Tersandung/ Terpeleset', '05 Des 2019 13:27:10', '05 Des 2019 13:24:13', 'Valensio Ryandi'],
    ['GRC938','PLTGU CIBATU', 'Bahaya Lainnya', '20 Jul 2020 09:31:58', '20 Jul 2020 09:25:09', 'denriei'],
    ['GRC9','ARUMAYA RESIDENCE', 'Bahaya Kesehatan Kerja', '23 Okt 2019 11:08:40', '23 Okt 2019 11:07:11', 'Giyanto'],
    ['GRC8976','Danareksa Tower', 'Bahaya Lifting', '31 Agt 2021 10:13:41', '31 Agt 2021 10:11:10', 'Samsul Rizal'],
    ['GRC8965','Danareksa Tower', 'Bahaya Tumpahan Bahan Berbahaya', '31 Agt 2021 10:10:44', '31 Agt 2021 10:08:10', 'Samsul Rizal'],
    ['GRC8964','Danareksa Tower', 'Housekeeping', '31 Agt 2021 10:08:27', '31 Agt 2021 10:06:10', 'Samsul Rizal'],
    ['GRC881','ARUMAYA RESIDENCE', 'Bahaya Listrik', '17 Jul 2020 18:47:52', '17 Jul 2020 18:41:18', 'Arif Tri Harmoko'],
    ['GRC880','ARUMAYA RESIDENCE', 'Bahaya Tersandung/ Terpeleset', '17 Jul 2020 18:44:22', '17 Jul 2020 18:41:18', 'Anwar Hidayat'],
    ['GRC879','ARUMAYA RESIDENCE', 'Bahaya Kesehatan Kerja', '17 Jul 2020 18:43:31', '17 Jul 2020 18:40:18', 'Supriyono'],
    ['GRC877','ARUMAYA RESIDENCE', 'Bahaya Tersandung/ Terpeleset', '17 Jul 2020 18:39:05', '17 Jul 2020 18:35:18', 'Supriyono'],
    ['GRC843','Diamond Tower', 'Bahaya Lainnya', '16 Jul 2020 16:22:08', '16 Jul 2020 16:20:16', 'Rifqi Paksi Nugroho'],
    ['GRC8368','INNOTECH - WS Jonggol', 'Tidak Ada Barikade', '04 Agt 2021 08:20:54', '04 Agt 2021 08:17:08', 'Handoko'],
    ['GRC784','PLTGU CIBATU', 'Bahaya Listrik', '15 Jul 2020 08:50:38', '15 Jul 2020 08:48:08', 'Irwan'],
    ['GRC7695','ARUMAYA RESIDENCE', 'Bahaya Jatuh Dari Ketinggian', '06 Jul 2021 17:52:54', '04 Jul 2021 17:49:17', 'M GUSNALDI'],
    ['GRC7677','WORKSHOP CILEUNGSI', 'Housekeeping', '06 Jul 2021 12:59:55', '06 Jul 2021 12:58:12', 'Edwin Fitrian'],
    ['GRC7674','WORKSHOP CILEUNGSI', 'Bahaya Terpapar Bahan Berbahaya', '06 Jul 2021 11:47:32', '06 Jul 2021 11:45:11', 'Heri Kusaeri'],
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Rejected" data={data} columns={columns} options={options} />
    </div>
  );
}

ListRejected.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListRejected);