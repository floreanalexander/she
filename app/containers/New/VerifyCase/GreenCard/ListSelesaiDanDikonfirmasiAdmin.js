import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import LinearProgress from '@material-ui/core/LinearProgress';
import Chip from '@material-ui/core/Chip';
import MUIDataTable from 'mui-datatables';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListSelesaiDanDikonfirmasiAdmin(props) {
  const columns = [
    {
      name: 'Ticket No',
      options: {
        filter: true,
      },
    },
    {
      name: 'Project',
      options: {
        filter: true,
      },
    },
    {
      name: 'Hazard Type',
      options: {
        filter: true,
      },
    },
    {
      name: 'Submitted Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Identified Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Reported By',
      options: {
        filter: true,
      },
    },
    {
      name: 'PIC',
      options: {
        filter: true,
      },
    },
    {
      name: 'Action',
      options: {
        filter: true,
      },
    },
  ];

  const data = [
    ['GRC9999','WORKSHOP CILEUNGSI', 'Bahan Berbahaya Beracun (B3)/Limbah B3', '16 Okt 2021 10:38:11', '16 Okt 2021 10:36:10', 'Imam Alifmuin', 'Imam Alifmuin'],
    ['GRC9998','WORKSHOP JONGGOL', 'Pekerjaan Panas/Hot Work', '16 Okt 2021 10:11:32', '15 Okt 2021 14:04:14', 'Agung Isbagiono', 'Muhammad Filda Neterian'],
    ['GRC9997','WORKSHOP CILEUNGSI', 'APD, Alat Pelindung Diri', '16 Okt 2021 10:06:43', '16 Okt 2021 10:05:10', 'Kokoh Dwiyan', 'Nasruan Adil'],
    ['GRC9996','WORKSHOP CILEUNGSI', 'Lainnya: ……', '16 Okt 2021 08:56:27', '16 Okt 2021 08:55:08',  'Cep Jamaludin', 'Teguh'],
    ['GRC9995','WORKSHOP JONGGOL', 'APD, Alat Pelindung Diri', '16 Okt 2021 08:48:37', '16 Okt 2021 08:46:08', 'Oleh Supriadi', 'Muhammad Filda Neterian'],
    ['GRC9994','WORKSHOP JONGGOL', 'Housekeeping/Tata Griya', '16 Okt 2021 08:46:22', '16 Okt 2021 08:42:08', 'Oleh Supriadi', 'SAGI NURMANTO'],
    ['GRC9993','WORKSHOP JONGGOL', 'Fasilitas Listrik sementara', '16 Okt 2021 08:42:28', '16 Okt 2021 08:39:08', 'Oleh Supriadi', 'SAGI NURMANTO'],
    ['GRC9992','WORKSHOP CILEUNGSI', 'Lainnya: ……', '16 Okt 2021 08:34:10', '12 Okt 2021 08:32:08', 'Cep Jamaludin', 'Iswanto (Spv)'],
    ['GRC9991','WORKSHOP CILEUNGSI', 'Bahan Berbahaya Beracun (B3)/Limbah B3', '16 Okt 2021 08:31:52', '14 Okt 2021 08:30:08', 'Cep Jamaludin', 'Nasruan Adil'],
    ['GRC9990','WORKSHOP CILEUNGSI', 'Housekeeping/Tata Griya', '16 Okt 2021 08:30:16', '16 Okt 2021 08:29:08', 'Cep Jamaludin', 'Ifan Meiyaludin'],  
    ['GRC999','PLTGU CIBATU', 'Tidak Ada Barikade', '22 Jul 2020 08:12:58', '22 Jul 2020 08:11:08', 'Iqbal Cahyadi', 'Rinanto Jatmiko'],
    ['GRC9989','WORKSHOP CILEUNGSI', 'Housekeeping/Tata Griya', '16 Okt 2021 08:29:11', '04 Okt 2021 08:28:08', 'Cep Jamaludin', 'Ayi'],
    ['GRC9988','WORKSHOP CILEUNGSI', 'Lainnya: ……', '16 Okt 2021 08:27:23', '28 Sep 2021 08:24:08', 'Cep Jamaludin', 'Cep Jamaludin'],
    ['GRC9987','WORKSHOP CILEUNGSI', 'Proteksi lubang & railing', '16 Okt 2021 08:20:24', '30 Sep 2021 08:18:08', 'Wahyu Sudrajad', 'Teguh'],
    ['GRC9986','WORKSHOP CILEUNGSI', 'Peralatan/Perkakas', '16 Okt 2021 08:18:44', '16 Okt 2021 08:17:08', 'Wahyu Sudrajad', 'Ifan Meiyaludin'],
    ['GRC9985','WORKSHOP CILEUNGSI', 'APD, Alat Pelindung Diri', '16 Okt 2021 08:11:42', '16 Okt 2021 08:10:08', 'Wahyu Sudrajad', 'Kokoh Dwiyan'],
    ['GRC9984','WORKSHOP CILEUNGSI', 'Housekeeping/Tata Griya', '15 Okt 2021 17:10:02', '15 Okt 2021 17:08:17', 'Wahyu Sudrajad', 'Imam Alifmuin'],
    ['GRC9983','THAMRIN NINE', 'Housekeeping/Tata Griya', '15 Okt 2021 16:21:27', '15 Okt 2021 16:20:16', 'Opik Taufik', 'KITARAS MEGIKEN GURUSINGA'],
    ['GRC9982','THAMRIN NINE', 'Proteksi lubang & railing', '15 Okt 2021 16:20:27', '15 Okt 2021 16:19:16', 'Opik Taufik', 'Habibi'],
    ['GRC9981','WORKSHOP JONGGOL', 'APD, Alat Pelindung Diri', '15 Okt 2021 15:05:04', '15 Okt 2021 15:02:15', 'Muhammad Filda Neterian', 'Muhammad Filda Neterian'],
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Selesai dan Dikonfirmasi Admin" data={data} columns={columns} options={options} />
    </div>
  );
}

ListSelesaiDanDikonfirmasiAdmin.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListSelesaiDanDikonfirmasiAdmin);