import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import LinearProgress from '@material-ui/core/LinearProgress';
import Chip from '@material-ui/core/Chip';
import MUIDataTable from 'mui-datatables';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListDitugaskanKePIC(props) {
  const columns = [
    {
      name: 'Ticket No',
      options: {
        filter: true,
      },
    },
    {
      name: 'Project',
      options: {
        filter: true,
      },
    },
    {
      name: 'Hazard Type',
      options: {
        filter: true,
      },
    },
    {
      name: 'Submitted Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Identified Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Reported By',
      options: {
        filter: true,
      },
    },
    {
      name: 'PIC',
      options: {
        filter: true,
      },
    },
    {
      name: 'Action',
      options: {
        filter: true,
      },
    },
  ];

  const data = [
    ['GRC997','ARUMAYA RESIDENCE', 'Housekeeping', '22 Jul 2020 07:11:09', '22 Jul 2020 07:08:07', 'Ilham Suhadi', 'Rahardjo'],
    ['GRC996','ARUMAYA RESIDENCE', 'Bahaya Jatuh Dari Ketinggian', '22 Jul 2020 07:07:36', '22 Jul 2020 07:06:07', 'Bambang Irianto', 'Rahardjo'],
    ['GRC995','ARUMAYA RESIDENCE', 'Housekeeping', '22 Jul 2020 07:06:26', '22 Jul 2020 07:05:07', 'Bambang Irianto', 'Rahardjo'],
    ['GRC994','ARUMAYA RESIDENCE', 'Housekeeping', '22 Jul 2020 07:05:09', '22 Jul 2020 07:03:07',  'Bambang Irianto', 'Rahardjo'],
    ['GRC993','ARUMAYA RESIDENCE', 'Bahaya Benda Jatuh', '22 Jul 2020 07:03:59', '22 Jul 2020 07:02:07', 'Bambang Irianto', 'Rahardjo'],
    ['GRC992','ARUMAYA RESIDENCE', 'Bahaya Benda Jatuh', '22 Jul 2020 07:02:46', '22 Jul 2020 07:01:07', 'Bambang Irianto', 'Rahardjo'],
    ['GRC991','ARUMAYA RESIDENCE', 'Bahaya Benda Jatuh', '22 Jul 2020 07:01:25', '22 Jul 2020 06:59:06', 'Bambang Irianto', 'Rahardjo'],
    ['GRC9900','ARUMAYA RESIDENCE', 'Fasilitas Listrik sementara', '13 Okt 2021 08:54:22', '13 Okt 2021 08:50:08', 'Erlangga Herditrianto', 'Iswanto'],
    ['GRC990','ARUMAYA RESIDENCE', 'Bahaya Benda Jatuh', '22 Jul 2020 06:44:03', '22 Jul 2020 06:42:06', 'EDI SUDARMANTO', 'Rahardjo'],
    ['GRC989','ARUMAYA RESIDENCE', 'Bahaya Jatuh Dari Ketinggian', '22 Jul 2020 06:43:41', '22 Jul 2020 05:40:06', 'Rahardjo', 'Rahardjo'],
    ['GRC988','ARUMAYA RESIDENCE', 'Tidak Ada Barikade', '22 Jul 2020 06:42:31', '22 Jul 2020 06:40:06', 'EDI SUDARMANTO', 'Rahardjo'],
    ['GRC9874','ARUMAYA RESIDENCE', 'Peralatan/Perkakas', '11 Okt 2021 11:31:21', '11 Okt 2021 11:30:11', 'Erlangga Herditrianto', 'Iswanto'],
    ['GRC987','ARUMAYA RESIDENCE', 'Bahaya Tersandung/ Terpeleset', '22 Jul 2020 06:40:14', '22 Jul 2020 06:38:06', 'EDI SUDARMANTO', 'Rahardjo'],
    ['GRC9860','ARUMAYA RESIDENCE', 'Fasilitas Listrik sementara', '09 Okt 2021 11:14:01', '09 Okt 2021 11:13:11', 'Erlangga Herditrianto', 'RAFHAN AZMI'],
    ['GRC986','ARUMAYA RESIDENCE', 'Housekeeping', '22 Jul 2020 06:33:05', '22 Jul 2020 06:31:06', 'EDI SUDARMANTO', 'Rahardjo'],
    ['GRC985','ARUMAYA RESIDENCE', 'Housekeeping', '22 Jul 2020 06:30:10', '22 Jul 2020 06:27:06', 'EDI SUDARMANTO', 'Rahardjo'],
    ['GRC9849','ARUMAYA RESIDENCE', 'Proteksi lubang & railing', '08 Okt 2021 15:30:33', '08 Okt 2021 15:29:15', 'Erlangga Herditrianto', 'RAFHAN AZMI'],
    ['GRC9844','THAMRIN NINE', 'Housekeeping/Tata Griya', '08 Okt 2021 10:20:27', '04 Okt 2021 09:45:09', 'Syifaurrofial InA', 'Syifaurrofial InA'],
    ['GRC9840','THAMRIN NINE', 'Proteksi lubang & railing', '08 Okt 2021 09:59:51', '07 Okt 2021 15:26:15', 'Ferdian Abror K', 'Ferdian Abror K'],
    ['GRC9839','THAMRIN NINE', 'Housekeeping/Tata Griya', '08 Okt 2021 09:57:37', '06 Okt 2021 14:26:14', 'Ferdian Abror K', 'Ferdian Abror K'],
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Ditugaskan ke PIC" data={data} columns={columns} options={options} />
    </div>
  );
}

ListDitugaskanKePIC.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListDitugaskanKePIC);