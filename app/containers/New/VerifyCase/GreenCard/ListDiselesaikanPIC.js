import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import LinearProgress from '@material-ui/core/LinearProgress';
import Chip from '@material-ui/core/Chip';
import MUIDataTable from 'mui-datatables';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListDiselesaikanPIC(props) {
  const columns = [
    {
      name: 'Ticket No',
      options: {
        filter: true,
      },
    },
    {
      name: 'Project',
      options: {
        filter: true,
      },
    },
    {
      name: 'Hazard Type',
      options: {
        filter: true,
      },
    },
    {
      name: 'Submitted Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Identified Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Reported By',
      options: {
        filter: true,
      },
    },
    {
      name: 'PIC',
      options: {
        filter: true,
      },
    },
    {
      name: 'Action',
      options: {
        filter: true,
      },
    },
  ];

  const data = [
    ['GRC9935','INNOTECH - WS Jonggol', 'Housekeeping/Tata Griya', '14 Okt 2021 09:31:53', '14 Okt 2021 09:32:09', 'Jumari', 'Ikhlas Nur'],
    ['GRC9898','ARUMAYA RESIDENCE', 'Fasilitas Listrik sementara', '12 Okt 2021 11:59:14', '12 Okt 2021 11:57:11', 'Iswanto', 'Erlangga Herditrianto'],
    ['GRC9859','ARUMAYA RESIDENCE', 'Fasilitas Listrik sementara', '09 Okt 2021 09:53:19', '09 Okt 2021 09:51:09', 'Iswanto', 'Erlangga Herditrianto'],
    ['GRC9857','ARUMAYA RESIDENCE', 'APD, Alat Pelindung Diri', '09 Okt 2021 09:16:43', '09 Okt 2021 09:15:09',  'RAFHAN AZMI', 'Erlangga Herditrianto'],
    ['GRC9856','ARUMAYA RESIDENCE', 'Proteksi lubang & railing', '09 Okt 2021 09:11:45', '09 Okt 2021 09:09:09', 'RAFHAN AZMI', 'Erlangga Herditrianto'],
    ['GRC9855','ARUMAYA RESIDENCE', 'Lainnya: ……', '09 Okt 2021 09:05:20', '09 Okt 2021 09:02:09', 'RAFHAN AZMI', 'Erlangga Herditrianto'],
    ['GRC9782','ARUMAYA RESIDENCE', 'Scaffolding/formwork', '05 Okt 2021 10:42:37', '05 Okt 2021 10:38:10', 'Iswanto', 'Erlangga Herditrianto'],
    ['GRC9775','INNOTECH - WS Jonggol', 'Housekeeping/Tata Griya', '05 Okt 2021 07:16:52', '05 Okt 2021 07:15:07', 'Edy Mustopa', 'Edy Mustopa'],
    ['GRC9767','ARUMAYA RESIDENCE', 'Fasilitas Listrik sementara', '04 Okt 2021 11:14:00', '04 Okt 2021 11:13:11', 'RAFHAN AZMI', 'Erlangga Herditrianto'],
    ['GRC9765','ARUMAYA RESIDENCE', 'APD, Alat Pelindung Diri', '04 Okt 2021 10:27:04', '04 Okt 2021 10:25:10', 'Iswanto', 'Erlangga Herditrianto'],  
    ['GRC9710','ARUMAYA RESIDENCE', 'Bahaya Listrik', '29 Sep 2021 15:11:22', '29 Sep 2021 15:09:15', 'Iswanto', 'Erlangga Herditrianto'],
    ['GRC9640','ARUMAYA RESIDENCE', 'Bahaya Tersandung/ Terpeleset', '27 Sep 2021 10:17:53', '27 Sep 2021 10:15:10', 'RAFHAN AZMI', 'Erlangga Herditrianto'],
    ['GRC9616','ARUMAYA RESIDENCE', 'Bahaya Tersandung/ Terpeleset', '24 Sep 2021 17:45:43', '24 Sep 2021 17:44:17', 'KRISNA WARDANA', 'Erlangga Herditrianto'],
    ['GRC9586','ARUMAYA RESIDENCE', 'Bahaya Jatuh Dari Ketinggian', '24 Sep 2021 07:32:57', '24 Sep 2021 07:31:07', 'RAFHAN AZMI', 'Erlangga Herditrianto'],
    ['GRC9553','ARUMAYA RESIDENCE', 'Bahaya Tersandung/ Terpeleset', '22 Sep 2021 21:33:36', '22 Sep 2021 21:00:21', 'ILHAM SUHADI', 'Erlangga Herditrianto'],
    ['GRC9552','ARUMAYA RESIDENCE', 'Tidak Ada Barikade', '22 Sep 2021 21:00:45', '22 Sep 2021 20:58:20', 'ILHAM SUHADI', 'Erlangga Herditrianto'],
    ['GRC9548','ARUMAYA RESIDENCE', 'Tidak Menggunakan APD', '22 Sep 2021 20:51:25', '22 Sep 2021 20:50:20', 'ARIF TRI HARMOKO', 'Erlangga Herditrianto'],
    ['GRC9544','ARUMAYA RESIDENCE', 'Bahaya Jatuh Dari Ketinggian', '22 Sep 2021 20:43:11', '22 Sep 2021 20:40:20', 'M. DEDI SETIAWAN', 'Erlangga Herditrianto'],
    ['GRC9542','ARUMAYA RESIDENCE', 'Tidak Ada Barikade', '22 Sep 2021 20:26:13', '22 Sep 2021 20:24:20', 'BELLA MAGHFIRAH OKTAVIANI', 'Erlangga Herditrianto'],
    ['GRC9531','ARUMAYA RESIDENCE', 'Housekeeping', '22 Sep 2021 19:56:12', '22 Sep 2021 19:55:19', 'M GUSNALDI', 'Erlangga Herditrianto'],
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Diselesaikan PIC" data={data} columns={columns} options={options} />
    </div>
  );
}

ListDiselesaikanPIC.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListDiselesaikanPIC);