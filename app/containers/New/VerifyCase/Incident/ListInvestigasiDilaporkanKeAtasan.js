import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListInvestigasiDilaporkanKeAtasan(props) {
  const columns = [
    {
      name: 'Ticket No',
      options: {
        filter: true,
      },
    },
    {
      name: 'Site Name',
      options: {
        filter: true,
      },
    },
    {
      name: 'Incident Name',
      options: {
        filter: true,
      },
    },
    {
      name: 'Category',
      options: {
        filter: true,
      },
    },
    {
      name: 'Incident Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Submitted Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Reported By',
      options: {
        filter: true,
      },
    },
    {
      name: 'Tim Investigasi',
      options: {
        filter: true,
      },
    },
    {
      name: 'Action',
      options: {
        filter: true,
      },
    },
  ];

  const data = [
    ['ICN64','WORKSHOP JONGGOL', 'Kepala Terbentur sachis Truk', 'Minor Injury', '14 Jan 2021 11:25:11', '15 Jan 2021 16:24:10', 'Agung Isbagiono', 'Agung Isbagiono'],
    ['ICN63','AVANIA RESIDENCE', 'Lifting crash', 'Nearmiss', '05 Jan 2021 14:30:14', '05 Jan 2021 23:41:15', 'RIO ADISTA WIDODO PUTRA', 'Rifqi Paksi Nugroho'],
    ['ICN59','CFPP Cirebon 2', 'Tertabrak Truck Mixer', 'Minor Injury', '10 Des 2020 03:05:03', '11 Des 2020 10:39:13', 'ALFI SAHRI JAMIL MUNTHE', 'ALFI SAHRI JAMIL MUNTHE'],
    ['ICN57','WORKSHOP JONGGOL', 'Material SSP merosot saat Pengangkatan menggunakan Base Crane menimpa bagian depan Kato', 'Property Damage', '16 Nov 2020 14:05:14',  '17 Nov 2020 14:02:20', 'Agung Isbagiono', 'Agung Isbagiono'],
    ['ICN29','Project Testing', 'Testing IR', 'Nearmiss', '17 Jun 2020 09:16:09', '17 Jun 2020 09:18:17', 'System Testing', 'Investigator Testing'],
    ['ICN41','CFPP Cirebon 2', 'Tertusuk paku', 'Minor Injury', '07 Agt 2020 17:02:17', '08 Agt 2020 10:26:23', 'ORYZA RACHMAHATI', 'ALFI SAHRI JAMIL MUNTHE'],
    ['ICN42','CFPP Cirebon 2', 'Tertusuk paku', 'Minor Injury', '08 Agt 2020 17:02:17', '10 Agt 2020 09:29:19', 'ORYZA RACHMAHATI', 'ALFI SAHRI JAMIL MUNTHE'],
    ['ICN35','CFPP Cirebon 2', 'Tertusuk paku', 'Minor Injury', '12 Jul 2020 07:49:07', '13 Jul 2020 08:00:15', 'ALFI SAHRI JAMIL MUNTHE', 'ALFI SAHRI JAMIL MUNTHE'],
    ['ICN24','CFPP Cirebon 2', 'Mobil terperosok ke parit', 'Nearmiss', '12 Mei 2020 12:00:12', '15 Mei 2020 16:10:01', 'ORYZA RACHMAHATI', 'ALFI SAHRI JAMIL MUNTHE'],
    ['ICN46','CFPP Cirebon 2', 'Tertusuk paku', 'Minor Injury', '08 Agt 2020 17:02:17', '29 Agt 2020 12:51:26', 'ALFI SAHRI JAMIL MUNTHE', 'ALFI SAHRI JAMIL MUNTHE'],  
    ['ICN57','WORKSHOP JONGGOL', 'Material SSP merosot saat Pengangkatan menggunakan Base Crane menimpa bagian depan Kato', 'Property Damage', '16 Nov 2020 14:05:14',  '17 Nov 2020 14:02:20', 'Agung Isbagiono', 'Agung Isbagiono'],
    ['ICN29','Project Testing', 'Testing IR', 'Nearmiss', '17 Jun 2020 09:16:09', '17 Jun 2020 09:18:17', 'System Testing', 'Investigator Testing'],
    ['ICN41','CFPP Cirebon 2', 'Tertusuk paku', 'Minor Injury', '07 Agt 2020 17:02:17', '08 Agt 2020 10:26:23', 'ORYZA RACHMAHATI', 'ALFI SAHRI JAMIL MUNTHE'],
    ['ICN42','CFPP Cirebon 2', 'Tertusuk paku', 'Minor Injury', '08 Agt 2020 17:02:17', '10 Agt 2020 09:29:19', 'ORYZA RACHMAHATI', 'ALFI SAHRI JAMIL MUNTHE'],
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Investigasi Dilaporkan ke Atasan" data={data} columns={columns} options={options} />
    </div>
  );
}

ListInvestigasiDilaporkanKeAtasan.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListInvestigasiDilaporkanKeAtasan);