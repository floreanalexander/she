import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListTerkirimKeAdmin(props) {
  const columns = [
    {
      name: 'Ticket No',
      options: {
        filter: true,
      },
    },
    {
      name: 'Site Name',
      options: {
        filter: true,
      },
    },
    {
      name: 'Incident Name',
      options: {
        filter: true,
      },
    },
    {
      name: 'Category',
      options: {
        filter: true,
      },
    },
    {
      name: 'Incident Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Submitted Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Reported By',
      options: {
        filter: true,
      },
    },
    {
      name: 'Action',
      options: {
        filter: true,
      },
    },
  ];

  const data = [
    ['ICN70','CLEON PARK APARTMENT', 'Nearmiss', 'Nearmiss', '01 Mar 2022 18:07:18', '01 Mar 2022 18:09:17', 'Yosep Margen S'],
    ['ICN40','THAMRIN NINE	', 'Busbar Passangger Hoist section teratas Terbakar', 'Property Damage', '08 Agt 2020 10:17:10', '08 Agt 2020 10:26:21', 'Chandra Subagiyo'],
    ['ICN39','Diamond Tower', 'Test', 'Nearmiss', '29 Jul 2020 10:03:10', '29 Jul 2020 10:04:10', 'Dian Afriyeni'],
    ['ICN38','Diamond Tower', 'Testing', 'Nearmiss', '28 Jul 2020 08:54:08',  '28 Jul 2020 08:55:33','Rifqi Paksi Nugroho'],
    ['ICN37','Diamond Tower', 'Tumpahan solar', 'Environmental Damage', '23 Jul 2020 15:42:15', '23 Jul 2020 15:44:03', 'Rifqi Paksi Nugroho'],
    ['ICN36','Diamond Tower', 'Trial', 'Minor Injury', '23 Jul 2020 15:31:15', '23 Jul 2020 15:32:39', 'Dian Afriyeni'],
    ['ICN33','THAMRIN NINE', 'Pengangkatan material besi menabrak bullnos kaca PT. Indalek', 'Property Damage', '25 Jun 2020 18:30:18', '26 Jun 2020 10:49:01', 'Chandra Subagiyo'],
    ['ICN30','Project Testing', 'Test 2', 'Nearmiss', '17 Jun 2020 10:43:10', '17 Jun 2020 10:45:54', 'System Testing']
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Terkirim ke Admin" data={data} columns={columns} options={options} />
    </div>
  );
}

ListTerkirimKeAdmin.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListTerkirimKeAdmin);
