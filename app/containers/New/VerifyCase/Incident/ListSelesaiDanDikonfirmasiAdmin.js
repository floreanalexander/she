import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListSelesaiDanDikonfirmasiAdmin(props) {
  const columns = [
    {
      name: 'Ticket No',
      options: {
        filter: true,
      },
    },
    {
      name: 'Site Name',
      options: {
        filter: true,
      },
    },
    {
      name: 'Incident Name',
      options: {
        filter: true,
      },
    },
    {
      name: 'Category',
      options: {
        filter: true,
      },
    },
    {
      name: 'Incident Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Submitted Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Reported By',
      options: {
        filter: true,
      },
    },
    {
      name: 'Tim Investigasi',
      options: {
        filter: true,
      },
    },
    {
      name: 'PIC',
      options: {
        filter: true,
      },
    },
    {
      name: 'Action',
      options: {
        filter: true,
      },
    },
  ];

  const data = [
    ['ICN66','Project Testing	', 'Test Insiden kebakaran', 'Environmental Damage', '23 Feb 2021 14:37:14', '23 Feb 2021 14:37:05', 'Employee Testing', 'Investigator Testing', 'PIC Testing'],
    ['ICN55','HEAD OFFICE', 'Testing', 'Property Damage', '10 Nov 2020 15:45:15', '10 Nov 2020 15:45:48', 'Employee Testing', 'Alan Damero Parhusip', 'Alicia Carine'],
    ['ICN28','Project Testing	', 'Test IR', 'Nearmiss', '15 Jun 2020 17:16:17', '15 Jun 2020 17:20:26', 'Investigator Testing', 'Investigator Testing', 'PIC Testing'],
    ['ICN25','PROJECT X', 'Pekerja terjatuh saat melakukan pekerjaan dan kakinya terluka', 'Minor Injury', '20 Mei 2020 10:43:10',  '20 Mei 2020 10:47:46', 'Alicia Carine', 'SHE Manager', 'Alicia Carine']
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Selesai dan Dikonfirmasi Admin" data={data} columns={columns} options={options} />
    </div>
  );
}

ListSelesaiDanDikonfirmasiAdmin.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListSelesaiDanDikonfirmasiAdmin);