import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListTerkirimKePetugasSHEHO(props) {
  const columns = [
    {
      name: 'Ticket No',
      options: {
        filter: true,
      },
    },
    {
      name: 'Site Name',
      options: {
        filter: true,
      },
    },
    {
      name: 'Incident Name',
      options: {
        filter: true,
      },
    },
    {
      name: 'Category',
      options: {
        filter: true,
      },
    },
    {
      name: 'Incident Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Submitted Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Reported By',
      options: {
        filter: true,
      },
    },
    {
      name: 'Tim Investigasi',
      options: {
        filter: true,
      },
    },
    {
      name: 'Action',
      options: {
        filter: true,
      },
    },
  ];

  const data = [
    ['ICN68','WORKSHOP CILEUNGSI', 'Jari tangan kanan memar terjepit tuas handle Hydraulic backhoe', 'Minor Injury', '06 Okt 2021 10:20:10', '08 Okt 2021 14:02:41', 'Kokoh Dwiyan', 'Kokoh Dwiyan'],
    ['ICN65','KERTAJATI', 'Assisten Survey Kejatuhan Longsoran', 'Major Injury', '06 Jan 2021 16:00:00', '10 Feb 2021 09:16:41', 'MUHAMAD RAKHMARDHIAN', 'WAHYU WIJANARKO']
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Terkirim ke Petugas SHE HO" data={data} columns={columns} options={options} />
    </div>
  );
}

ListTerkirimKePetugasSHEHO.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListTerkirimKePetugasSHEHO);