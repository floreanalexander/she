import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListRejected(props) {
  const columns = [
    {
      name: 'Ticket No',
      options: {
        filter: true,
      },
    },
    {
      name: 'Site Name',
      options: {
        filter: true,
      },
    },
    {
      name: 'Incident Name',
      options: {
        filter: true,
      },
    },
    {
      name: 'Category',
      options: {
        filter: true,
      },
    },
    
    {
      name: 'Incident Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Submitted Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Reported By',
      options: {
        filter: true,
      },
    },
    {
      name: 'Action',
      options: {
        filter: true,
      },
    },
  ];

  const data = [
    ['ICN52','Project Testing', 'Testing 26', 'Property Damage', '26 Okt 2020 11:16:11', '26 Okt 2020 11:18:07', 'Inspector Testing'],
    ['ICN18','Autograpgh ABC', 'tertimpa material', 'Minor Injury', '04 Mar 2020 10:52:10', '04 Mar 2020 11:01:14', 'Ade Ismail'],
    ['ICN19','Autograpgh ABC', 'Lengan tangan hampir terpeleset', 'Nearmiss', '04 Mar 2020 10:16:10', '04 Mar 2020 11:03:18', 'Tukijan'],
    ['ICN21','Autograpgh ABC', 'Org hampir terjatuh akibat tersandung', 'Nearmiss', '04 Mar 2020 11:02:11',  '04 Mar 2020 11:03:51', 'Alicia Carine'],
    ['ICN22','WORKSHOP JONGGOL', 'Tertabrak kambing', 'Minor Injury', '06 Mar 2020 09:41:09', '06 Mar 2020 09:44:36', 'Bambang Haryo Pratama'],
    ['ICN17','Autograpgh ABC', 'Ada penurunan settlement tanah', 'Property Damage', '04 Mar 2020 10:51:10', '04 Mar 2020 11:00:25', 'Dimas Eko Prayoso'],
    ['ICN16','ARUMAYA RESIDENCE', 'Bahaya terpeleset', 'Nearmiss', '23 Okt 2019 11:13:11', '23 Okt 2019 11:22:38', 'Katarina S'],
    ['ICN15','ARUMAYA RESIDENCE', 'Aliran listrik', 'Environmental Damage', '23 Okt 2019 11:08:11', '23 Okt 2019 11:10:49', 'Gunawan M R'],
    ['ICN14','HEAD OFFICE', 'Test', 'Minor Injury', '03 Okt 2019 09:21:09', '03 Okt 2019 09:24:29', 'Alicia Carine'],
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Rejected" data={data} columns={columns} options={options} />
    </div>
  );
}

ListRejected.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListRejected);