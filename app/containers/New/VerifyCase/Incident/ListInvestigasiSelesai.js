import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListInvestigasiSelesai(props) {
  const columns = [
    {
      name: 'Ticket No',
      options: {
        filter: true,
      },
    },
    {
      name: 'Site Name',
      options: {
        filter: true,
      },
    },
    {
      name: 'Incident Name',
      options: {
        filter: true,
      },
    },
    {
      name: 'Category',
      options: {
        filter: true,
      },
    },
    {
      name: 'Incident Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Submitted Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Reported By',
      options: {
        filter: true,
      },
    },
    {
      name: 'Tim Investigasi',
      options: {
        filter: true,
      },
    },
    {
      name: 'Action',
      options: {
        filter: true,
      },
    },
  ];

  const data = [
        ['ICN45','WORKSHOP CILEUNGSI', 'Jari tangan kiri ( Telunjuk ) Terjepit Swivel Roll hose', 'Minor Injury', '28 Agt 2020 14:30:14', '29 Agt 2020 07:11:11', 'Agung Isbagiono', 'Kokoh Dwiyan'],
    ['ICN48','WORKSHOP CILEUNGSI', 'Pelipis Mata kanan terkena serpihan palu bodem', 'Minor Injury', '12 Sep 2020 11:20:00', '12 Sep 2020 15:19:05', 'Agung Isbagiono', 'Kokoh Dwiyan'],
    ['ICN56','HEAD OFFICE', 'Testing 2', 'Nearmiss', '11 Nov 2020 11:03:11', '11 Nov 2020 11:04:47', 'Employee Testing', 'Alan Damero Parhusip'],
    ['ICN26','PROJECT X', 'Orang Terjepit besi', 'Minor Injury', '20 Mei 2020 13:46:13',  '20 Mei 2020 13:54:42', 'Alicia Carine', 'SHE Manager'],
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Investigasi Selesai dan akan Ditindak Lanjuti" data={data} columns={columns} options={options} />
    </div>
  );
}

ListInvestigasiSelesai.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListInvestigasiSelesai);