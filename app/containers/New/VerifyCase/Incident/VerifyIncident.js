import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Hidden from '@material-ui/core/Hidden';
import Badge from '@material-ui/core/Badge';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import styles from '../../../../components/Widget/widget-jss';

import { Helmet } from 'react-helmet';
import { NewPaperBlock } from 'dan-components';
import ListTerkirimKeAdmin from './ListTerkirimKeAdmin';
import ListDitugaskanKeTimInvestigasi from './ListDitugaskanKeTimInvestigasi'
import ListInvestigasiDilaporkanKeAtasan from './ListInvestigasiDilaporkanKeAtasan'
import ListTerkirimKePetugasSHEHO from './ListTerkirimKePetugasSHEHO'
import ListTerkirimKeL2 from './ListTerkirimKeL2'
import ListTerkirimKeL1 from './ListTerkirimKeL1'
import ListDitugaskanKePIC from './ListDitugaskanKePIC';
import ListDiterimaPIC from './ListDiterimaPIC';
import ListDiselesaikanPIC from './ListDiselesaikanPIC';
import ListSelesaiDanDikonfirmasiAdmin from './ListSelesaiDanDikonfirmasiAdmin';
import ListRejected from './ListRejected';


/* Tab Container */
function TabContainer(props) {
  const { children } = props;
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};
/* END Tab Container */

/* Terkirim ke Admin */
function TerkirimKeAdmin(props) {
  const title = 'Verify Incident - Terkirim ke Admin';
  const description = 'Halaman Verify Incident';
  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
    <NewPaperBlock>
      <ListTerkirimKeAdmin />
    </NewPaperBlock>
    </div>
  );
}

TerkirimKeAdmin.propTypes = {
  classes: PropTypes.object.isRequired,
  openMenu: PropTypes.func.isRequired,
};

const TerkirimKeAdminStyled = withStyles(styles)(TerkirimKeAdmin);
/* END Terkirim ke Admin */

/* DitugaskanKeTimInvestigasi */
function DitugaskanKeTimInvestigasi(props) {
  const title = 'Verify Incident - Ditugaskan ke Tim Investigasi';
  const description = 'Halaman Verify Incident';
  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
    <NewPaperBlock>
      <ListDitugaskanKeTimInvestigasi />
    </NewPaperBlock>
    </div>
  );
}

DitugaskanKeTimInvestigasi.propTypes = {
  classes: PropTypes.object.isRequired,
  openMenu: PropTypes.func.isRequired,
};

const DitugaskanKeTimInvestigasiStyled = withStyles(styles)(DitugaskanKeTimInvestigasi);
/* END DitugaskanKeTimInvestigasi */

/* InvestigasiDilaporkanKeAtasan */
function InvestigasiDilaporkanKeAtasan(props) {
  const title = 'Verify Incident - Investigasi Dilaporkan ke Atasan';
  const description = 'Halaman Verify Incident';
  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
    <NewPaperBlock>
      <ListInvestigasiDilaporkanKeAtasan />
    </NewPaperBlock>
    </div>
  );
}

InvestigasiDilaporkanKeAtasan.propTypes = {
  classes: PropTypes.object.isRequired,
  openMenu: PropTypes.func.isRequired,
};

const InvestigasiDilaporkanKeAtasanStyled = withStyles(styles)(InvestigasiDilaporkanKeAtasan);
/* END InvestigasiDilaporkanKeAtasan */

/* TerkirimKePetugasSHEHO */
function TerkirimKePetugasSHEHO(props) {
  const title = 'Verify Incident - Terkirim ke Petugas SHE HO';
  const description = 'Halaman Verify Incident';
  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
    <NewPaperBlock>
      <ListTerkirimKePetugasSHEHO />
    </NewPaperBlock>
    </div>
  );
}

TerkirimKePetugasSHEHO.propTypes = {
  classes: PropTypes.object.isRequired,
  openMenu: PropTypes.func.isRequired,
};

const TerkirimKePetugasSHEHOStyled = withStyles(styles)(TerkirimKePetugasSHEHO);
/* END TerkirimKePetugasSHEHO */

/* TerkirimKeL2 */
function TerkirimKeL2(props) {
  const title = 'Verify Incident - Terkirim Ke L2';
  const description = 'Halaman Verify Incident';
  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
    <NewPaperBlock>
      <ListTerkirimKeL2 />
    </NewPaperBlock>
    </div>
  );
}

TerkirimKeL2.propTypes = {
  classes: PropTypes.object.isRequired,
  openMenu: PropTypes.func.isRequired,
};

const TerkirimKeL2Styled = withStyles(styles)(TerkirimKeL2);
/* END TerkirimKeL2 */

/* TerkirimKeL1 */
function TerkirimKeL1(props) {
  const title = 'Verify Incident - Terkirim Ke L1';
  const description = 'Halaman Verify Incident';
  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
    <NewPaperBlock>
      <ListTerkirimKeL1 />
    </NewPaperBlock>
    </div>
  );
}

TerkirimKeL1.propTypes = {
  classes: PropTypes.object.isRequired,
  openMenu: PropTypes.func.isRequired,
};

const TerkirimKeL1Styled = withStyles(styles)(TerkirimKeL1);
/* END TerkirimKeL1 */

/* Ditugaskan ke PIC */
function DitugaskanKePIC(props) {
  const title = 'Verify Incident - Ditugaskan ke PIC';
  const description = 'Halaman Verify Incident';
  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
    <NewPaperBlock>
      <ListDitugaskanKePIC />
    </NewPaperBlock>
    </div>
  );
}

DitugaskanKePIC.propTypes = {
  classes: PropTypes.object.isRequired,
};

const DitugaskanKePICStyled = withStyles(styles)(DitugaskanKePIC);
/* END Ditugaskan ke PIC */

/* Diterima PIC */
function DiterimaPIC(props) {
  const title = 'Verify Incident - Diterima PIC';
  const description = 'Halaman Verify Incident';
  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
    <NewPaperBlock>
      <ListDiterimaPIC />
    </NewPaperBlock>
    </div>
  );
}

DiterimaPIC.propTypes = {
  classes: PropTypes.object.isRequired,
  openMenu: PropTypes.func.isRequired,
};

const DiterimaPICStyled = withStyles(styles)(DiterimaPIC);
/* Diterima PIC */

/* Diselesaikan PIC */
function DiselesaikanPIC(props) {
  const title = 'Verify Incident - Diselesaikan PIC';
  const description = 'Halaman Verify Incident';
  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
    <NewPaperBlock>
      <ListDiselesaikanPIC />
    </NewPaperBlock>
    </div>
  );
}

DiselesaikanPIC.propTypes = {
  classes: PropTypes.object.isRequired,
  openMenu: PropTypes.func.isRequired,
};

const DiselesaikanPICStyled = withStyles(styles)(DiselesaikanPIC);
/* Diselesaikan PIC */

/* Selesai dan Dikonfirmasi Admin */
function SelesaiDanDikonfirmasiAdmin(props) {
  const title = 'Verify Incident - Selesai dan Dikonfirmasi Admin';
  const description = 'Halaman Verify Incident';
  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
    <NewPaperBlock>
      <ListSelesaiDanDikonfirmasiAdmin />
    </NewPaperBlock>
    </div>
  );
}

SelesaiDanDikonfirmasiAdmin.propTypes = {
  classes: PropTypes.object.isRequired,
  openMenu: PropTypes.func.isRequired,
};

const SelesaiDanDikonfirmasiAdminStyled = withStyles(styles)(SelesaiDanDikonfirmasiAdmin);
/* Selesai dan Dikonfirmasi Admin */

/* Rejected */
function Rejected(props) {
  const title = 'Verify Incident - Rejected';
  const description = 'Halaman Verify Incident';
  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
    <NewPaperBlock>
      <ListRejected />
    </NewPaperBlock>
    </div>
  );
}

Rejected.propTypes = {
  classes: PropTypes.object.isRequired,
  openMenu: PropTypes.func.isRequired,
};

const RejectedStyled = withStyles(styles)(Rejected);
/* Rejected */

function VerifyDailyInspection(props) {
  const [value, setValue] = useState(0);
  const [anchorEl, setAnchorEl] = useState(null);
  const [anchorElAction, setAnchorElAction] = useState(null);

  const handleChange = (event, val) => {
    setValue(val);
  };

  const handleOpen = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleOpenAction = event => {
    setAnchorElAction(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
    setAnchorElAction(null);
  };

  const { classes } = props;
  return (
    <Fragment>
      <Paper className={classes.rootContact}>
        <AppBar position="static" color="default">
          <Hidden smDown>
            <Tabs
              value={value}
              onChange={handleChange}
              indicatorColor="primary"
              textColor="primary"
              variant="fullWidth"
            >
              <Tab label="Terkirim ke Admin"/>
              <Tab
                label={(
                  <Badge className={classes.tabNotif} color="secondary">
                    Ditugaskan ke Tim Investigasi
                  </Badge>
                )}
              />
              <Tab
                label={(
                  <Badge className={classes.tabNotif} color="secondary">
                    Investigasi Dilaporkan ke Atasan
                  </Badge>
                )}
              />
              <Tab
                label={(
                  <Badge className={classes.tabNotif} color="secondary">
                    Terkirim ke Petugas SHE HO
                  </Badge>
                )}
              />
              <Tab
                label={(
                  <Badge className={classes.tabNotif} color="secondary">
                    Terkirim ke L2
                  </Badge>
                )}
              />
              <Tab
                label={(
                  <Badge className={classes.tabNotif} color="secondary">
                    Terkirim ke L1
                  </Badge>
                )}
              />
              <Tab
                label={(
                  <Badge className={classes.tabNotif} color="secondary">
                    Terkirim ke FH
                  </Badge>
                )}
              />
              <Tab
                label={(
                  <Badge className={classes.tabNotif} color="secondary">
                    Investigasi Selesai dan akan Ditindak Lanjuti
                  </Badge>
                )}
              />
              <Tab
                label={(
                  <Badge className={classes.tabNotif} color="secondary">
                    Ditugaskan ke PIC
                  </Badge>
                )}
              />
              <Tab
                label={(
                  <Badge className={classes.tabNotif} color="secondary">
                    Diterima PIC
                  </Badge>
                )}
              />
              <Tab
                label={(
                  <Badge className={classes.tabNotif} color="secondary">
                    Verifikasi Petugas SHE HO
                  </Badge>
                )}
              />
              <Tab
                label={(
                  <Badge className={classes.tabNotif} color="secondary">
                    Diselesaikan PIC
                  </Badge>
                )}
              />
              <Tab
                label={(
                  <Badge className={classes.tabNotif} color="secondary">
                    Selesai dan Dikonfirmasi Admin
                  </Badge>
                )}
              />
              <Tab
                label={(
                  <Badge className={classes.tabNotif} color="secondary">
                    Rejecetd
                  </Badge>
                )}
              />
            </Tabs>
          </Hidden>
        </AppBar>
        {value === 0 && <TabContainer><TerkirimKeAdminStyled openMenu={handleOpen} /></TabContainer>}
        {value === 1 && <TabContainer><DitugaskanKeTimInvestigasiStyled openMenu={handleOpenAction} /></TabContainer>}
        {value === 2 && <TabContainer><InvestigasiDilaporkanKeAtasanStyled openMenu={handleOpenAction} /></TabContainer>}
        {value === 3 && <TabContainer><TerkirimKePetugasSHEHOStyled openMenu={handleOpenAction} /></TabContainer>}
        {value === 4 && <TabContainer><TerkirimKeL2Styled openMenu={handleOpenAction} /></TabContainer>}
        {value === 5 && <TabContainer><TerkirimKeL1Styled openMenu={handleOpenAction} /></TabContainer>}
        {value === 8 && <TabContainer><DitugaskanKePICStyled /></TabContainer>}
        {value === 9 && <TabContainer><DiterimaPICStyled openMenu={handleOpenAction} /></TabContainer>}
        {value === 11 && <TabContainer><DiselesaikanPICStyled openMenu={handleOpenAction} /></TabContainer>}
        {value === 12 && <TabContainer><SelesaiDanDikonfirmasiAdminStyled openMenu={handleOpenAction} /></TabContainer>}
        {value === 13 && <TabContainer><RejectedStyled openMenu={handleOpenAction} /></TabContainer>}
      </Paper>
    </Fragment>
  );
}

VerifyDailyInspection.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(VerifyDailyInspection);
