import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import LinearProgress from '@material-ui/core/LinearProgress';
import Chip from '@material-ui/core/Chip';
import MUIDataTable from 'mui-datatables';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListDitugaskanKeTimInvestigasi(props) {
  const columns = [
    {
      name: 'Ticket No',
      options: {
        filter: true,
      },
    },
    {
      name: 'Site Name',
      options: {
        filter: true,
      },
    },
    {
      name: 'Incident Name',
      options: {
        filter: true,
      },
    },
    {
      name: 'Category',
      options: {
        filter: true,
      },
    },
    {
      name: 'Incident Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Submitted Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Reported By',
      options: {
        filter: true,
      },
    },
    {
      name: 'Tim Investigasi',
      options: {
        filter: true,
      },
    },
    {
      name: 'Action',
      options: {
        filter: true,
      },
    },
  ];

  const data = [
    ['ICN73','WORKSHOP JONGGOL', 'FAC (Luka gores pada jari)', 'Minor Injury', '25 Mei 2022 12:05:12', '31 Mei 2022 14:15:35', 'Agung Isbagiono', 'Agung Isbagiono'],
    ['ICN72','WORKSHOP CILEUNGSI', 'Pintu kabin HDR penyok', 'Property Damage', '31 Mar 2022 11:41:11', '31 Mar 2022 11:43:58', 'Kokoh Dwiyan', 'Kokoh Dwiyan'],
    ['ICN71','WORKSHOP CILEUNGSI	', 'Near miss', 'Nearmiss', '26 Mar 2022 09:05:09', '26 Mar 2022 14:27:42', 'Kokoh Dwiyan', 'Kokoh Dwiyan'],
    ['ICN69','CLEON PARK APARTMENT', 'Terjatuh', 'Minor Injury', '31 Des 2021 09:31:09',  '31 Des 2021 09:33:00','Arief Rahman', 'Yosep Margen S'],
    ['ICN67','WORKSHOP CILEUNGSI', 'Jari kanan telunjuk terjepit gear', 'Minor Injury', '19 Mar 2021 13:20:13', '20 Mar 2021 09:23:41', 'Agung Isbagiono', 'Kokoh Dwiyan'],
    ['ICN60','THE STATURE KEBON SIRIH', 'Protruding object', 'Minor Injury', '15 Des 2020 08:44:08', '16 Des 2020 08:53:28', 'Irwan', 'Wahyu Laksono'],
    ['ICN61','AVANIA RESIDENCE', 'Jari tengah dan manis tangan kanan terjepit sling saat erection', 'Minor Injury', '17 Des 2020 10:30:10', '17 Des 2020 17:50:16', 'RIO ADISTA WIDODO PUTRA', 'Rifqi Paksi Nugroho'],
    ['ICN58','ARUMAYA RESIDENCE', 'Tersenggol chain sling', 'Minor Injury', '21 Nov 2020 14:00:14', '03 Des 2020 14:17:45', 'Erlangga Herditrianto', 'Bambang Irianto'],
    ['ICN62','Diamond Tower', 'Overheat HDR Zoomlion (HDR 250)', 'Property Damage', '17 Des 2020 23:30:23', '18 Des 2020 09:51:33', 'Dian Afriyeni', 'LINGGA AHLUL FIQRI'],
    ['ICN49','WORKSHOP CILEUNGSI', 'Jari telunjuk kiri Terjepit Swivel Roll bearing', 'Minor Injury', '28 Agt 2020 14:30:14', '29 Sep 2020 11:56:17', 'Agung Isbagiono', 'Kokoh Dwiyan'],
    ['ICN69','CLEON PARK APARTMENT', 'Terjatuh', 'Minor Injury', '31 Des 2021 09:31:09',  '31 Des 2021 09:33:00','Arief Rahman', 'Yosep Margen S'],
    ['ICN67','WORKSHOP CILEUNGSI', 'Jari kanan telunjuk terjepit gear', 'Minor Injury', '19 Mar 2021 13:20:13', '20 Mar 2021 09:23:41', 'Agung Isbagiono', 'Kokoh Dwiyan'],
    ['ICN60','THE STATURE KEBON SIRIH', 'Protruding object', 'Minor Injury', '15 Des 2020 08:44:08', '16 Des 2020 08:53:28', 'Irwan', 'Wahyu Laksono'],
    ['ICN61','AVANIA RESIDENCE', 'Jari tengah dan manis tangan kanan terjepit sling saat erection', 'Minor Injury', '17 Des 2020 10:30:10', '17 Des 2020 17:50:16', 'RIO ADISTA WIDODO PUTRA', 'Rifqi Paksi Nugroho'],
    ['ICN58','ARUMAYA RESIDENCE', 'Tersenggol chain sling', 'Minor Injury', '21 Nov 2020 14:00:14', '03 Des 2020 14:17:45', 'Erlangga Herditrianto', 'Bambang Irianto']
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Ditugaskan ke Tim Investigasi" data={data} columns={columns} options={options} />
    </div>
  );
}

ListDitugaskanKeTimInvestigasi.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListDitugaskanKeTimInvestigasi);