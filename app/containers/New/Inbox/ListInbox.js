import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import LinearProgress from '@material-ui/core/LinearProgress';
import Chip from '@material-ui/core/Chip';
import MUIDataTable from 'mui-datatables';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListInbox(props) {
  const columns = [
    {
      name: 'From',
      options: {
        filter: true,
      },
    },
    {
      name: 'Message',
      options: {
        filter: true,
        customBodyRender: value => {
          const nf = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          });

          return nf.format(value);
        },
      },
    },
    {
      name: 'Time/Date',
      options: {
        filter: true,
        customBodyRender: value => {
          if (value === 'active') {
            return <Chip label="Active" color="secondary" />;
          }
          if (value === 'non-active') {
            return <Chip label="Non Active" color="primary" />;
          }
          return <Chip label="Unknown" />;
        },
      },
    },
    {
      name: 'Action',
      options: {
        filter: true,
      },
    },
  ];

  const data = [
    ['Gabby George', 100000, 'active', 'Business Analyst'],
    ['Aiden Lloyd', 200000, 'active', 'Business Consultant'],
    ['Jaden Collins', 500000, 'non-active', 'Attorney'],
    ['Franky Rees', 50000, 'active', 'Business Analyst'],
    ['Aaren Rose', 75000, 'unknown', 'Business Consultant'],
    ['Blake Duncan', 94000, 'active', 'Business Management Analyst'],
    ['Frankie Parry', 210000, 'non-active', 'Agency Legal Counsel'],
    ['Lane Wilson', 65000, 'active', 'Commercial Specialist'],
    ['Robin Duncan', 77000, 'unknown', 'Business Analyst'],
    ['Mel Brooks', 135000, 'active', 'Business Consultant'],
    ['Harper White', 420000, 'non-active', 'Attorney'],
    ['Kris Humphrey', 150000, 'active', 'Agency Legal Counsel'],
    ['Frankie Long', 170000, 'active', 'Industrial Analyst'],
    ['Brynn Robbins', 90000, 'active', 'Business Analyst'],
    ['Justice Mann', 33000, 'non-active', 'Business Consultant'],
    ['Addison Navarro', 295000, 'non-active', 'Business Management Analyst'],
    ['Jesse Welch', 100000, 'active', 'Agency Legal Counsel'],
    ['Eli Mejia', 400000, 'active', 'Commercial Specialist'],
    ['Gene Leblanc', 110000, 'active', 'Industrial Analyst'],
    ['Danny Leon', 220000, 'non-active', 'Computer Scientist'],
    ['Lane Lee', 180000, 'unknown', 'Corporate Counselor'],
    ['Jesse Hall', 99000, 'active', 'Business Analyst'],
    ['Danni Hudson', 90000, 'active', 'Agency Legal Counsel'],
    ['Terry Macdonald', 140000, 'active', 'Commercial Specialist'],
    ['Justice Mccarthy', 330000, 'active', 'Attorney'],
    ['Silver Carey', 250000, 'active', 'Computer Scientist'],
    ['Franky Miles', 190000, 'active', 'Industrial Analyst'],
    ['Glen Nixon', 80000, 'non-active', 'Corporate Counselor'],
    ['Gabby Strickland', 45000, 'unknown', 'Business Process Consultant'],
    ['Mason Ray', 142000, 'active', 'Computer Scientist'],
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="List Inbox" data={data} columns={columns} options={options} />
    </div>
  );
}

ListInbox.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListInbox);
