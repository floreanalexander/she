import React, { useState } from 'react';
import { Helmet } from 'react-helmet';
import brand from 'dan-api/dummy/brand';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import SearchIcon from '@material-ui/icons/Search';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import { NewPaperBlock } from 'dan-components';
import ListInbox from './ListInbox';

import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  divider: {
    margin: `${theme.spacing(3)}px 0`,
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 220,
  },
  picker: {
    margin: `${theme.spacing(3)}px 5px`,
  },
  button: {
    margin: theme.spacing(1),
  },
  leftIcon: {
    marginRight: theme.spacing(1),
  },
});

function Inbox(props) {
  const { classes } = props;
  const title = 'Inbox';
  const description = 'Halaman Inbox';

  const [dataState, setDataState] = useState({
    age: '',
    name: 'hai',
  });
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [anchorEl, setAnchorEl] = useState(null);
  const [currentLocale, setCurrentLocale] = useState('id');

  const handleChange = event => {
    setDataState({
      ...dataState,
      [event.target.name]: event.target.value,
    });
  };
  const handleDateChange = date => {
    setSelectedDate(date);
  };
  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
      <NewPaperBlock style={{ paddingBottom: 0 }}>
        <div className={classes.root}>
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="age-simple">Filter</InputLabel>
            <Select
              value={dataState.age}
              onChange={handleChange}
              inputProps={{
                name: 'age',
                id: 'age-simple',
              }}
            >
              <MenuItem value="">
                <em>--Pilih Filter--</em>
              </MenuItem>
              <MenuItem value={10}>Semua Pesan</MenuItem>
              <MenuItem value={20}>Sudah Dibaca</MenuItem>
              <MenuItem value={30}>Belum DIbaca</MenuItem>
            </Select>
          </FormControl>
          {/* <div style={{ textAlign: 'right' }}>
            <Button className={classes.button} variant="contained" color="secondary">
              <SearchIcon className={classes.leftIcon} />
              CARI
            </Button>
          </div> */}
        </div>
      </NewPaperBlock>
      <ListInbox />
    </div>
  );
}

Inbox.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Inbox);
