import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import LinearProgress from '@material-ui/core/LinearProgress';
import Chip from '@material-ui/core/Chip';
import MUIDataTable from 'mui-datatables';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListIncident(props) {
  const columns = [
    {
      name: 'No',
      options: {
        filter: true,
      },
    },
    {
      name: 'Ticket No',
      options: {
        filter: true,
      },
    },
    {
      name: 'Site Name',
      options: {
        filter: true,
      },
    },
    {
      name: 'Incident Name',
      options: {
        filter: true,
      },
    },
    {
      name: 'Category',
      options: {
        filter: true,
      },
    },
    {
      name: 'Incident Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Submitted Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Reported By',
      options: {
        filter: true,
      },
    },
    {
      name: 'Status',
      options: {
        filter: true,
        customBodyRender: value => {
          if (value === 'active') {
            return <Chip label="Active" color="secondary" />;
          }
          if (value === 'non-active') {
            return <Chip label="Non Active" color="primary" />;
          }
          return <Chip label="Unknown" />;
        },
      },
    },
  ];

  const data = [
    ['1', 'ICN14', 'HEAD OFFICE', 'Test', 'Minor Injury', '03 Okt 2019', '03 Okt 2019', 'Alicia Carine', 'active'],
    ['2', 'ICN15', 'ARUMAYA RESIDENCE', 'Aliran listrik', 'Environmental Damage', '23 Okt 2019', '23 Okt 2019', 'Gunawan M R', 'active'],
    ['3', 'ICN16', 'ARUMAYA RESIDENCE', 'Bahaya terpeleset', 'Nearmiss', '23 Okt 2019', '23 Okt 2019', 'Katarina S', 'non-active', 'Attorney'],
    ['4', 'ICN17', 'Autograpgh ABC', 'Ada penurunan settlement tanah', 'Property Damage', '04 Mar 2020', '04 Mar 2020', 'Dimas Eko Prayoso', 'active'],
    ['5', 'ICN18', 'Autograpgh ABC', 'tertimpa material', 'Minor Injury', '04 Mar 2020', '04 Mar 2020', 'Ade Ismail', 'unknown'],
    ['6', 'ICN19', 'Autograpgh ABC', 'Lengan tangan hampir terpeleset', 'Nearmiss	', '04 Mar 2020', '04 Mar 2020', 'Tukijan', 'active'],
    ['7', 'ICN20', 'Autograpgh ABC', 'Seling baja putus dr hook crane service', 'Nearmiss	', '04 Mar 2020', '04 Mar 2020', 'Benny Octo Darliansyah', 'non-active'],
    ['8', 'ICN21', 'Autograpgh ABC', 'Org hampir terjatuh akibat tersandung', 'Nearmiss	', '04 Mar 2020', '04 Mar 2020', 'Alicia Carine', 'active'],
    ['9', 'ICN22', 'WORKSHOP JONGGOL', 'Tertabrak kambing', 'Minor Injury', '06 Mar 2020', '06 Mar 2020', 'Bambang Haryo Pratama', 'unknown'],
    ['10', 'ICN23', 'THE STATURE KEBON SIRIH', 'Terpukul Pipa', 'Minor Injury', '12 Mei 2020', '15 Mei 2020', 'Nabila Putri', 'active'],
    ['11', 'ICN24', 'CFPP Cirebon 2', 'Mobil terperosok ke parit', 'Nearmiss', '12 Mei 2020', '15 Mei 2020', 'ORYZA RACHMAHATI', 'non-active'],
    ['12', 'ICN25', 'PROJECT X', 'Pekerja terjatuh saat melakukan pekerjaan dan kakinya terluka', 'Minor Injury', '20 Mei 2020', '20 Mei 2020', 'Alicia Carine', 'active'],
    ['13', 'ICN26', 'PROJECT X', 'Orang Terjepit besi',  'Minor Injury', '20 Mei 2020', '20 Mei 2020', 'Alicia Carine', 'active'],
    ['14', 'ICN27', 'ARUMAYA RESIDENCE', 'Kolom Rebah', 'Nearmiss', '11 Jun 2020', '11 Jun 2020', 'Bambang Irianto', 'active'],
    ['15', 'ICN28', 'Project Testing', 'Test IR', 'Nearmiss', '15 Jun 2020', '15 Jun 2020', 'Investigator Testing', 'non-active'],
    ['16', 'ICN29', 'Project Testing', 'Testing IR', 'Nearmiss', '17 Jun 2020', '17 Jun 2020', 'System Testing', 'non-active'],
    ['17', 'ICN30', 'Project Testing', 'Test2', 'Nearmiss', '17 Jun 2020', '17 Jun 2020', 'System Testing', 'active'],
    ['18', 'ICN31', 'CFPP Cirebon 2	', 'Test', 'Nearmiss', '17 Jun 2020', '17 Jun 2020', 'ORYZA RACHMAHATI', 'active'],
    ['19', 'ICN32', 'THE STATURE KEBON SIRIH', 'Jatuh dari ketinggian', 'Fatality', '29 Mei 2020', '18 Jun 2020', 'Nabila Putri', 'active'],
    ['20', 'ICN33', 'THAMRIN NINE', 'Pengangkatan material besi menabrak bullnos kaca PT. Indalek', 'Property Damage', '25 Jun 2020', '26 Jun 2020', 'Chandra Subagiyo', 'non-active'],
    ['21', 'ICN34', 'CFPP Cirebon 2', 'Tertusuk paku', 'Minor Injury', '12 Jul 2020', '12 Jul 2020', 'ORYZA RACHMAHATI', 'unknown'],
    ['22', 'ICN35', 'CFPP Cirebon 2', 'Tertusuk paku', 'Minor Injury', '12 Jul 2020', '13 Jul 2020',  'ALFI SAHRI JAMIL MUNTHE', 'active'],
    ['23', 'ICN36', 'Diamond Tower', 'Trial', 'Minor Injury', '23 Jul 2020', '23 Jul 2020', 'Dian Afriyeni', 'active'],
    ['24', 'ICN37', 'Diamond Tower', 'Tumpahan soalr', 'Environmental Damage', '23 Jul 2020', '23 Jul 2020', 'Rifqi Paksi Nugroho', 'active'],
    ['25', 'ICN38', 'Diamond Tower', 'Testing', 'Nearmiss', '28 Jul 2020', '28 Jul 2020', 'Rifqi Paksi Nugroho', 'active'],
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Incident Report" data={data} columns={columns} options={options} />
    </div>
  );
}

ListIncident.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListIncident);
