import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import LinearProgress from '@material-ui/core/LinearProgress';
import Chip from '@material-ui/core/Chip';
import MUIDataTable from 'mui-datatables';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListGreenCard(props) {
  const columns = [
    {
      name: 'No',
      options: {
        filter: true,
      },
    },
    {
      name: 'Nama Karyawan',
      options: {
        filter: true,
      },
    },
    {
      name: 'PIC',
      options: {
        filter: true,
      },
    },
    {
      name: 'Jenis Bahaya',
      options: {
        filter: true,
      },
    },
    {
      name: 'Jenis Bahaya Spesifik',
      options: {
        filter: true,
      },
    },
    {
      name: 'Lokasi',
      options: {
        filter: true,
      },
    },
    {
      name: 'Tanggal',
      options: {
        filter: true,
      },
    },
    {
      name: 'Divisi',
      options: {
        filter: true,
      },
    },
    {
      name: 'Uraian Bahaya yang Ditemukan',
      options: {
        filter: true,
      },
    },
    {
      name: 'Tindakan yang Sudah Dilaksanakan',
      options: {
        filter: true,
      },
    },
    {
      name: 'Status',
      options: {
        filter: true,
      },
    },
    {
      name: 'RAB Amount',
      options: {
        filter: true,
        customBodyRender: value => {
          const nf = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
          });

          return nf.format(value);
        },
      },
    },
    {
      name: 'Status',
      options: {
        filter: true,
        customBodyRender: value => {
          if (value === 'active') {
            return <Chip label="Active" color="secondary" />;
          }
          if (value === 'non-active') {
            return <Chip label="Non Active" color="primary" />;
          }
          return <Chip label="Unknown" />;
        },
      },
    },
    {
      name: 'Division',
      options: {
        filter: true,
      },
    },
  ];

  const data = [
    ['1','Alicia Clarine', '', 'Kondisi tidak aman', 'Bahaya Listrik', 'Site Office', '23 Okt 2019', '', 'adanya potensi bahaya listrik', '', 'Rejected', 100000, 'active', 'Business Analyst'],
    ['2','AFIF ELFIYAN', '', 'Tindakan tidak aman', 'Bahaya Terpapar Bahan Berbahaya', 'WWT', '25 Jul 2020', '', 'Pekerja tidak memakai APD faceshield saat melakukan chiping beton', 'Mengarahkan dan breefing pekerja utk memakai faceshield setiap melakukan pekerjaan chiping', 'In Progress', 200000, 'active', 'Business Consultant'],
    ['3','Giyanto', '', 'Tindakan tidak aman', 'Bahaya Kesehatan Kerja', 'Site Office', '23 Okt 2019', '', 'Test', '', 'Rejected', 500000, 'non-active', 'Attorney'],
    ['4','Hasanudin', '', 'Kondisi tidak aman', 'Bahaya Tersandung/ Terpeleset',  'Test Pile', '31 Okt 2020', '', 'sudah pasang reling', 'sudah close', 'Rejected', 50000, 'active', 'Business Analyst'],
    ['5','Inspector Testing', '', 'Tindakan tidak aman', 'Bahaya Jatuh Dari Ketinggian', 'Jakarta', '03 Agt 2020', '', 'testing create ticket', 'testing online', 'In Progress', 75000, 'unknown', 'Business Consultant'],
    ['6','Komang Triyana', '', 'Kondisi tidak aman', 'Bahaya Listrik', 'Silo', '16 Mar 2020	', '', 'Terdapat kabel power yg di tanam', 'Isi tanda atau rambu', 'Rejected', 94000, 'active', 'Business Management Analyst'],
    ['7','Rokib', '', 'Kondisi tidak aman', 'Tidak Ada Barikade', 'Innotech Material Storage', '06 Nov 2020', '', 'Material diakses tanpa barikade', 'Memasang rubercone di area material', 'Rejected', 210000, 'non-active', 'Agency Legal Counsel'],
    ['8','Edi Riono', '', 'Kondisi tidak aman', 'Bahaya Kebakaran', 'Site Office', '23 Okt 2019', '', 'Rambut api', 'Rambut disemir', 'Rejected', 65000, 'active', 'Commercial Specialist'],
    ['9','Hendra Gunawan', '', 'Tindakan Aman', 'Housekeeping/Tata Griya', 'Podium', '21 Jul 2022', '', 'Area bersih', 'Mantap', 'In Progress', 77000, 'unknown', 'Business Analyst'],
    ['10','Iwan Amali', '', 'Kondisi tidak aman', 'Housekeeping', 'Bore Pile', '04 Nov 2020',	'', 'Kalau d pakai lg kena penyakit', 'Harus d simpan dengan baik', 'Rejected', 135000, 'active', 'Business Consultant'],
    ['11','Zulmy Rhamaditya Wibowo', '', 'Kondisi tidak aman', 'Bahaya Terpapar Bahan Berbahaya', 'ZONA 1', '30 Okt 2020', '', 'Tidak ada secondary contaiment', 'Memberikan secondary contaiment', 'Rejected', 420000, 'non-active', 'Attorney'],
    ['12','PIC Testing', '', 'Tindakan Tidak Aman', 'Bahaya Jatuh Dari Ketinggian', 'Jakarta', '22 Okt 2020', '', 'Testing play store', '', 'In Progress', 150000, 'active', 'Agency Legal Counsel'],
    ['13','AFIF ELFIYAN', '', 'Tindakan Tidak Aman', 'Bahaya Terpapar Bahan Berbahaya', 'WWT', '25 Jul 2020', '', 'Pekerja tidak memakai APD faceshield saat melakukan chiping beton', 'Mengarahkan dan breefing pekerja utk memakai faceshield setiap melakukan pekerjaan chiping', 'In Progress', 170000, 'active', 'Industrial Analyst'],
    ['14','Didin Alipudin', '', 'Kondisi tidak aman', 'Bahaya Benda Jatuh', 'Zone 1', '05 Nov 2020', '', 'Timber beam bekas tangga blm dirapihkan', 'Segera di angkat dan di rapihkan', 'Rejected', 90000, 'active', 'Business Analyst'],
    ['15','M. Awaluddinur', '', 'Tindakan Tidak Aman', 'Tidak Menggunakan APD', 'Pabrikasi', '27 Okt 2020', '', 'Hanya Memakai sarung tangan plastik saat pengelasan', 'Segera pakai sarung tangan las, dan di siapkan sarung tangan pas cadangan apa bila basah', 'Rejected', 33000, 'non-active', 'Business Consultant'],
    ['16','ORYZA RACHMAHATI', '', 'Kondisi tidak aman', 'Bahaya Tertabrak', 'CW Pipe', '07 Jul 2020', '', 'Xxx', '', 'Rejected', 295000, 'non-active', 'Business Management Analyst'],
    ['17','Dimas Eko Prayoso', '', 'Kondisi tidak aman', 'Bahaya Lifting', 'D-Wall', '06 Nov 2020', '', 'Terdapat sling yang tidak update', 'Segera di update', 'Rejected', 100000, 'active', 'Agency Legal Counsel'],
    ['18','Alicia Carine', '', 'Kondisi tidak aman', 'Bahaya Listrik', 'Site Office', '05 Mar 2020', '', 'kabel terkelupas berpotensi bahaya listrik', '', 'Rejected', 400000, 'active', 'Commercial Specialist'],
    ['19','Hasanudin', '', 'Kondisi tidak aman', 'Bahaya Tertabrak', 'Test Pile', '31 Okt 2020', '', 'pipa csl terlindas tertabrak', 'belum di pasang rel', 'Rejected', 110000, 'active', 'Industrial Analyst'],
    ['20','ALFI SAHRI JAMIL MUNTHE', '', 'Tindakan Tidak Aman', 'Housekeeping', 'Coal Run-Off', '07 Jul 2020', '', 'Sampah berserakan pada area genset yang berpotensi api', 'Kumpulkan dan buang ke tempat sampah sementara', 'In Progress', 220000, 'non-active', 'Computer Scientist'],
    ['21','Namuy Kurniawan', '', 'Kondisi tidak aman', 'Housekeeping', 'Estate Management',  '04 Agt 2020', '', 'Sampah menumpuk', 'Bersih kan dan rapihkan', 'In Progress', 180000, 'unknown', 'Corporate Counselor'],
    ['22','Hasanudin', '', 'Kondisi tidak aman', 'Bahaya Tersandung/ Terpeleset', 'Site Office', '25 Okt 2020', '', 'harus di gulung dirapihkan', 'biar rapih dan tidak cepat rusak', 'In Progress', 99000, 'active', 'Business Analyst'],
    ['23','Rokib', '', 'Kondisi tidak aman', 'Bahaya Terpapar Bahan Berbahaya', 'Innotech Temporary Material', '06 Nov 2020', '', 'Accu bekas tidak di simpan ke gudang b3', 'Memindahkan accu bekas ke gudang b3', 'Rejected', 90000, 'active', 'Agency Legal Counsel'],
    ['24','Julianto', '', 'Kondisi tidak aman', 'Tidak Ada Barikade', 'Zona 2 Switch yard dan GIS	', '26 Jul 2020', '', 'Tidak ada baricade untuk proteksi lubang u ditch', 'Segera di beri baricade untuk memproteksi area lubang', 'Rejected', 140000, 'active', 'Commercial Specialist'],
    ['25','Bambang Haryo Pratama', '', 'Tindakan Tidak Aman', 'Tidak Menggunakan APD', 'GEDUNG C LT 2', '04 Nov 2019', '', 'Sdr. Hanafi tidka menggunakan ApD dinlokasi berbahaya', 'Sp 3', 'In Progress', 330000, 'active', 'Attorney'],
  ];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Green Card Report" data={data} columns={columns} options={options} />
    </div>
  );
}

ListGreenCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListGreenCard);
