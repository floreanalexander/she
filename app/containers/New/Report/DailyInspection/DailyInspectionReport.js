import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import SearchIcon from '@material-ui/icons/Search';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import { NewPaperBlock, Loading } from 'dan-components';
import moment from 'moment';
import _ from 'lodash';
import ListDailyInspection from './ListDailyInspection';
import useGet from '../../../../hooks/useGet';
import usePost from '../../../../hooks/usePost';

import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  divider: {
    margin: `${theme.spacing(3)}px 0`,
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 220,
  },
  picker: {
    margin: `${theme.spacing(3)}px 5px`,
  },
  button: {
    margin: theme.spacing(1),
  },
  leftIcon: {
    marginRight: theme.spacing(1),
  },
});

function DailyInspectionReport(props) {
  const { classes } = props;
  const title = 'Report Daily Inspection';
  const description = 'Halaman Report Daily Inspection';

  const { responseGetData } = useGet();
  const { responsePostData } = usePost();

  const [loadingList, setLoadingList] = useState(true);
  const [loadingReport, setLoadingReport] = useState(true);

  const [listCompany, setListCompany] = useState([]);
  const [listDivision, setListDivision] = useState([]);
  const [listProject, setListProject] = useState([]);
  const [listDocumentType, setListDocumentType] = useState([]);

  const [dataState, setDataState] = useState({
    startCreateDate: '2022-02-24',
    endCreateDate: '2022-02-26',
    startDocDate: '2022-02-24',
    endDocDate: '2022-02-26',
    company: '0',
    division: '0',
    project: '0',
    document: '0',
  });
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [dataList, setDataList] = useState([]);

  const handleChange = event => {
    setDataState({
      ...dataState,
      [event.target.name]: event.target.value,
    });
  };
  const handleDateChange = (date, name) => {
    setDataState({
      ...dataState,
      [name]: date,
    });
  };

  const fetchListCompany = async () => {
    const result = await responseGetData('/api/company', false);
    if (!result) {
      setListCompany([]);
      return;
    }
    setListCompany(result);
  };

  const fetchListDivision = async () => {
    const result = await responseGetData('/api/division', false);
    if (!result) {
      setListDivision([]);
      return;
    }
    setListDivision(result);
  };

  const fetchListProject = async () => {
    const result = await responseGetData('/api/project/maintain', false);
    if (!result) {
      setListProject([]);
      return;
    }
    setListProject(result.data);
  };

  const fetchListDocumentType = async () => {
    const result = await responseGetData('/api/document-type', false);
    if (!result) {
      setListDocumentType([]);
      return;
    }
    setListDocumentType(result);
  };

  useEffect(() => {
    async function fetchDropdown() {
      await fetchListCompany();
      await fetchListDivision();
      await fetchListProject();
      await fetchListDocumentType();
      setLoadingList(false);
      await onSearch();
      setLoadingReport(false);
    }
    fetchDropdown();
  }, []);

  const onSearch = async () => {
    setLoadingReport(true);
    _.set(dataState, 'startCreateDate', moment(dataState.startCreateDate).format('YYYY-MM-DD'));
    _.set(dataState, 'endCreateDate', moment(dataState.endCreateDate).format('YYYY-MM-DD'));
    _.set(dataState, 'startDocDate', moment(dataState.startDocDate).format('YYYY-MM-DD'));
    _.set(dataState, 'endDocDate', moment(dataState.endDocDate).format('YYYY-MM-DD'));
    const result = await responsePostData('/api/report/leadtime-prospect', dataState, false);
    if (!result) {
      setDataList([]);
      setLoadingReport(false);
      return;
    } else {
      var tempList = [];
      result.data.map(item =>
        tempList.push([
          item.DocumentNo,
          item.DocumentType,
          item.DocumentDate,
          item.EntryDate,
          item.Leadtime,
          item.ProjectNo,
          item.ProjectName,
          item.Division,
          item.CompanyName,
        ]),
      );
      setDataList(tempList);
      setLoadingReport(false);
    }
  };

  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
      {loadingReport ? <Loading /> : <ListDailyInspection data={dataList} />}
    </div>
  );
}

DailyInspectionReport.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DailyInspectionReport);
