import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import LinearProgress from '@material-ui/core/LinearProgress';
import Chip from '@material-ui/core/Chip';
import MUIDataTable from 'mui-datatables';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListDailyInspection(props) {
  const columns = [
    {
      name: 'No',
      options: {
        filter: true,
      },
    },
    {
      name: 'Location of Finding',
      options: {
        filter: true,
      },
    },
    {
      name: 'Description of Finding',
      options: {
        filter: true,
      },
    },
    {
      name: 'Inspector',
      options: {
        filter: true,
      },
    },
    {
      name: 'PIC',
      options: {
        filter: true,
      },
    },
    {
      name: 'Target Closure Date',
      options: {
        filter: true,
      },
    },
    {
      name: 'Photo of Corrective Action Take',
      options: {
        filter: true,
      },
    },
    {
      name: 'Status Remarks',
      options: {
        filter: true,
      },
    },
  ];

  const data = [];

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
    selectableRows: 'none', // multiple, single, none
    selectableRowsHideCheckboxes: true,
    selectableRowsOnClick: false,
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      <MUIDataTable title="Daily Inspection Report" data={data} columns={columns} options={options} />
    </div>
  );
}

ListDailyInspection.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListDailyInspection);
