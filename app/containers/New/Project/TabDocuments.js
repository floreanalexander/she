import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import { PapperBlock } from 'dan-components';
import _ from 'lodash';
import moment from 'moment';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import useGet from '../../../hooks/useGet';

import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 350,
  },
  button: {
    margin: theme.spacing(1),
  },
  leftIcon: {
    marginRight: theme.spacing(1),
  },
});

function TabDocuments(props) {
  const { classes } = props;
  const user = useSelector(state => state.login.usersLogin);
  const { responseGetData } = useGet();

  const initForm = {
    flag: 0, // default 0 akan dihapus, setelah simpan berubah jadi 1
    DocDate: '', // 2022-06-09
    DocNumber: '',
    DocStatus: '', // Open | Close
    DocStatusId: '', // 1 | 2
    Title: '', // Document Type Name
    TitleCode: '', // Document Type Id,
    CreateDate: new Date(),
    CreateBy: user?.NRP,
  };

  const [loadingList, setLoadingList] = useState(true);
  const [dataState, setDataState] = useState(initForm);
  const [listDocumentType, setListDocumentType] = useState([]);
  const listStatus = [
    {
      Id: 1,
      Name: 'Open',
    },
    {
      Id: 2,
      Name: 'Close',
    },
  ];

  const [open, setOpen] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);

  const fetchListDocumentType = async () => {
    const result = await responseGetData('/api/document-type', false);
    if (!result) {
      setListDocumentType([]);
      return;
    }
    setListDocumentType(result);
  };

  useEffect(() => {
    async function fetchDropdown() {
      await fetchListDocumentType();
      setLoadingList(false);
    }
    fetchDropdown();
  }, []);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClickOpenDelete = item => {
    setDataState(item);
    setOpenDelete(true);
  };

  const handleClose = (event, reason) => {
    if (reason !== 'backdropClick') {
      setOpen(false);
      setDataState(initForm);
    }
  };
  const handleCloseDelete = (event, reason) => {
    if (reason !== 'backdropClick') {
      setOpenDelete(false);
      setDataState(initForm);
    }
  };

  const handleChangeDocumentType = (Id, DocumentName) => {
    setDataState({
      ...dataState,
      Title: DocumentName,
      TitleCode: Id,
    });
  };

  const handleDateChange = date => {
    console.log(date);
    setDataState({
      ...dataState,
      DocDate: moment(date).format('YYYY-MM-DD'),
    });
  };

  const handleChangeStatus = (Id, StatusName) => {
    setDataState({
      ...dataState,
      DocStatusId: Id,
      DocStatus: StatusName,
    });
  };

  const handleChange = event => {
    setDataState({
      ...dataState,
      [event.target.name]: event.target.value,
    });
  };

  const handleAdd = () => {
    const newData = [...props.dataState.documents];
    console.log(dataState);
    if (
      dataState.DocDate === '' ||
      dataState.DocNumber === '' ||
      dataState.DocStatusId === '' ||
      dataState.TitleCode === ''
    ) {
      alert('harap perhatikan isian');
      return;
    }
    _.set(dataState, 'DocDate', moment(dataState.DocDate).format('YYYY-MM-DD'));
    newData.push(dataState);
    props.handleChange(newData);
    handleClose();
  };

  const handleDelete = () => {
    const newData = [...props.dataState.documents];
    props.handleChange(
      _.filter(newData, function pick(n) {
        return n.DocNumber !== dataState.DocNumber;
      }),
    );
    handleCloseDelete();
  };

  return (
    <>
      <div style={{ textAlign: 'right' }}>
        <Button className={classes.button} variant="contained" color="secondary" onClick={handleClickOpen}>
          <AddIcon className={classes.leftIcon} />
          TAMBAH
        </Button>
      </div>
      {props.dataState.documents.map(item => {
        return (
          <PapperBlock title={item.Title}>
            <p style={{ display: 'flex' }}>
              <span style={{ width: '20%' }}>Status </span>
              <span style={{ width: '80%' }}>: {item.DocStatus}</span>
            </p>
            <p style={{ display: 'flex' }}>
              <span style={{ width: '20%' }}>Document Date </span>
              <span style={{ width: '80%' }}>: {moment(item.DocDate).format('DD/MM/YYYY')}</span>
            </p>
            <p style={{ display: 'flex' }}>
              <span style={{ width: '20%' }}>Document Number </span>
              <span style={{ width: '80%' }}>: {item.DocNumber}</span>
            </p>
            <p style={{ display: 'flex' }}>
              <span style={{ width: '20%' }}>Created Date </span>
              <span style={{ width: '80%' }}>: {moment(item.CreateDate).format('DD/MM/YYYY')}</span>
            </p>
            <p style={{ display: 'flex' }}>
              <span style={{ width: '20%' }}>Created By </span>
              <span style={{ width: '80%' }}>: {item.CreateBy}</span>
            </p>
            <div style={{ textAlign: 'right' }}>
              <Button
                className={classes.button}
                variant="contained"
                color="error"
                onClick={() => handleClickOpenDelete(item)}
              >
                <DeleteIcon />
              </Button>
            </div>
          </PapperBlock>
        );
      })}
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Tambah Dokumen</DialogTitle>
        <DialogContent>
          {/* <DialogContentText>
            To subscribe to this website, please enter your email address here. We will send updates occasionally.
          </DialogContentText> */}
          {/* <TextField autoFocus margin="dense" id="name" label="Email Address" type="email" fullWidth /> */}
          {loadingList ? (
            <div>Loading...</div>
          ) : (
            <div className={classes.root}>
              <FormControl className={classes.formControl} error={dataState.TitleCode === ''}>
                <InputLabel htmlFor="partner">Document Type</InputLabel>
                <Select
                  value={dataState.TitleCode}
                  inputProps={{
                    name: 'TitleCode',
                    id: 'partner',
                  }}
                >
                  <MenuItem value="">
                    <em>--Pilih Document Type--</em>
                  </MenuItem>
                  {listDocumentType.map(item => {
                    return (
                      <MenuItem
                        key={item.Id}
                        value={item.Id}
                        onClick={() => {
                          handleChangeDocumentType(item.Id, item.Name);
                        }}
                      >
                        {item.Name}
                      </MenuItem>
                    );
                  })}
                </Select>
                {dataState.TitleCode === '' && <FormHelperText>Harap Pilih Document Type</FormHelperText>}
              </FormControl>
              <FormControl className={classes.formControl} error={dataState.DocNumber === ''}>
                <InputLabel htmlFor="document-number">Document Number</InputLabel>
                <Input
                  inputProps={{
                    name: 'DocNumber',
                    id: 'document-number',
                  }}
                  value={dataState.DocNumber}
                  onChange={handleChange}
                />
                {dataState.DocNumber === '' && <FormHelperText>Harap Masukkan Document Number</FormHelperText>}
              </FormControl>
              <FormControl className={classes.formControl} error={dataState.DocDate === ''}>
                <MuiPickersUtilsProvider utils={MomentUtils}>
                  <KeyboardDatePicker
                    label="Document Date"
                    format="DD/MM/YYYY"
                    placeholder="Pilih Icon Calendar"
                    mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                    value={dataState.DocDate}
                    onChange={handleDateChange}
                    animateYearScrolling={false}
                  />
                </MuiPickersUtilsProvider>
              </FormControl>
              <FormControl className={classes.formControl} error={dataState.DocStatusId === ''}>
                <InputLabel htmlFor="partner">Document Status</InputLabel>
                <Select
                  value={dataState.DocStatusId}
                  inputProps={{
                    name: 'DocStatusId',
                    id: 'partner',
                  }}
                >
                  <MenuItem value="">
                    <em>--Pilih Document Status--</em>
                  </MenuItem>
                  {listStatus.map(item => {
                    return (
                      <MenuItem
                        key={item.Id}
                        value={item.Id}
                        onClick={() => {
                          handleChangeStatus(item.Id, item.Name);
                        }}
                      >
                        {item.Name}
                      </MenuItem>
                    );
                  })}
                </Select>
                {dataState.DocStatusId === '' && <FormHelperText>Harap Pilih Document Status</FormHelperText>}
              </FormControl>
            </div>
          )}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            CANCEL
          </Button>
          <Button onClick={() => handleAdd()} color="primary">
            ADD
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog open={openDelete} onClose={handleCloseDelete} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Delete Partner</DialogTitle>
        <DialogContent>
          <DialogContentText>Apakah Anda yakin akan menghapus partner?</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => handleDelete()} color="primary">
            YA
          </Button>
          <Button onClick={() => handleCloseDelete()} color="default">
            TIDAK
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

TabDocuments.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TabDocuments);
