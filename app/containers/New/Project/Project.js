import React, { useState } from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import { NewPaperBlock } from 'dan-components';
import ListProject from './ListProject';

import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  divider: {
    margin: `${theme.spacing(3)}px 0`,
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  button: {
    margin: theme.spacing(1),
  },
  leftIcon: {
    marginRight: theme.spacing(1),
  },
});

function Project(props) {
  const { classes } = props;
  const title = 'Project';
  const description = 'Halaman Project';

  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
      <NewPaperBlock style={{ paddingBottom: 0 }}>
        <div style={{ textAlign: 'right' }}>
          <Button
            className={classes.button}
            variant="contained"
            color="secondary"
            onClick={() => {
              props.history.push('/app/admin/project-list/tambah');
            }}
          >
            <AddIcon className={classes.leftIcon} />
            TAMBAH BARU
          </Button>
        </div>
      </NewPaperBlock>
      <ListProject />
    </div>
  );
}

Project.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(Project));
