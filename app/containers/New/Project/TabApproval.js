import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import AddIcon from '@material-ui/icons/Add';
import CheckIcon from '@material-ui/icons/Check';
import { useSelector } from 'react-redux';
import { PapperBlock } from 'dan-components';
import _ from 'lodash';
import useGet from '../../../hooks/useGet';
import usePost from '../../../hooks/usePost';

import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 350,
  },
  button: {
    margin: theme.spacing(1),
  },
  leftIcon: {
    marginRight: theme.spacing(1),
  },
});

function TabApproval(props) {
  const { classes } = props;
  const user = useSelector(state => state.login.usersLogin);
  const { responseGetData } = useGet();
  const { responsePostData } = usePost();

  const initForm = {
    ApprovalID: '',
    ReceiverID: '',
    ReceiverName: '',
    Notes: '',
    Status: '',
    PICID: '',
    CreateBy: user?.NRP, // ambil dari work type (Id)
  };

  const [dataState, setDataState] = useState(initForm);
  const [listPIC, setListPIC] = useState([]);

  const [open, setOpen] = useState(false);
  const [openApprove, setOpenApprove] = useState(false);

  const fetchListPIC = async id => {
    const result = await responseGetData('/api/approval-detail/list-pic/'.concat(id), false);
    if (!result) {
      setListPIC([]);
      return;
    }
    setListPIC(result.data);
  };

  const fetchAssign = async () => {
    const payload = {
      Id: dataState.idDB,
      ReceiverID: dataState.ReceiverID,
      ReceiverEmail: user?.EmailOffice,
      Notes: dataState.Notes,
      CreateBy: user?.NRP,
    };

    if (dataState.ReceiverID === '') {
      alert('PIC belum diisi!');
      return;
    }
    const result = await responsePostData('/api/approval-detail/assign', payload, false);
    console.log('fetchAssign result', result);
    if (result?.success) {
      await props.fetchUpdate();
      await props.fetchDetail();
      handleClose();
    }
  };

  const fetchApprove = async () => {
    const payload = {
      Id: dataState.idDB,
      ApprovalID: dataState.ApprovalID,
      ReceiverName: dataState.ReceiverName,
      CreateBy: user?.NRP,
    };
    const result = await responsePostData('/api/approval-detail/approve', payload, false);
    console.log('fetchApprove result', result);
    if (result?.success) {
      await props.fetchUpdate();
      await props.fetchDetail();
      handleCloseApprove();
    }
  };

  const handleClickOpen = async item => {
    setDataState(item);
    await fetchListPIC(item.PICID);
    setOpen(true);
  };
  const handleClickOpenApprove = item => {
    if (props.dataState.main_ProjectName === '') {
      alert('Project Name belum diisi!');
      return;
    }
    if (props.dataState.general_PIC === '') {
      alert('PIC belum diisi!');
      return;
    }
    if (props.dataState.main_ProjectName === '') {
      alert('Project Name belum diisi!');
      return;
    }
    setDataState(item);
    setOpenApprove(true);
  };

  const handleClose = (event, reason) => {
    if (reason !== 'backdropClick') {
      setOpen(false);
      setDataState(initForm);
    }
  };
  const handleCloseApprove = (event, reason) => {
    if (reason !== 'backdropClick') {
      setOpenApprove(false);
      setDataState(initForm);
    }
  };

  const handleChange = event => {
    setDataState({
      ...dataState,
      [event.target.name]: event.target.value,
    });
  };

  const handleChangePIC = (Id, PICName) => {
    setDataState({
      ...dataState,
      ReceiverID: Id,
      ReceiverName: PICName,
    });
  };

  return (
    <>
      {/* <div style={{ textAlign: 'right' }}>
        <Button className={classes.button} variant="contained" color="secondary" onClick={handleClickOpen}>
          <AddIcon className={classes.leftIcon} />
          TAMBAH
        </Button>
      </div> */}
      {console.log('datauser', user)}
      {props.dataState._approvals.map(item => {
        return (
          <PapperBlock title={item.Title}>
            <p style={{ display: 'flex' }}>
              <span style={{ width: '20%' }}>Status </span>
              <span style={{ width: '80%' }}>: {item.Status}</span>
            </p>
            <p style={{ display: 'flex' }}>
              <span style={{ width: '20%' }}>Notes </span>
              <span style={{ width: '80%' }}>: {item.Notes}</span>
            </p>
            <p style={{ display: 'flex' }}>
              <span style={{ width: '20%' }}>PIC </span>
              <span style={{ width: '80%' }}>: {item.ReceiverName}</span>
            </p>
            <div style={{ textAlign: 'right' }}>
              {(user.RoleId == 1 || user.RoleId == 3) && (
                <Button
                  className={classes.button}
                  variant="contained"
                  color="secondary"
                  onClick={() => handleClickOpen(item)}
                  disabled={item.ReceiverID !== ''}
                >
                  <AddIcon className={classes.leftIcon} />
                  ASSIGN
                </Button>
              )}
              {user.Id == item.ReceiverID && (
                <Button
                  className={classes.button}
                  variant="contained"
                  color="secondary"
                  onClick={() => handleClickOpenApprove(item)}
                  disabled={item.Index !== item.StepCurrent || item.Status === 'APPROVED'}
                >
                  <CheckIcon />
                  APPROVE
                </Button>
              )}
            </div>
          </PapperBlock>
        );
      })}
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Tambah Scope of Work</DialogTitle>
        <DialogContent>
          <div className={classes.root}>
            <FormControl className={classes.formControl} error={dataState.ReceiverID === ''}>
              <InputLabel htmlFor="pic">PIC</InputLabel>
              <Select
                value={dataState.ReceiverID}
                inputProps={{
                  name: 'ReceiverID',
                  id: 'pic',
                }}
              >
                <MenuItem value="">
                  <em>--Pilih PIC--</em>
                </MenuItem>
                {listPIC.map(item => {
                  return (
                    <MenuItem
                      key={item.Id}
                      value={item.Id}
                      onClick={() => {
                        handleChangePIC(item.Id, item.FullName);
                      }}
                    >
                      {item.FullName}
                    </MenuItem>
                  );
                })}
              </Select>
              {dataState.ReceiverID === '' && <FormHelperText>Harap Masukkan PIC</FormHelperText>}
            </FormControl>
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="notes">Notes</InputLabel>
              <Input
                inputProps={{
                  name: 'Notes',
                  id: 'notes',
                }}
                value={dataState.Notes}
                onChange={handleChange}
              />
            </FormControl>
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            CANCEL
          </Button>
          <Button onClick={() => fetchAssign()} color="primary">
            SAVE
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog open={openApprove} onClose={handleCloseApprove} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Approve</DialogTitle>
        <DialogContent>
          <DialogContentText>Apakah Anda yakin akan approve?</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => fetchApprove()} color="primary">
            YA
          </Button>
          <Button onClick={() => handleCloseApprove()} color="default">
            TIDAK
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

TabApproval.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TabApproval);
