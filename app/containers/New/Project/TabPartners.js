import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import { PapperBlock } from 'dan-components';
import _ from 'lodash';
import useGet from '../../../hooks/useGet';

import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 350,
  },
  button: {
    margin: theme.spacing(1),
  },
  leftIcon: {
    marginRight: theme.spacing(1),
  },
});

function TabPartners(props) {
  const { classes } = props;
  const { responseGetData } = useGet();

  const initForm = {
    flag: 0, // default 0 akan dihapus, setelah simpan berubah jadi 1
    id: 1, // default nama id value 1,setelah simpan berubah jadi idDB value generate sendiri
    partnerContactPerson: '', // ambil dari PartnerDetail (_contactPerson.Name)
    partnerContactPersonId: '', // ambil dari PartnerDetail (_contactPerson.Id)
    partnerFunction: '', // ambil dari partnerFUnction (Name)
    partnerFunctionId: '', // ambil dari partnerFUnction (Id)
    partnerName: '', // ambil dari partner (partnerName)
    partnerId: '', // ambil dari partner (Id)
  };

  const [loadingList, setLoadingList] = useState(true);
  const [dataState, setDataState] = useState(initForm);
  const [listPartner, setListPartner] = useState([]);
  const [listPartnerFunction, setListPartnerFunction] = useState([]);
  const [listPartnerCP, setListPartnerCP] = useState([]);

  const [open, setOpen] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);

  const fetchListPartner = async () => {
    const result = await responseGetData('/api/partner', false);
    if (!result) {
      setListPartner([]);
      return;
    }
    setListPartner(result);
  };

  const fetchListPartnerFunction = async () => {
    const result = await responseGetData('/api/partner-function', false);
    if (!result) {
      setListPartnerFunction([]);
      return;
    }
    setListPartnerFunction(result);
  };

  const fetchListPartnerCP = async id_partner => {
    const result = await responseGetData('/api/partner/detail/'.concat(id_partner), false);
    if (!result) {
      setListPartnerCP([]);
      return;
    }
    setListPartnerCP(result.data._contactPerson);
  };

  useEffect(() => {
    async function fetchDropdown() {
      await fetchListPartner();
      await fetchListPartnerFunction();
      setLoadingList(false);
    }
    fetchDropdown();
  }, []);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClickOpenDelete = item => {
    setDataState(item);
    setOpenDelete(true);
  };

  const handleClose = (event, reason) => {
    if (reason !== 'backdropClick') {
      setOpen(false);
      setDataState(initForm);
    }
  };
  const handleCloseDelete = (event, reason) => {
    if (reason !== 'backdropClick') {
      setOpenDelete(false);
      setDataState(initForm);
    }
  };

  const handleChangePartner = (Id, PartnerName) => {
    fetchListPartnerCP(Id);
    setDataState({
      ...dataState,
      partnerId: Id,
      partnerName: PartnerName,
    });
  };

  const handleChangeFunction = (Id, FunctionName) => {
    setDataState({
      ...dataState,
      partnerFunctionId: Id,
      partnerFunction: FunctionName,
    });
  };

  const handleChangeCP = (Id, CPName) => {
    setDataState({
      ...dataState,
      partnerContactPersonId: Id,
      partnerContactPerson: CPName,
    });
  };

  const handleAdd = () => {
    const newData = [...props.dataState.partners];
    if (dataState.partnerId === '') {
      alert('data partner belum diisi');
      return;
    }
    if (dataState.partnerFunctionId === '') {
      alert('data function belum diisi');
      return;
    }
    newData.push(dataState);
    props.handleChange(newData);
    handleClose();
  };

  const handleDelete = () => {
    const newData = [...props.dataState.partners];
    props.handleChange(
      _.filter(newData, function pick(n) {
        return n.partnerContactPersonId !== dataState.partnerContactPersonId;
      }),
    );
    handleCloseDelete();
  };

  return (
    <>
      <div style={{ textAlign: 'right' }}>
        <Button className={classes.button} variant="contained" color="secondary" onClick={handleClickOpen}>
          <AddIcon className={classes.leftIcon} />
          TAMBAH
        </Button>
      </div>
      {props.dataState.partners.map(item => {
        return (
          <PapperBlock title={item.partnerName}>
            <p style={{ display: 'flex' }}>
              <span style={{ width: '20%' }}>Function </span>
              <span style={{ width: '80%' }}>: {item.partnerFunction}</span>
            </p>
            <p style={{ display: 'flex' }}>
              <span style={{ width: '20%' }}>Contact Person </span>
              <span style={{ width: '80%' }}>: {item.partnerContactPerson}</span>
            </p>
            <div style={{ textAlign: 'right' }}>
              <Button
                className={classes.button}
                variant="contained"
                color="error"
                onClick={() => handleClickOpenDelete(item)}
              >
                <DeleteIcon />
              </Button>
            </div>
          </PapperBlock>
        );
      })}
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Tambah Partner</DialogTitle>
        <DialogContent>
          {/* <DialogContentText>
            To subscribe to this website, please enter your email address here. We will send updates occasionally.
          </DialogContentText> */}
          {/* <TextField autoFocus margin="dense" id="name" label="Email Address" type="email" fullWidth /> */}
          {loadingList ? (
            <div>Loading...</div>
          ) : (
            <div className={classes.root}>
              <FormControl className={classes.formControl} error={dataState.partnerId === ''}>
                <InputLabel htmlFor="partner">Partner</InputLabel>
                <Select
                  value={dataState.partnerId}
                  // onChange={handleChangePartner}
                  inputProps={{
                    name: 'partnerId',
                    id: 'partner',
                  }}
                >
                  <MenuItem value="">
                    <em>--Pilih Partner--</em>
                  </MenuItem>
                  {listPartner.map(item => {
                    return (
                      <MenuItem
                        key={item.Id}
                        value={item.Id}
                        onClick={() => {
                          handleChangePartner(item.Id, item.PartnerName);
                        }}
                      >
                        {item.PartnerName}
                      </MenuItem>
                    );
                  })}
                </Select>
                {dataState.partnerId === '' && <FormHelperText>Harap Pilih Partner</FormHelperText>}
              </FormControl>
              <FormControl className={classes.formControl} error={dataState.partnerFunctionId === ''}>
                <InputLabel htmlFor="function">Function</InputLabel>
                <Select
                  value={dataState.partnerFunctionId}
                  // onChange={handleChangeFunction}
                  inputProps={{
                    name: 'partnerFunctionId',
                    id: 'function',
                  }}
                >
                  <MenuItem value="">
                    <em>--Pilih Function--</em>
                  </MenuItem>
                  {listPartnerFunction.map(item => {
                    return (
                      <MenuItem
                        key={item.Id}
                        value={item.Id}
                        onClick={() => {
                          handleChangeFunction(item.Id, item.Name);
                        }}
                      >
                        {item.Name}
                      </MenuItem>
                    );
                  })}
                </Select>
                {dataState.partnerFunctionId === '' && <FormHelperText>Harap Pilih Function</FormHelperText>}
              </FormControl>
              <FormControl className={classes.formControl}>
                <InputLabel htmlFor="cp">Contact Person</InputLabel>
                <Select
                  value={dataState.partnerContactPersonId}
                  // onChange={handleChangeContactPerson}
                  inputProps={{
                    name: 'partnerContactPersonId',
                    id: 'cp',
                  }}
                >
                  <MenuItem value="">
                    <em>--Pilih Contact Person--</em>
                  </MenuItem>
                  {listPartnerCP.map(item => {
                    return (
                      <MenuItem
                        key={item.Id}
                        value={item.Id}
                        onClick={() => {
                          handleChangeCP(item.Id, item.Name);
                        }}
                      >
                        {item.Name}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </div>
          )}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            CANCEL
          </Button>
          <Button onClick={() => handleAdd()} color="primary">
            ADD
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog open={openDelete} onClose={handleCloseDelete} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Delete Partner</DialogTitle>
        <DialogContent>
          <DialogContentText>Apakah Anda yakin akan menghapus partner?</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => handleDelete()} color="primary">
            YA
          </Button>
          <Button onClick={() => handleCloseDelete()} color="default">
            TIDAK
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

TabPartners.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TabPartners);
