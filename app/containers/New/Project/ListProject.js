import React, { useEffect, useState } from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import Chip from '@material-ui/core/Chip';
import MUIDataTable from 'mui-datatables';
import Loading from 'dan-components/Loading';
import useGet from '../../../hooks/useGet';

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto',
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all',
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
      },
    },
  },
  button: {
    margin: theme.spacing(1),
  },
});
/*
  It uses npm mui-datatables. It's easy to use, you just describe columns and data collection.
  Checkout full documentation here :
  https://github.com/gregnb/mui-datatables/blob/master/README.md
*/
function ListProject(props) {
  const { responseGetData } = useGet();
  const [loading, setLoading] = useState(true);
  const [list, setList] = useState([[]]);
  const columns = [
    {
      name: 'Nama Project',
      options: {
        filter: true,
      },
    },
    {
      name: 'Division',
      options: {
        filter: true,
      },
    },
    {
      name: 'Sector',
      options: {
        filter: true,
      },
    },
    {
      name: 'Status',
      options: {
        filter: true,
        customBodyRender: value => {
          if (value === 'CREATED') {
            return <Chip label="CREATED" color="secondary" />;
          }
          if (value === 'PROSPECT') {
            return <Chip label="PROSPECT" color="primary" />;
          }
          return <Chip label={value} />;
        },
      },
    },
    {
      name: 'Action',
      options: {
        filter: false,
        customBodyRender: value => {
          return (
            <>
              <Tooltip title="Project">
                <IconButton
                  color="primary"
                  onClick={() => {
                    props.history.push(`/app/admin/project-list/${value}`);
                  }}
                  className={classes.button}
                  aria-label="Project"
                >
                  <Icon>edit</Icon>
                </IconButton>
              </Tooltip>
              <Tooltip title="Activities">
                <IconButton
                  color="secondary"
                  onClick={() => {
                    console.log(value);
                  }}
                  className={classes.button}
                  aria-label="Activities"
                >
                  <Icon>alarm</Icon>
                </IconButton>
              </Tooltip>
            </>
          );
        },
      },
    },
  ];

  const fetchList = async () => {
    const result = await responseGetData('/api/project/maintain', false);
    if (!result) {
      setList([]);
      return;
    }
    var tempList = [];
    result.data.map(item =>
      tempList.push([
        item.ProjectName,
        item.Division ? item.Division.Name : '',
        item.SubSector ? item.SubSector.Name : '',
        item.Status ? item.Status.StatusDescription : '',
        item.Id,
      ]),
    );
    console.log(tempList);
    setList(tempList);
  };

  useEffect(() => {
    async function fetch() {
      await fetchList();
      setLoading(false);
    }
    fetch();
  }, []);

  const options = {
    filterType: 'dropdown',
    responsive: 'vertical',
    print: true,
    rowsPerPage: 10,
    page: 0,
    textLabels: {
      body: {
        noMatch: 'No data available',
      },
    },
    selectableRows: 'single', // multiple, single, none
    selectableRowsHideCheckboxes: true,
    selectableRowsOnClick: false,
  };

  const { classes } = props;

  return (
    <div className={classes.table}>
      {loading ? <Loading /> : <MUIDataTable title="List Project" data={list} columns={columns} options={options} />}
    </div>
  );
}

ListProject.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(ListProject));
