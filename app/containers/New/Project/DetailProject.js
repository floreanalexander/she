import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import AppBar from '@material-ui/core/AppBar';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Typography from '@material-ui/core/Typography';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Checkbox from '@material-ui/core/Checkbox';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import CheckIcon from '@material-ui/icons/Check';
import SettingsIcon from '@material-ui/icons/Settings';
import RefreshIcon from '@material-ui/icons/Refresh';
import { useDispatch, useSelector } from 'react-redux';
import { NewPaperBlock, Loading } from 'dan-components';
import messageStyles from 'dan-styles/Messages.scss';
import _ from 'lodash';
import moment from 'moment';
import useGet from '../../../hooks/useGet';
import usePost from '../../../hooks/usePost';
import usePut from '../../../hooks/usePut';
import { showNotifAction } from '../../../redux/actions/notifActions';

import TabGeneral from './TabGeneral';
import TabLocation from './TabLocation';
import TabPartners from './TabPartners';
import TabDocuments from './TabDocuments';
import TabSow from './TabSow';
import TabAmounts from './TabAmounts';
import TabApproval from './TabApproval';

import { withStyles } from '@material-ui/core/styles';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography component={'span'}>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node.isRequired,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`,
  };
}

const styles = theme => ({
  divider: {
    margin: `${theme.spacing(3)}px 0`,
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
  },
  rootTab: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 300,
  },
  button: {
    margin: theme.spacing(1),
  },
  leftIcon: {
    marginRight: theme.spacing(1),
  },
  snackbar: {
    margin: theme.spacing(1),
  },
});

const requiredValidation = value => (value === null || value === '' ? false : true);

function DetailProject(props) {
  const { classes } = props;
  const user = useSelector(state => state.login.usersLogin);
  const title = 'Detail Partner';
  const description = 'Halaman Detail Partner';
  const dispatch = useDispatch();
  const { responseGetData } = useGet();
  const { responsePostData } = usePost();
  const { responsePutData } = usePut();

  const initForm = {
    Id: '',
    main_ProjectNumber: '0000000000',
    main_ProjectName: '',
    main_Status: 'INIT',
    main_ProspectNo: '',
    main_ProspectAmount: '',
    main_Tender: '',
    main_TenderAmount: '',
    general_Bank: '',
    general_Criteria: '',
    general_Division: '',
    general_EndDate: '',
    general_Facility: '',
    general_Informant: '',
    general_InformantRole: '',
    general_PIC: '',
    general_Positioning: '',
    general_SourceFund: '',
    general_StartDate: '',
    general_SubSector: '',
    location_Country: '',
    location_Province: '',
    location_City: '',
    location_Street: '',
    location_PostalCode: '',
    status_Drop: false,
    status_SetStatus: 1,
    status: { status_SetStatus: 1, status_Drop: false },
    partners: [],
    documents: [],
    sow: [],
    amounts: [],
    CreateBy: user?.NRP,
  };

  const [geografis, setGeografis] = useState(0);
  const [subsector, setSubsector] = useState(0);
  const [amount, setAmount] = useState(0);
  const [fund, setFund] = useState(0);
  const [positioning, setPositioning] = useState(0);

  const [loadingList, setLoadingList] = useState(true);
  const [loadingStatus, setLoadingStatus] = useState(true);
  const [openStatus, setOpenStatus] = useState(false);
  const [initState, setInitState] = useState(initForm);
  const [dataState, setDataState] = useState(initForm);

  const [tabValue, setTabValue] = React.useState(0);

  const [listCriteria, setListCriteria] = useState([]);
  const [listDivision, setListDivision] = useState([]);
  const [listSubsector, setListSubsector] = useState([]);
  const [listPositioning, setListPositioning] = useState([]);
  const [listSourceFund, setListSourceFund] = useState([]);
  const [listPIC, setListPIC] = useState([]);
  const [listInformantRole, setListInformantRole] = useState([]);
  const [listBank, setListBank] = useState([]);
  const [listCountry, setListCountry] = useState([]);
  const [listProvince, setListProvince] = useState([]);
  const [listFilterStatus, setListFilterStatus] = useState([]);

  const handleChangeTab = (event, newValue) => {
    setTabValue(newValue);
  };

  const fetchDetailProject = async () => {
    const result = await responseGetData(`/api/project/detail/${props.match.params.Id}`, true);
    if (!result) {
      return;
    }
    const temp = result.data;
    _.set(temp, 'CreateBy', user?.NRP);
    _.set(temp, 'partners', result.data._partners);
    _.set(temp, 'documents', result.data._documents);
    _.set(temp, 'sow', result.data._sows);
    _.set(temp, 'amounts', result.data._amounts);
    _.set(temp, 'status.status_SetStatus', result.data.status_SetStatus);
    _.set(temp, 'status.status_Drop', result.data.status_Drop);
    delete temp._partners;
    delete temp._documents;
    delete temp._sows;
    delete temp._amounts;
    setInitState(temp);
    setDataState(temp);
  };

  const fetchListCriteria = async () => {
    const result = await responseGetData('/api/criteria', false);
    if (!result) {
      setListCriteria([]);
      return;
    }
    setListCriteria(result);
  };

  const fetchListDivision = async () => {
    const result = await responseGetData('/api/division', false);
    if (!result) {
      setListDivision([]);
      return;
    }
    setListDivision(result);
  };

  const fetchListSubsector = async () => {
    const result = await responseGetData('/api/sub-sector', false);
    if (!result) {
      setListSubsector([]);
      return;
    }
    setListSubsector(result);
  };

  const fetchListPositioning = async () => {
    const result = await responseGetData('/api/positioning', false);
    if (!result) {
      setListPositioning([]);
      return;
    }
    setListPositioning(result);
  };

  const fetchListSourceFund = async () => {
    const result = await responseGetData('/api/source-of-fund', false);
    if (!result) {
      setListSourceFund([]);
      return;
    }
    setListSourceFund(result);
  };

  const fetchListPIC = async () => {
    const result = await responseGetData('/api/users/pic', false);
    if (!result) {
      setListPIC([]);
      return;
    }
    setListPIC(result.data);
  };

  const fetchListInformantRole = async () => {
    const result = await responseGetData('/api/informant-role', false);
    if (!result) {
      setListInformantRole([]);
      return;
    }
    setListInformantRole(result);
  };

  const fetchListBank = async () => {
    const result = await responseGetData('/api/bank', false);
    if (!result) {
      setListBank([]);
      return;
    }
    setListBank(result);
  };

  const fetchListCountry = async () => {
    const result = await responseGetData('/api/country', false);
    if (!result) {
      setListCountry([]);
      return;
    }
    setListCountry(result);
  };

  const fetchListProvince = async () => {
    const result = await responseGetData('/api/province', false);
    if (!result) {
      setListProvince([]);
      return;
    }
    setListProvince(result);
  };

  const fetchListFilterStatus = async () => {
    const payload = {
      High: dataState._status.StatusHigh,
      Low: dataState._status.StatusLow,
      Profile: dataState._status.StatusProfileId,
    };
    const result = await responsePostData('/api/status/filter', payload, false);
    if (!result) {
      setListFilterStatus([]);
      setLoadingStatus(false);
      return;
    }
    setListFilterStatus(result.data);
    setLoadingStatus(false);
  };

  useEffect(() => {
    async function fetchDropdown() {
      if (props.match.params.Id !== 'tambah') {
        await fetchDetailProject();
      }
      await fetchListCriteria();
      await fetchListDivision();
      await fetchListSubsector();
      await fetchListPositioning();
      await fetchListSourceFund();
      await fetchListPIC();
      await fetchListInformantRole();
      await fetchListBank();
      await fetchListCountry();
      await fetchListProvince();
      setLoadingList(false);
    }
    fetchDropdown();
  }, []);

  useEffect(() => {
    _.set(
      dataState,
      'main_ProspectAmount',
      dataState.amounts.length > 0
        ? dataState.amounts.filter(f => f.TitleId === 2).length > 0
          ? dataState.amounts.filter(f => f.TitleId === 2)[0].Amount
          : ''
        : '',
    );
    _.set(
      dataState,
      'main_TenderAmount',
      dataState.amounts.length > 0
        ? dataState.amounts.filter(f => f.TitleId === 1).length > 0
          ? dataState.amounts.filter(f => f.TitleId === 1)[0].Amount
          : ''
        : '',
    );
  }, [dataState.amounts]);

  useEffect(() => {
    // geografis
    _.find(listProvince, function (obj) {
      if (obj.Id === dataState.location_Province) {
        if (obj.Stp === 1) {
          setGeografis(1);
        } else if (obj.Stp === 0) {
          setGeografis(2);
        } else {
          setGeografis(0);
        }
        return true;
      }
    });
  }, [dataState, dataState.location_Province, listProvince]);

  useEffect(() => {
    // subsector
    if (dataState.general_SubSector !== '' && dataState.general_SubSector !== 999) {
      setSubsector(1);
    } else if (dataState.general_SubSector === 999) {
      setSubsector(2);
    } else {
      setSubsector(0);
    }
  }, [dataState, dataState.general_SubSector]);

  useEffect(() => {
    // amount
    if (dataState.amounts.length > 0) {
      if (dataState.general_SubSector !== '') {
        listSubsector
          .filter(f => f.Id === dataState.general_SubSector)
          .filter(f2 => {
            if (parseInt(dataState.main_ProspectAmount) >= parseInt(f2.StpAmount)) {
              setAmount(1);
            } else {
              setAmount(2);
            }
          });
      } else {
        setAmount(0);
      }
    }
  }, [dataState, dataState.amounts, dataState.general_SubSector, dataState.main_ProspectAmount, listSubsector]);

  useEffect(() => {
    // fund
    if (dataState.general_SourceFund === '') {
      setFund(0);
    } else if (dataState.general_SourceFund === 1 && dataState.general_Positioning === 3) {
      // JO Member & APBN
      setFund(1);
    } else if (dataState.general_SourceFund === 2 && dataState.general_Positioning === 3) {
      // JO Member & APBD
      setFund(1);
    } else if (dataState.general_SourceFund !== 1 && dataState.general_SourceFund !== 2) {
      setFund(1);
    } else {
      setFund(2);
    }
  }, [dataState, dataState.general_SourceFund, dataState.general_Positioning]);

  useEffect(() => {
    // positioning
    if (dataState.general_Positioning === '') {
      setPositioning(0);
    } else {
      setPositioning(1);
    }
  }, [dataState.general_Positioning]);

  const handleChange = event => {
    setDataState({
      ...dataState,
      [event.target.name]: event.target.value,
    });
  };

  const handleDateChange = (date, name) => {
    console.log(date);
    console.log(name);
    setDataState({
      ...dataState,
      [name]: date,
    });
  };

  const handleChangeCB = name => event => {
    setDataState({
      ...dataState,
      [name]: event.target.checked ? 1 : 0,
    });
  };

  const handleChangePIC = item => {
    setDataState({
      ...dataState,
      general_PIC: item.UserId,
      CompanyID: item.CompanyID,
    });
  };

  const handleChangeTabPartner = values => {
    setDataState({
      ...dataState,
      partners: values,
    });
  };

  const handleChangeTabDocument = values => {
    setDataState({
      ...dataState,
      documents: values,
    });
  };

  const handleChangeTabSow = values => {
    setDataState({
      ...dataState,
      sow: values,
    });
  };

  const handleChangeTabAmount = values => {
    setDataState({
      ...dataState,
      amounts: values,
    });
  };

  const onSubmit = async redirect => {
    if (dataState.main_ProjectName === '' || dataState.general_Division === '') {
      dispatch(showNotifAction('Perhatikan lagi isian'));
      return;
    }
    _.set(dataState, 'general_StartDate', moment(dataState.general_StartDate).format('MM/DD/YYYY'));
    _.set(dataState, 'general_EndDate', moment(dataState.general_EndDate).format('MM/DD/YYYY'));
    delete dataState._status;
    let result = null;
    if (props.match.params.Id === 'tambah') {
      result = await responsePostData('/api/project', dataState, true);
    } else {
      result = await responsePutData('/api/project', dataState, true);
    }
    if (!result) {
      return;
    } else {
      if (redirect) {
        props.history.push('/app/admin/project-list');
      }
    }
  };

  const handleClickOpenStatus = () => {
    fetchListFilterStatus();
    setOpenStatus(true);
  };

  const handleCloseStatus = (event, reason) => {
    if (reason !== 'backdropClick') {
      setOpenStatus(false);
    }
  };

  const handleSetStatus = () => {
    handleCloseStatus();
  };

  const handleChangeStatus = event => {
    _.set(dataState, 'status.status_SetStatus', event.target.value);
  };

  const handleChangeDropProject = event => {
    console.log(event.target.checked);
    _.set(dataState, 'status.status_Drop', event.target.checked);
    setDataState({
      ...dataState,
      status: {
        status_SetStatus: dataState.status.status_SetStatus,
        status_Drop: event.target.checked,
      },
    });
  };

  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
      {loadingList ? (
        <Loading />
      ) : (
        <NewPaperBlock style={{ paddingBottom: 0 }}>
          <div className={classes.root}>
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="project-number">Project Number</InputLabel>
              <Input
                inputProps={{
                  name: 'main_ProjectNumber',
                  id: 'project-number',
                }}
                value={dataState.main_ProjectNumber}
                onChange={handleChange}
                disabled
              />
              <FormHelperText>Project number will be generated by system automatically</FormHelperText>
            </FormControl>
            <FormControl className={classes.formControl} error={!requiredValidation(dataState.main_ProjectName)}>
              <InputLabel htmlFor="project-name">Project Name</InputLabel>
              <Input
                inputProps={{
                  name: 'main_ProjectName',
                  id: 'project-name',
                }}
                value={dataState.main_ProjectName}
                onChange={handleChange}
              />
              {!requiredValidation(dataState.main_ProjectName) ? (
                <FormHelperText>Harus diisi</FormHelperText>
              ) : (
                <FormHelperText>Contoh: Eclipse Project</FormHelperText>
              )}
            </FormControl>
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="status">Status</InputLabel>
              <Input
                inputProps={{
                  name: 'main_Status',
                  id: 'status',
                }}
                value={dataState.main_Status}
                onChange={handleChange}
                disabled
              />
            </FormControl>
          </div>
          <div className={classes.root}>
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="prospect-no">Prospect No.</InputLabel>
              <Input
                inputProps={{
                  name: 'main_ProspectNo',
                  id: 'prospect-no',
                }}
                value={dataState.main_ProspectNo}
                onChange={handleChange}
                disabled
              />
            </FormControl>
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="prospect-amount">Prospect Amount</InputLabel>
              <Input
                inputProps={{
                  name: 'main_ProspectAmount',
                  id: 'prospect-amount',
                }}
                value={dataState.main_ProspectAmount}
                // onChange={handleChange}
                disabled
              />
              <FormHelperText>Rp {dataState.main_ProspectAmount}</FormHelperText>
            </FormControl>
          </div>
          <div className={classes.root}>
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="tender-no">Tender No.</InputLabel>
              <Input
                inputProps={{
                  name: 'main_Tender',
                  id: 'tender-no',
                }}
                value={dataState.main_Tender}
                onChange={handleChange}
                disabled
              />
            </FormControl>
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="tender-amount">Tender Amount</InputLabel>
              <Input
                inputProps={{
                  name: 'main_TenderAmount',
                  id: 'tender-amount',
                }}
                value={dataState.main_TenderAmount}
                // onChange={handleChange}
                disabled
              />
              <FormHelperText>Rp {dataState.main_TenderAmount}</FormHelperText>
            </FormControl>
          </div>
          {props.match.params.Id !== 'Tambah' && (
            <div style={{ textAlign: 'right' }}>
              <Button
                className={classes.button}
                variant="contained"
                color="default"
                onClick={() => {
                  handleClickOpenStatus();
                }}
              >
                <SettingsIcon className={classes.leftIcon} />
                SET STATUS
              </Button>
              <Button
                className={classes.button}
                variant="contained"
                color="default"
                onClick={() => {
                  console.log('dataState', dataState);
                }}
              >
                <SettingsIcon className={classes.leftIcon} />
                DEBUG
              </Button>
            </div>
          )}
          <div>
            <SnackbarContent
              className={classNames(
                classes.snackbar,
                geografis === 0
                  ? messageStyles.bgInfo
                  : geografis === 1
                  ? messageStyles.bgSuccess
                  : messageStyles.bgError,
              )}
              message="Segmenting Geografis"
            />
            <SnackbarContent
              className={classNames(
                classes.snackbar,
                subsector === 0
                  ? messageStyles.bgInfo
                  : subsector === 1
                  ? messageStyles.bgSuccess
                  : messageStyles.bgError,
              )}
              message="Sub Sector"
            />
            <SnackbarContent
              className={classNames(
                classes.snackbar,
                amount === 0 ? messageStyles.bgInfo : amount === 1 ? messageStyles.bgSuccess : messageStyles.bgError,
              )}
              message="Amount"
            />
            <SnackbarContent
              className={classNames(
                classes.snackbar,
                fund === 0 ? messageStyles.bgInfo : fund === 1 ? messageStyles.bgSuccess : messageStyles.bgError,
              )}
              message="Source of Fund"
            />
            <SnackbarContent
              className={classNames(
                classes.snackbar,
                positioning === 0
                  ? messageStyles.bgInfo
                  : positioning === 1
                  ? messageStyles.bgSuccess
                  : messageStyles.bgError,
              )}
              message="Positioning"
            />
          </div>
          <br />
          <div className={classes.rootTab}>
            <AppBar position="static" color="default">
              <Tabs
                value={tabValue}
                onChange={handleChangeTab}
                indicatorColor="primary"
                textColor="primary"
                variant="scrollable"
                scrollButtons="auto"
                aria-label="scrollable auto tabs example"
              >
                <Tab label="General" {...a11yProps(0)} />
                <Tab label="Location" {...a11yProps(1)} />
                <Tab label="Partners" {...a11yProps(2)} />
                <Tab label="Documents" {...a11yProps(3)} />
                <Tab label="Scope of Works" {...a11yProps(4)} />
                <Tab label="Amount" {...a11yProps(5)} />
                <Tab label="Approval" {...a11yProps(6)} />
              </Tabs>
            </AppBar>
            <TabPanel value={tabValue} index={0}>
              <TabGeneral
                dataState={dataState}
                handleChange={handleChange}
                handleChangePIC={item => handleChangePIC(item)}
                handleDateChange={handleDateChange}
                listCriteria={listCriteria}
                listDivision={listDivision}
                listSubsector={listSubsector}
                listPositioning={listPositioning}
                listSourceFund={listSourceFund}
                listPIC={listPIC}
                listInformantRole={listInformantRole}
                listBank={listBank}
              />
            </TabPanel>
            <TabPanel value={tabValue} index={1}>
              <TabLocation
                dataState={dataState}
                handleChange={handleChange}
                listCountry={listCountry}
                listProvince={listProvince}
              />
            </TabPanel>
            <TabPanel value={tabValue} index={2}>
              <TabPartners dataState={dataState} handleChange={handleChangeTabPartner} />
            </TabPanel>
            <TabPanel value={tabValue} index={3}>
              <TabDocuments dataState={dataState} handleChange={handleChangeTabDocument} />
            </TabPanel>
            <TabPanel value={tabValue} index={4}>
              <TabSow dataState={dataState} handleChange={handleChangeTabSow} />
            </TabPanel>
            <TabPanel value={tabValue} index={5}>
              <TabAmounts dataState={dataState} handleChange={handleChangeTabAmount} />
            </TabPanel>
            <TabPanel value={tabValue} index={6}>
              <TabApproval
                dataState={dataState}
                fetchDetail={() => fetchDetailProject()}
                fetchUpdate={() => onSubmit(false)}
              />
            </TabPanel>
          </div>
          <div style={{ textAlign: 'center' }}>
            <Button
              className={classes.button}
              variant="contained"
              color="secondary"
              onClick={() => {
                onSubmit(true);
              }}
            >
              <CheckIcon className={classes.leftIcon} />
              SIMPAN
            </Button>
            <Button
              className={classes.button}
              variant="contained"
              onClick={() => {
                setDataState(initState);
              }}
            >
              <RefreshIcon className={classes.leftIcon} />
              RESET
            </Button>
          </div>
          <Dialog open={openStatus} onClose={handleCloseStatus} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">Manage Status</DialogTitle>
            <DialogContent>
              {loadingStatus ? (
                <div>Loading...</div>
              ) : (
                <div className={classes.root}>
                  <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="main-status">Status</InputLabel>
                    <Input
                      inputProps={{
                        name: 'main_Status',
                        id: 'main-status',
                      }}
                      value={dataState.main_Status}
                      disabled
                    />
                  </FormControl>
                  <FormControl
                    className={classes.formControl}
                    error={!requiredValidation(dataState.status.status_SetStatus)}
                  >
                    <InputLabel htmlFor="status">Status</InputLabel>
                    <Select
                      value={dataState.status.status_SetStatus}
                      onChange={handleChangeStatus}
                      inputProps={{
                        name: 'status',
                        id: 'status',
                      }}
                    >
                      <MenuItem value="">
                        <em>--Pilih Status--</em>
                      </MenuItem>
                      {listFilterStatus.map(item => {
                        return (
                          <MenuItem key={item.Id} value={item.Id}>
                            {item.StatusDescription}
                          </MenuItem>
                        );
                      })}
                    </Select>
                    {!requiredValidation(dataState.status.status_SetStatus) && (
                      <FormHelperText>Harap pilih Status</FormHelperText>
                    )}
                  </FormControl>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={dataState.status.status_Drop}
                        onChange={e => handleChangeDropProject(e)}
                        value="status_Drop"
                        color="primary"
                      />
                    }
                    label="Drop Project"
                  />
                </div>
              )}
            </DialogContent>
            <DialogActions>
              <Button onClick={() => handleSetStatus()} color="primary">
                YA
              </Button>
              <Button onClick={() => handleCloseStatus()} color="default">
                TIDAK
              </Button>
            </DialogActions>
          </Dialog>
        </NewPaperBlock>
      )}
    </div>
  );
}

DetailProject.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DetailProject);
