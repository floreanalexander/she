import React from 'react';
import PropTypes from 'prop-types';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import _ from 'lodash';

import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 350,
  },
});

const requiredValidation = value => (value === null || value === '' ? false : true);

function TabGeneral(props) {
  const { classes } = props;

  return (
    <>
      <div className={classes.root}>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="criteria">Criteria</InputLabel>
          <Select
            value={props.dataState.general_Criteria}
            onChange={props.handleChange}
            inputProps={{
              name: 'general_Criteria',
              id: 'criteria',
            }}
          >
            <MenuItem value="">
              <em>--Pilih Criteria--</em>
            </MenuItem>
            {props.listCriteria.map(item => {
              return (
                <MenuItem key={item.Id} value={item.Id}>
                  {item.Name}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
        <FormControl className={classes.formControl} error={!requiredValidation(props.dataState.general_Division)}>
          <InputLabel htmlFor="division">Division</InputLabel>
          <Select
            value={props.dataState.general_Division}
            onChange={props.handleChange}
            inputProps={{
              name: 'general_Division',
              id: 'division',
            }}
          >
            <MenuItem value="">
              <em>--Pilih Division--</em>
            </MenuItem>
            {props.listDivision.map(item => {
              return (
                <MenuItem key={item.Id} value={item.Id}>
                  {item.Name}
                </MenuItem>
              );
            })}
          </Select>
          {!requiredValidation(props.dataState.general_Division) && (
            <FormHelperText>Harap pilih Division</FormHelperText>
          )}
        </FormControl>
      </div>
      <div className={classes.root}>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="sub-sector">Sub Sector</InputLabel>
          <Select
            value={props.dataState.general_SubSector}
            onChange={props.handleChange}
            inputProps={{
              name: 'general_SubSector',
              id: 'sub-sector',
            }}
          >
            <MenuItem value="">
              <em>--Pilih Sub Sector--</em>
            </MenuItem>
            {props.listSubsector.map(item => {
              return (
                <MenuItem key={item.Id} value={item.Id}>
                  {item.Name}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
        <FormControl className={classes.formControl} error={!requiredValidation(props.dataState.general_Division)}>
          <InputLabel htmlFor="division">Positioning</InputLabel>
          <Select
            value={props.dataState.general_Positioning}
            onChange={props.handleChange}
            inputProps={{
              name: 'general_Positioning',
              id: 'division',
            }}
          >
            <MenuItem value="">
              <em>--Pilih Positioning--</em>
            </MenuItem>
            {props.listPositioning.map(item => {
              return (
                <MenuItem key={item.Id} value={item.Id}>
                  {item.Name}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
      </div>
      <div className={classes.root}>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="source-fund">Source of Fund</InputLabel>
          <Select
            value={props.dataState.general_SourceFund}
            onChange={props.handleChange}
            inputProps={{
              name: 'general_SourceFund',
              id: 'source-fund',
            }}
          >
            <MenuItem value="">
              <em>--Pilih Source of Fund--</em>
            </MenuItem>
            {props.listSourceFund.map(item => {
              return (
                <MenuItem key={item.Id} value={item.Id}>
                  {item.Name}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="pic">PIC</InputLabel>
          <Select
            value={props.dataState.general_PIC}
            // onChange={props.handleChange}
            inputProps={{
              name: 'general_PIC',
              id: 'pic',
            }}
          >
            <MenuItem value="" onClick={() => props.handleChangePIC({ UserId: '', CompanyId: '' })}>
              <em>--Pilih PIC--</em>
            </MenuItem>
            {props.listPIC.map(item => {
              return (
                <MenuItem key={item.UserId} value={item.UserId} onClick={() => props.handleChangePIC(item)}>
                  {item.FullName}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
      </div>
      <div className={classes.root}>
        <FormControl className={classes.formControl}>
          <MuiPickersUtilsProvider utils={MomentUtils}>
            <KeyboardDatePicker
              label="Start Date"
              format="DD/MM/YYYY"
              placeholder="10/10/2018"
              mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
              value={props.dataState.general_StartDate}
              onChange={e => props.handleDateChange(e, 'general_StartDate')}
              animateYearScrolling={false}
            />
          </MuiPickersUtilsProvider>
        </FormControl>
        <FormControl className={classes.formControl}>
          <MuiPickersUtilsProvider utils={MomentUtils}>
            <KeyboardDatePicker
              label="End Date"
              format="DD/MM/YYYY"
              placeholder="10/10/2018"
              mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
              value={props.dataState.general_EndDate}
              onChange={e => props.handleDateChange(e, 'general_EndDate')}
              animateYearScrolling={false}
            />
          </MuiPickersUtilsProvider>
        </FormControl>
      </div>
      <div className={classes.root}>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="informant">Informant</InputLabel>
          <Input
            inputProps={{
              name: 'general_Informant',
              id: 'informant',
            }}
            value={props.dataState.general_Informant}
            onChange={props.handleChange}
          />
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="informant-role">Informant Role</InputLabel>
          <Select
            value={props.dataState.general_InformantRole}
            onChange={props.handleChange}
            inputProps={{
              name: 'general_InformantRole',
              id: 'informant-role',
            }}
          >
            <MenuItem value="">
              <em>--Pilih Informant Role--</em>
            </MenuItem>
            {props.listInformantRole.map(item => {
              return (
                <MenuItem key={item.Id} value={item.Id}>
                  {item.Name}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
      </div>
      <div className={classes.root}>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="bank">Bank</InputLabel>
          <Select
            value={props.dataState.general_Bank}
            onChange={props.handleChange}
            inputProps={{
              name: 'general_Bank',
              id: 'bank',
            }}
          >
            <MenuItem value="">
              <em>--Pilih Bank--</em>
            </MenuItem>
            {props.listBank.map(item => {
              return (
                <MenuItem key={item.Id} value={item.Id}>
                  {item.Name}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="facility">Facility</InputLabel>
          <Input
            inputProps={{
              name: 'general_Facility',
              id: 'facility',
            }}
            value={props.dataState.general_Facility}
            onChange={props.handleChange}
          />
          <FormHelperText>Rp. {props.dataState.general_Facility}</FormHelperText>
          <FormHelperText>Masukkan nominal, contoh: 9500000</FormHelperText>
        </FormControl>
      </div>
    </>
  );
}

TabGeneral.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TabGeneral);
