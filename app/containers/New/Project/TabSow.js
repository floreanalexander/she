import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import { PapperBlock } from 'dan-components';
import _ from 'lodash';
import useGet from '../../../hooks/useGet';

import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 350,
  },
  button: {
    margin: theme.spacing(1),
  },
  leftIcon: {
    marginRight: theme.spacing(1),
  },
});

function TabSow(props) {
  const { classes } = props;
  const { responseGetData } = useGet();

  const initForm = {
    Flag: 0, // default 0 akan dihapus, setelah simpan berubah jadi 1
    Title: '',
    Note: '',
    EstimatedValue: '',
    WorkType: '', // ambil dari work type (Name)
    WorkTypeId: '', // ambil dari work type (Id)
  };

  const [loadingList, setLoadingList] = useState(true);
  const [dataState, setDataState] = useState(initForm);
  const [listSow, setListSow] = useState([]);

  const [open, setOpen] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);

  const fetchListSow = async () => {
    const result = await responseGetData('/api/sow-type', false);
    if (!result) {
      setListSow([]);
      return;
    }
    setListSow(result);
  };

  useEffect(() => {
    async function fetchDropdown() {
      await fetchListSow();
      setLoadingList(false);
    }
    fetchDropdown();
  }, []);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClickOpenDelete = item => {
    setDataState(item);
    setOpenDelete(true);
  };

  const handleClose = (event, reason) => {
    if (reason !== 'backdropClick') {
      setOpen(false);
      setDataState(initForm);
    }
  };
  const handleCloseDelete = (event, reason) => {
    if (reason !== 'backdropClick') {
      setOpenDelete(false);
      setDataState(initForm);
    }
  };

  const handleChange = event => {
    setDataState({
      ...dataState,
      [event.target.name]: event.target.value,
    });
  };

  const handleChangeSow = (Id, SowName) => {
    setDataState({
      ...dataState,
      WorkTypeId: Id,
      WorkType: SowName,
    });
  };

  const handleAdd = () => {
    const newData = [...props.dataState.sow];
    if (dataState.Title === '') {
      alert('data description belum diisi');
      return;
    }
    newData.push(dataState);
    props.handleChange(newData);
    handleClose();
  };

  const handleDelete = () => {
    const newData = [...props.dataState.sow];
    props.handleChange(
      _.filter(newData, function pick(n) {
        return n.Title !== dataState.Title;
      }),
    );
    handleCloseDelete();
  };

  return (
    <>
      <div style={{ textAlign: 'right' }}>
        <Button className={classes.button} variant="contained" color="secondary" onClick={handleClickOpen}>
          <AddIcon className={classes.leftIcon} />
          TAMBAH
        </Button>
      </div>
      {props.dataState.sow.map(item => {
        return (
          <PapperBlock title={item.Title}>
            <p style={{ display: 'flex' }}>
              <span style={{ width: '20%' }}>Work Type </span>
              <span style={{ width: '80%' }}>: {item.WorkType}</span>
            </p>
            <p style={{ display: 'flex' }}>
              <span style={{ width: '20%' }}>Note </span>
              <span style={{ width: '80%' }}>: {item.Note}</span>
            </p>
            <p style={{ display: 'flex' }}>
              <span style={{ width: '20%' }}>Estimated Value </span>
              <span style={{ width: '80%' }}>: {item.EstimatedValue}</span>
            </p>
            <div style={{ textAlign: 'right' }}>
              <Button
                className={classes.button}
                variant="contained"
                color="error"
                onClick={() => handleClickOpenDelete(item)}
              >
                <DeleteIcon />
              </Button>
            </div>
          </PapperBlock>
        );
      })}
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Tambah Scope of Work</DialogTitle>
        <DialogContent>
          {/* <DialogContentText>
            To subscribe to this website, please enter your email address here. We will send updates occasionally.
          </DialogContentText> */}
          {/* <TextField autoFocus margin="dense" id="name" label="Email Address" type="email" fullWidth /> */}
          {loadingList ? (
            <div>Loading...</div>
          ) : (
            <div className={classes.root}>
              <FormControl className={classes.formControl} error={dataState.Title === ''}>
                <InputLabel htmlFor="title">Description</InputLabel>
                <Input
                  inputProps={{
                    name: 'Title',
                    id: 'title',
                  }}
                  value={dataState.Title}
                  onChange={handleChange}
                />
                {dataState.Title === '' && <FormHelperText>Masukkan Description</FormHelperText>}
              </FormControl>
              <FormControl className={classes.formControl}>
                <InputLabel htmlFor="note">Note</InputLabel>
                <Input
                  inputProps={{
                    name: 'Note',
                    id: 'note',
                  }}
                  value={dataState.Note}
                  onChange={handleChange}
                />
              </FormControl>
              <FormControl className={classes.formControl}>
                <InputLabel htmlFor="estimated-value">Estimated Value</InputLabel>
                <Input
                  inputProps={{
                    name: 'EstimatedValue',
                    id: 'estimated-value',
                  }}
                  value={dataState.EstimatedValue}
                  onChange={handleChange}
                />
                <FormHelperText>Rp {dataState.EstimatedValue}</FormHelperText>
                <FormHelperText>Masukkan nominal, contoh: 9500000</FormHelperText>
              </FormControl>
              <FormControl className={classes.formControl}>
                <InputLabel htmlFor="sow">Work Type</InputLabel>
                <Select
                  value={dataState.WorkTypeId}
                  inputProps={{
                    name: 'WorkTypeId',
                    id: 'sow',
                  }}
                >
                  <MenuItem value="">
                    <em>--Pilih Work Type--</em>
                  </MenuItem>
                  {listSow.map(item => {
                    return (
                      <MenuItem
                        key={item.Id}
                        value={item.Id}
                        onClick={() => {
                          handleChangeSow(item.Id, item.Name);
                        }}
                      >
                        {item.Name}
                      </MenuItem>
                    );
                  })}
                </Select>
              </FormControl>
            </div>
          )}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            CANCEL
          </Button>
          <Button onClick={() => handleAdd()} color="primary">
            ADD
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog open={openDelete} onClose={handleCloseDelete} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Delete Partner</DialogTitle>
        <DialogContent>
          <DialogContentText>Apakah Anda yakin akan menghapus partner?</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => handleDelete()} color="primary">
            YA
          </Button>
          <Button onClick={() => handleCloseDelete()} color="default">
            TIDAK
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

TabSow.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TabSow);
