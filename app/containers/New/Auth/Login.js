import React from 'react';
import { Helmet } from 'react-helmet';
import brand from 'dan-api/dummy/brand';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { useDispatch } from 'react-redux';
import { LoginForm } from 'dan-components';
import styles from 'dan-components/Forms/user-jss';
import usePost from '../../../hooks/usePost';
import { initAction } from '../../../redux/actions/reduxFormActions';

function Login(props) {
  const { responsePostData } = usePost();
  const dispatch = useDispatch();

  const submitForm = async values => {
    const result = await responsePostData('/api/users/login', { Password: values.Password, Username: values.Username });
    if (result.success) {
      const payload = {
        Id: result.data.ProfileUser[0].Id,
        Username: result.data.ProfileUser[0].Username,
        RoleId: result.data.ProfileUser[0].RoleId,
        RoleInitial: result.data.ProfileUser[0].RoleInitial,
        NRP: result.data.ProfileUser[1].NRP,
        FullName: result.data.ProfileUser[1].FullName,
        Gender: result.data.ProfileUser[1].Gender,
        Phone: result.data.ProfileUser[1].Phone,
        EmailPrivate: result.data.ProfileUser[1].EmailPrivate,
        EmailOffice: result.data.ProfileUser[1].EmailOffice,
        CompanyID: result.data.ProfileUser[1].CompanyID,
        token: result.data.token,
      };
      await dispatch(initAction(payload));
      localStorage.setItem('token_prospect', result.data.token);
      localStorage.setItem('tempUser', JSON.stringify(payload));
      props.history.push('/app/admin/dashboard-greencard');
      // window.location.href = '/app/admin/dashboard-greencard';
    }
    // setTimeout(() => {
    //   console.log(`You submitted:\n\n${values}`);
    //   window.location.href = '/app/admin/dashboard-greencard';
    // }, 500); // simulate server latency
  };

  const title = brand.name + ' - Login';
  const description = brand.desc;
  const { classes } = props;
  return (
    <div className={classes.root}>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
      <div className={classes.container}>
        <div className={classes.userFormWrap}>
          <LoginForm onSubmit={values => submitForm(values)} />
        </div>
      </div>
    </div>
  );
}

Login.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Login);
