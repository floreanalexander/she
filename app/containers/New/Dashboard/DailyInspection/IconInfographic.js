import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import OndemandVideo from '@material-ui/icons/OndemandVideo';
import SupervisorAccount from '@material-ui/icons/SupervisorAccount';
import CollectionsBookmark from '@material-ui/icons/CollectionsBookmark';
import Edit from '@material-ui/icons/Edit';
import colorfull from 'dan-api/palette/colorfull';
import { CounterWidget } from 'dan-components';
import styles from 'dan-components/Widget/widget-jss';

function IconInfographic(props) {
  const { classes } = props;
  return (
    <div className={classes.rootCounterFull}>
      <Grid container spacing={3}>
        <Grid item xs={12} md={12} lg={12}>
          <CounterWidget
            color={colorfull[3]}
            start={0}
            end={54}
            duration={3}
            title="Total Daily Inspection"
          >
            <Edit className={classes.counterIcon} />
          </CounterWidget>
        </Grid>
      </Grid>
      <Grid container spacing={2}>
        <Grid item xs={12} md={4} lg={4}>
          <CounterWidget
            color={colorfull[2]}
            start={0}
            end={43}
            duration={3}
            title="Terkirim ke Admin"
          >
            <Edit className={classes.counterIcon} />
          </CounterWidget>
        </Grid>
        <Grid item xs={12} md={4} lg={4}>
          <CounterWidget
            color={colorfull[2]}
            start={0}
            end={4}
            duration={3}
            title="Ditugaskan ke PIC"
          >
            <Edit className={classes.counterIcon} />
          </CounterWidget>
        </Grid>
        <Grid item xs={12} md={4} lg={4}>
          <CounterWidget
            color={colorfull[2]}
            start={0}
            end={0}
            duration={3}
            title="Diterima PIC"
          >
            <Edit className={classes.counterIcon} />
          </CounterWidget>
        </Grid>
        <Grid item xs={12} md={4} lg={4}>
          <CounterWidget
            color={colorfull[2]}
            start={0}
            end={7}
            duration={3}
            title="Selesai dan Dikonfirmasi Admin"
          >
            <Edit className={classes.counterIcon} />
          </CounterWidget>
        </Grid>
        <Grid item xs={12} md={4} lg={4}>
          <CounterWidget
            color={colorfull[2]}
            start={0}
            end={0}
            duration={3}
            title="Batal"
          >
            <Edit className={classes.counterIcon} />
          </CounterWidget>
        </Grid>
        <Grid item xs={12} md={4} lg={4}>
          <CounterWidget
            color={colorfull[2]}
            start={0}
            end={0}
            duration={3}
            title="Diselesaikan PIC"
          >
            <Edit className={classes.counterIcon} />
          </CounterWidget>
        </Grid>
      </Grid>
    </div>
  );
}

IconInfographic.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(IconInfographic);
