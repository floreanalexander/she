import React from 'react';
import { Bar } from 'react-chartjs-2';

const data = {
  labels: [
    'Alat Berat/Alat Produksi', 
    'Lainnya...', 
    'APD, Alat Pelindung Diri',
    'Lalu Lintas/Traffic Management',
    'Personal Value', 
    'Housekeeping/Tata Griya',
    'Pekerjaan Panas/Hot Work', 
    'Alat Angkat/Pengangkatan',
    'Bahan Berbahaya Beracun (B3)/Limbah B3',
    'Scaffolding/formwork'],
  datasets: [
    {
      label: 'Type of Finding',
      backgroundColor: '#B3E5FC',
      borderColor: '#1976D2',
      borderWidth: 1,
      hoverBackgroundColor: '#0288D1',
      hoverBorderColor: '#1976D2',
      data: [1, 5, 4, 1, 1, 23, 1, 2, 8, 4, 0]
    }
  ]
};

function BarTypeOfFinding() {
  return (
    <div>
      <h2>Type of Finding</h2>
      <Bar
        data={data}
        width={100}
        height={50}
      />
    </div>
  );
}

export default BarTypeOfFinding;