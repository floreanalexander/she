import React from 'react';
import { Bar } from 'react-chartjs-2';

const data = {
  labels: [
    'Opik Taufik',
    'BENNY OCTO', 
    'Restu Andri', 
    'Agung Isbagiono',
    'Kokoh Dwiyan',
    'Muhammad Filda Neterian',
    'Imam Alifmuin', 
    ],
  datasets: [
    {
      label: 'Submitted Finding',
      backgroundColor: '#B3E5FC',
      borderColor: '#1976D2',
      borderWidth: 1,
      hoverBackgroundColor: '#0288D1',
      hoverBorderColor: '#1976D2',
      data: [13, 9, 9, 7, 7, 6, 5, 0]
    }
  ]
};

function BarMostOftenSubmittedFinding() {
  return (
    <div>
      <h2>The Most Often Submitted Finding</h2>
      <Bar
        data={data}
        width={100}
        height={50}
      />
    </div>
  );
}

export default BarMostOftenSubmittedFinding;