import React from 'react';
import { Bar } from 'react-chartjs-2';

const data = {
  labels: [
    'SAGI NURMANTO', 
    'Muhammad Filda Neterian',
    'YOGI SOPANDI'],
  datasets: [
    {
      label: 'Reported PIC',
      backgroundColor: '#B3E5FC',
      borderColor: '#1976D2',
      borderWidth: 1,
      hoverBackgroundColor: '#0288D1',
      hoverBorderColor: '#1976D2',
      data: [10, 2, 1, 0]
    }
  ]
};

function BarMostOftenReportedPIC() {
  return (
    <div>
      <h2>The Most Often Reported PIC</h2>
      <Bar
        data={data}
        width={100}
        height={50}
      />
    </div>
  );
}

export default BarMostOftenReportedPIC;