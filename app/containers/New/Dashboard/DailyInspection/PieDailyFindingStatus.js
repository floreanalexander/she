import React from 'react';
import { Pie } from 'react-chartjs-2';

const data = {
  labels: [
    'Closed',
    'In Progress',
    'Open'
  ],
  datasets: [{
    data: [7, 6, 43],
    backgroundColor: [
      '#2196F3',
      '#4CAF50',
      '#FFEB3B'
      
    ],
    hoverBackgroundColor: [
      '#1976D2',
      '#388E3C',
      '#FBC02D'
    ]
  }]
};

function PieDailyFindingStatus() {
  return (
    <div>
      <h2>Daily Finding Status</h2>
      <Pie data={data} />
    </div>
  );
}

export default PieDailyFindingStatus;
