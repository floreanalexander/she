import React from 'react';
import { Bar } from 'react-chartjs-2';

const data = {
  labels: [
    'GA', 
    'SHE'],
  datasets: [
    {
      label: 'Submitted Green Card',
      backgroundColor: '#B3E5FC',
      borderColor: '#1976D2',
      borderWidth: 1,
      hoverBackgroundColor: '#0288D1',
      hoverBorderColor: '#1976D2',
      data: [20, 88, 0]
    }
  ]
};

function BarMostSubmittedGreencard() {
  return (
    <div>
      <h2>Submitted Green Card</h2>
      <Bar
        data={data}
        width={100}
        height={50}
      />
    </div>
  );
}

export default BarMostSubmittedGreencard;