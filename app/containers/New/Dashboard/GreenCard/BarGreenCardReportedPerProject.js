import React from 'react';
import { Bar } from 'react-chartjs-2';

const data = {
  labels: [
    'Thamrin Nine', 
    'Cleon Park Apartment',
    'Fly Ash Silo Batang', 
    'Workshop JonggoL',
    'PLTM Besai Kemu',  
    'Workshop Cileungsi'],
  datasets: [
    {
      label: 'Project',
      backgroundColor: '#B3E5FC',
      borderColor: '#1976D2',
      borderWidth: 1,
      hoverBackgroundColor: '#0288D1',
      hoverBorderColor: '#1976D2',
      data: [20, 34, 11, 18, 7, 18, 0]
    }
  ]
};

function BarGreenCardReportedPerProject() {
  return (
    <div>
      <h2>Green Card Reported Per Project</h2>
      <Bar
        data={data}
        width={100}
        height={50}
      />
    </div>
  );
}

export default BarGreenCardReportedPerProject;