import React from 'react';
import { Pie } from 'react-chartjs-2';

const data = {
  labels: [
    'Kondisi Tidak Aman',
    'Tindakan Aman',
    'Tindakan Tidak Aman',
    'Saran'
  ],
  datasets: [{
    data: [57, 42, 7, 2],
    backgroundColor: [
      '#2196F3',
      '#4CAF50',
      '#FFEB3B',
      '#F44336'
    ],
    hoverBackgroundColor: [
      '#1976D2',
      '#388E3C',
      '#FBC02D',
      '#D32F2F'
    ]
  }]
};

function PieTypeOfHazard() {
  return (
    <div>
      <h2>Type of Hazard</h2>
      <Pie data={data} />
    </div>
  );
}

export default PieTypeOfHazard;
