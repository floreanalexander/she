import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import SearchIcon from '@material-ui/icons/Search';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import { NewPaperBlock } from 'dan-components';

import IconInfographic from './IconInfographic';
import BarGreenCardStatus from './BarGreenCardStatus';
import BarGreenCardReportedPerProject from './BarGreenCardReportedPerProject';
import PieGreenCardStatus from './PieGreenCardStatus';
import PieTypeOfHazard from './PieTypeOfHazard';
import BarGreenCardMostReportedHazard from './BarGreenCardMostReportedHazard';
import BarMostSubmittedGreencard from './BarMostSubmittedGreencard';
import BarTopUser from './BarTopUser';
import ListTender from './ListTender';
import useGet from '../../../../hooks/useGet';

import { withStyles } from '@material-ui/core/styles';
import BarTopPIC from './BarTopPIC';

const styles = theme => ({
  divider: {
    margin: `${theme.spacing(3)}px 0`,
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 220,
  },
  picker: {
    margin: `${theme.spacing(3)}px 5px`,
  },
  button: {
    margin: theme.spacing(1),
  },
  leftIcon: {
    marginRight: theme.spacing(1),
  },
});

function DashboardGreenCard(props) {
  const { responseGetData } = useGet();
  const { classes } = props;
  const title = 'Dashboard Green Card';
  const description = 'Halaman Dashboard Green Card';

  const [loadingList, setLoadingList] = useState(true);
  const [listCompany, setListCompany] = useState([]);
  const [listDisplay, setListDisplay] = useState([]);

  const [selectedCompany, setSelectedCompany] = useState('');
  const [selectedDisplay, setSelectedDisplay] = useState('');
  const [selectedStartDate, setSelectedStartDate] = useState(new Date());
  const [selectedEndDate, setSelectedEndDate] = useState(new Date());

  const fetchListCompany = async () => {
    const result = await responseGetData('/api/company', false);
    if (!result) {
      setListCompany([]);
      return;
    }
    setListCompany(result);
  };

  useEffect(() => {
    async function fetchDropdown() {
      await fetchListCompany();
      setLoadingList(false);
    }
    fetchDropdown();
  }, []);

  const handleChangeCompany = event => {
    setSelectedCompany(event.target.value);
  };
  const handleChangeDisplay = event => {
    setSelectedDisplay(event.target.value);
  };
  const handleStartDateChange = date => {
    setSelectedStartDate(date);
  };
  const handleEndDateChange = date => {
    setSelectedEndDate(date);
  };
  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
      <IconInfographic />
      <NewPaperBlock>
        <BarGreenCardStatus />
      </NewPaperBlock>
      <NewPaperBlock>
        <BarGreenCardReportedPerProject />
      </NewPaperBlock>
      <NewPaperBlock>
        <PieGreenCardStatus />
      </NewPaperBlock>
      <NewPaperBlock>
        <PieTypeOfHazard />
      </NewPaperBlock>
      <NewPaperBlock>
        <BarGreenCardMostReportedHazard />
      </NewPaperBlock>
      <NewPaperBlock>
        <BarMostSubmittedGreencard />
      </NewPaperBlock>
      <NewPaperBlock>
        <BarTopUser />
      </NewPaperBlock>
      <NewPaperBlock>
        <BarTopPIC />
      </NewPaperBlock>
      <ListTender />
    </div>
  );
}

DashboardGreenCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DashboardGreenCard);
