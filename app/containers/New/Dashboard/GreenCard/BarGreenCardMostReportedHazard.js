import React from 'react';
import { Bar } from 'react-chartjs-2';

const data = {
  labels: [
    'Bahan Berbahaya Beracun (B3)/Limbah B3', 
    'Scaffolding/formwork', 
    'Lainnya:...', 
    'Alat Angkat/Pengangkatan', 
    'Alat Berat/Alat Produksi', 
    'Pekerjaan Panas/Hot Work',
    'Proteksi lubang & railing',
    'APD, Alat Pelindung Diri',
    'Akses/Bekerja di Ketinggian',
    'Personal Value'
    ],
  datasets: [
    {
      label: 'Hazard',
      backgroundColor: '#B3E5FC',
      borderColor: '#1976D2',
      borderWidth: 1,
      hoverBackgroundColor: '#0288D1',
      hoverBorderColor: '#1976D2',
      data: [6, 5, 12, 3, 1, 4, 4, 7, 6, 4, 0]
    }
  ]
};

function BarGreenCardMostReportedHazard() {
  return (
    <div>
      <h2>Green Card Most Reported Hazard</h2>
      <Bar
        data={data}
        width={100}
        height={50}
      />
    </div>
  );
}

export default BarGreenCardMostReportedHazard;