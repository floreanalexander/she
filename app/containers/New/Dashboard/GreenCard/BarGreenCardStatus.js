import React from 'react';
import colorfull from 'dan-api/palette/colorfull';
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  CartesianAxis,
  Tooltip,
} from 'recharts';

const color = ({
  primary: colorfull[6],
  secondary: colorfull[3],
  third: colorfull[2],
  fourth: colorfull[4],
});

const dataSales = [
  {
    name: 'Jan',
    Fashions: 40,
    Electronics: 124,
    Toys: 17,
    Vouchers: 20
  },
  {
    name: 'Feb',
    Fashions: 45,
    Electronics: 100,
    Toys: 2,
    Vouchers: 100
  },
  {
    name: 'Mar',
    Fashions: 27,
    Electronics: 20,
    Toys: 0,
    Vouchers: 80
  },
  {
    name: 'Apr',
    Fashions: 50,
    Electronics: 120,
    Toys: 29,
    Vouchers: 14
  },
  {
    name: 'May',
    Fashions: 32,
    Electronics: 117,
    Toys: 20,
    Vouchers: 86
  },
  {
    name: 'Jun',
    Fashions: 50,
    Electronics: 34,
    Toys: 11,
    Vouchers: 29
  },
  {
    name: 'Jul',
    Fashions: 24,
    Electronics: 40,
    Toys: 3,
    Vouchers: 40
  },
  {
    name: 'Aug',
    Fashions: 32,
    Electronics: 117,
    Toys: 20,
    Vouchers: 86
  },
  {
    name: 'Sept',
    Fashions: 40,
    Electronics: 20,
    Toys: 24,
    Vouchers: 100
  },
  {
    name: 'Oct',
    Fashions: 50,
    Electronics: 113,
    Toys: 29,
    Vouchers: 14
  },
  {
    name: 'Dec',
    Fashions: 79,
    Electronics: 101,
    Toys: 4,
    Vouchers: 3
  },
];

function BarGreenCardStatus() {
  return (
    <div>
                <BarChart
                  data={dataSales}
                >
                  <XAxis dataKey="name" tickLine={false} />
                  <YAxis axisLine={false} tickSize={3} tickLine={false} tick={{ stroke: 'none' }} />
                  <CartesianGrid vertical={false} strokeDasharray="3 3" />
                  <CartesianAxis />
                  <Tooltip />
                  <Bar dataKey="Fashions" fill={color.primary} />
                  <Bar dataKey="Electronics" fill={color.secondary} />
                  <Bar dataKey="Toys" fill={color.third} />
                  <Bar dataKey="Vouchers" fill={color.fourth} />
                </BarChart>
          </div>
  );
}

export default BarGreenCardStatus;