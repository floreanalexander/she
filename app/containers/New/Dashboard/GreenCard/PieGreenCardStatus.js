import React from 'react';
import { Pie } from 'react-chartjs-2';

const data = {
  labels: [
    'Open',
    'Closed',
    'In Progress'
  ],
  datasets: [{
    data: [45, 9, 54],
    backgroundColor: [
      '#FFEB3B',
      '#4CAF50',
      '#2196F3'
    ],
    hoverBackgroundColor: [
      '#FBC02D',
      '#388E3C',
      '#1976D2'
    ]
  }]
};

function PieGreenCardStatus() {
  return (
    <div>
      <h2>Green Card Status</h2>
      <Pie data={data} />
    </div>
  );
}

export default PieGreenCardStatus;
