import React from 'react';
import { Bar } from 'react-chartjs-2';

const data = {
  labels: [
    'Chaerul Rijal',
    'Imam Alifmuin',
    'SAGI NURMANTO', 
    'Arief Rahman', 
    'Muhammad Filda Neterian',
    'ARI KRISWANTO',
    'Syachroni Arief Budiman',
    'Teguh',
    'Eddy Budhiyanto',
    'Cep Jamaludin'],
  datasets: [
    {
      label: 'Top 10 PIC',
      backgroundColor: '#B3E5FC',
      borderColor: '#1976D2',
      borderWidth: 1,
      hoverBackgroundColor: '#0288D1',
      hoverBorderColor: '#1976D2',
      data: [15, 8, 7, 7, 5, 4, 4, 2, 2, 2, 0]
    }
  ]
};

function BarTopPIC() {
  return (
    <div>
      <h2>Top 10 PIC</h2>
      <Bar
        data={data}
        width={100}
        height={50}
      />
    </div>
  );
}

export default BarTopPIC;