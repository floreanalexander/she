import React from 'react';
import { Bar } from 'react-chartjs-2';

const data = {
  labels: [
    'Opik Taufik',
    'Dian Afriyeni',
    'RAHMAT WAHYUDI',
    'Heri Akbar',
    'Eddy Polistiono',
    'Hendriyatna',
    'Widi Yuni H',
    'EGY DJUMHANA KARTAKUSUMA',
    'Muhammad Filda Neterian',
    'ARI KRISWANTO'],
  datasets: [
    {
      label: 'Top User',
      backgroundColor: '#B3E5FC',
      borderColor: '#1976D2',
      borderWidth: 1,
      hoverBackgroundColor: '#0288D1',
      hoverBorderColor: '#1976D2',
      data: [8, 8, 7, 6, 4, 4, 4, 4, 4, 3, 0]
    }
  ]
};

function BarTopUser() {
  return (
    <div>
      <h2>Top 10 User</h2>
      <Bar
        data={data}
        width={100}
        height={50}
      />
    </div>
  );
}

export default BarTopUser;