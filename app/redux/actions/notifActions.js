import * as types from '../constants/notifConstants';

export const showNotifAction = data => ({ type: types.SHOW_NOTIF, data });
export const clearNotifAction = () => ({ type: types.CLOSE_NOTIF });
