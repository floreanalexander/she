import produce from 'immer';
import { INIT, LOGOUT } from '../constants/reduxFormConstants';

const initialState = {
  // usersLogin: {
  //   email: 'johndoe@mail.com',
  //   password: '12345678',
  //   remember: false,
  // },
  usersLogin: JSON.parse(localStorage.getItem('tempUser')),
};

/* eslint-disable default-case, no-param-reassign */
const loginReducer = (state = initialState, action = {}) =>
  produce(state, draft => {
    switch (action.type) {
      case INIT:
        draft.userLogin = state;
        draft.usersLogin = action.data;
        break;
      case LOGOUT:
        localStorage.removeItem('token_prospect');
        draft.usersLogin = null;
        break;
      default:
        break;
    }
  });

export default loginReducer;
