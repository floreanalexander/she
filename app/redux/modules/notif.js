import produce from 'immer';
import { SHOW_NOTIF, CLOSE_NOTIF } from '../constants/notifConstants';

const initialState = {
  message: '',
};

/* eslint-disable default-case, no-param-reassign */
const notifReducer = (state = initialState, action = {}) =>
  produce(state, draft => {
    switch (action.type) {
      case SHOW_NOTIF:
        draft.message = action.data;
        break;
      case CLOSE_NOTIF:
        draft.message = '';
        break;
      default:
        break;
    }
  });

export default notifReducer;
